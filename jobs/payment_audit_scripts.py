from datetime import datetime
import logging
import zipfile
import time
import csv
import sys
import io
from io import StringIO
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import click
from rich.prompt import Prompt
from rich.progress import track
from rich.console import Console
from rich.table import Table
from config import *

AUDIT_REPORT_CSV = "audit_report.csv"

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

read_conn = psycopg2.connect(
        database=READ_DB_CONNECTION_DB, 
        user=READ_DB_CONNECTION_USERNAME, 
        password=READ_DB_CONNECTION_PASSWORD, 
        host=READ_DB_CONNECTION_HOST, 
        port=READ_DB_CONNECTION_PORT
    )


def fetch_data(method, relative_url, params=None, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.status_code)
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + response.json()['message'])
    return response.json()

def get_all_modified_loans(unixtimestamp):
    url = 'loans'
    params = {'modifiedSinceTimestamp': unixtimestamp}
    response = fetch_data('get', url, params=params)
    loans = []
    filtered_records = response['totalFilteredRecords']
    loans.extend(response['pageItems'])
    offset = 0
    limit = 200
    while filtered_records > 200:
        offset = offset + limit 
        params['offset'] = offset
        response = fetch_data('get', url, params=params)
        loans.extend(response['pageItems'])
        filtered_records = filtered_records - limit
    logging.info("totals loans fetched and filtered %s %s", len(loans), response['totalFilteredRecords'])
    return loans

def get_all_repayments():
    logging.info("fetching all client details")
    client_det  = fetch_data("GET",'data-export/prepared/addToQueue',{"tenantIdentifier":MUSONI_TENANT_ID,"exportId":MUSONI_PAYMENT_AUDIT_ID,"fileFormat":"csv"})
    logging.info("report queued")
    time.sleep(5)
    if client_det:
        response = fetch_data("GET",'data-export/prepared/{0}/attachment/direct-link'.format(str(client_det)))
        
        if response:
            if '.zip' in response['value']:
                r = requests.get(response['value'])
                zf = zipfile.ZipFile(io.BytesIO(r.content))
                file = zf.namelist()[0]
                transactions = pd.read_csv(zf.open(file),delimiter = ';')
            else:
                transactions = pd.read_csv(response['value'],delimiter=';')
    logging.info("report downloaded")
    transactions = transactions[transactions['Total Repaid'].notnull()]
    return transactions
    

def get_invoice_number(loan_id):
    url = f'loans/{loan_id}/'
    response = fetch_data('get', url, params={})
    # logging.info("totals loans fetched and filtered %s %s", len(loans), response['totalFilteredRecords'])
    return response['externalId']

def get_ims_allocation_details(confirmation_code):
    sql_query = """select payment_refrence_code, pc.transaction_amount,customer_invoice_payments.amount,  customer_invoices.invoice_number, cp.id as customer_payment_id, cp.customer_id as customer_payment_cust_id , pc.customer_id as tille_payment_cust_id, pc.sender_number 
    from payment_confirmations pc 
    join customer_payment_details cpd on pc.confirmation_code = cpd.payment_refrence_code 
    join customer_payments cp on cpd.customer_payment_id = cp.id
    left join customer_invoice_payments on customer_invoice_payments.customer_payment_id = cp.id
    left join invoice_payment_allocations on invoice_payment_allocations.id = customer_invoice_payments.invoice_payment_allocation_id
    left join customer_invoices on customer_invoices.id = invoice_payment_allocations.customer_invoice_id
    where payment_refrence_code  = %s"""
    cursor = read_conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute(sql_query, (confirmation_code, ))
    return cursor.fetchall()

def check_invoices_match(ims_details, musoni_invoice, amount, customer_id):
    invoice_matching = False
    amount_matching = False
    customer_matching = True
    entire_amount = 0
    reason = ''
    logging.info("payment to be matched %s %s %s", musoni_invoice, amount, customer_id)
    for record in ims_details:
        logging.info("matching record %s ", record)
        if record['customer_payment_cust_id'] != customer_id:
            logging.info("customer is not matching %s, %s", record['customer_payment_cust_id'], customer_id)
            reason += f"customer({record['customer_payment_cust_id']}, {customer_id})"
            customer_matching  = False
        logging.info("invoice matched %s, %s", record['invoice_number'], musoni_invoice)
        if record['invoice_number'] == musoni_invoice and not invoice_matching:
            logging.info("invoice matched %s, %s", record['invoice_number'], musoni_invoice)
            invoice_matching = True
        else:
            reason += f"invoice({record['invoice_number']}, {musoni_invoice})"
        entire_amount = entire_amount + record['amount']
    if float(amount) == entire_amount:
        amount_matching = True
        logging.info("amount matched %s, %s", entire_amount, amount)
    else:
        reason += f"amount({entire_amount}, {amount})"
    return amount_matching, invoice_matching, customer_matching, reason


    


def display_result(final_results, success, error):
    table = Table(title="Match data")
    table.add_column("Confirmation Code", justify="center", style="cyan",)
    table.add_column("Amount",justify="center")
    table.add_column("Invoice", justify="center")
    table.add_column("Customer", justify="center")
    table.add_column("Reason", justify="center")
    for record in final_results:
        table.add_row(str(record['code']), str(record['amount']), str(record['invoice']), str(record['customer']), str(record['reason']))
    console = Console()
    console.print(table)

def is_repayment(record):
    return True if record['Total Repaid'] else False

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def process_csv_file(file_path):
    success_count = 0
    error_count = 0
    final_results = []
    error_records = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp, delimiter=";")
            for record in reader:
                if not is_repayment(record):
                    continue
                logging.info("processing record %s", record)
                try:
                    invoice_number = get_invoice_number(record['Account Number'].lstrip('0'))
                    # ims_details = get_ims_allocation_details(record['Reference'])
                    ims_details = get_ims_allocation_details('OF82RZXV06')
                    logging.info("rows and invoice retrieved %s %s", ims_details[0]['invoice_number'], invoice_number)
                    amount_matching, invoice_matching, customer_matching, reason = check_invoices_match(ims_details, invoice_number, record['Total Repaid'], record['External Client ID'])
                    final_results.append(
                        {
                            'code': record['Reference'],
                            'amount': amount_matching,
                            'invoice': invoice_matching,
                            'customer': customer_matching,
                            'reason': reason
                        }
                    )
                    logging.info("matching details %s %s %s", amount_matching, invoice_matching, customer_matching)
                    success_count = success_count + 1
                except:
                    error_count = error_count + 1
                    error_records.append(record)
                    logging.exception("error in processing the record %s", record)
    except:
        logging.exception("error in processing file")
    finally:
        
        display_result(final_results, success_count, error_count)

@click.command()
def do_payment_audit():
    success_count = 0
    error_count = 0
    final_results = []
    error_records = []
    try:
        for record in get_all_repayments().to_dict("records"):
            logging.info("processing record %s", record)
            try:
                invoice_number = get_invoice_number(record['Account Number'])
                ims_details = get_ims_allocation_details(record['Reference'])
                logging.info("rows and invoice retrieved %s %s", ims_details[0]['invoice_number'], invoice_number)
                amount_matching, invoice_matching, customer_matching, reason = check_invoices_match(ims_details, invoice_number, record['Total Repaid'], record['External Client ID'])
                final_results.append(
                    {
                        'code': record['Reference'],
                        'amount': amount_matching,
                        'invoice': invoice_matching,
                        'customer': customer_matching,
                        'reason': reason
                    }
                )
                logging.info("matching details %s %s %s", amount_matching, invoice_matching, customer_matching)
                success_count = success_count + 1
            except:
                error_count = error_count + 1
                error_records.append(record)
                logging.exception("error in processing the record %s", record)
    except:
        logging.exception("error in processing file")
    finally:
        csv_header = ['code', 'amount', 'invoice', 'customer', 'reason']
        if final_results:
            writer = csv.DictWriter(open(AUDIT_REPORT_CSV, "w"), fieldnames=csv_header)
            writer.writeheader()
            writer.writerows(final_results)
        display_result(final_results, success_count, error_count)



commands.add_command(process_csv_file)
commands.add_command(do_payment_audit)


if __name__ == '__main__':
   commands() 
   # print(get_all_repayments().to_dict("records"))