#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
if [ -z "$ARGS" ]
  then
    echo "If ARGS is not passed then it runs with the current date"
    /usr/bin/python3 jobs/client_manager.py close-without-limit
  else
    echo "ARGS is passed running for"
    echo ${ARGS}
    /usr/bin/python3 jobs/client_manager.py close-without-limit --close-date ${ARGS}
fi
popd