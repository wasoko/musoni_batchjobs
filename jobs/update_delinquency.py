import datetime
from datetime import date, timedelta
import json
import logging
import sys
import warnings

import click
import musoni_utils
import pandas as pd
import psycopg2
import psycopg2.extras
from config import *
from crm_connector import (get_auth_token, get_customer_details,
                           update_customer_details)
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")  

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout),logging.FileHandler('AL_update_MusoniIms_v2.log')])

conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

def fetch_overdue_customers_for_date(overdue_date):
    logging.info("fetch overdue customers from musoni data filters api")
    payload = {
        "filterRulesExpression": {
            "condition": "AND",
            "rules": [
                {
                    "id": "status",
                    "field": "status",
                    "type": "integer",
                    "input": "select",
                    "operator": "equal",
                    "value": 300
                },
                {
                    "id": "overdueSinceDate",
                    "field": "overdueSinceDate",
                    "type": "date",
                    "input": "overdueSinceDate",
                    "operator": "equal",
                    "value": overdue_date
                }
            ],
            "valid": True
        },
        "responseParameters": [
            {
                "name": "id",
                "ordinal": 0
            },
            {
                "name": "clientId",
                "ordinal": 3
            },
            {
                "name": "externalId",
                "ordinal": 1
            },
            {
                "name": "overdueSinceDate",
                "ordinal": 2
            }
        ],
        "sortByParameters": [
            {
                "ordinal": 0,
                "name": "principalOutstanding",
                "direction": "DESC"
            }
        ]
    }
    offset = 0
    limit = 200
    final_response = []
    response = musoni_utils.call_data_filter(payload, offset, limit)
    while response['pageItems']:
        final_response.extend(response['pageItems'])
        if response['totalFilteredRecords'] < limit:
            break
        offset = offset + limit
        response = musoni_utils.call_data_filter(payload, offset, limit)
    return final_response

def fetch_non_delinquent_overdue_records():
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user = MUSONI_DB_CONNECTION_USERNAME,pwd = MUSONI_DB_CONNECTION_PASSWORD , host = MUSONI_DB_CONNECTION_HOST,port = MUSONI_DB_CONNECTION_PORT,db = MUSONI_DB_CONNECTION_DB))
    query = """select
            m_client.id as musoni_id,
            m_client.external_id as external_id,
            m_office.name as branch,
            m_code_value.code_value as country,
            ml_client_details.Available_Credi11,
            ml_client_details.Delinquency12,
            count(
                CASE
                WHEN enum_val2.enum_value = 'Active'
                and m_loan.expected_maturedon_date <= current_date then 1
                end
            ) as overdue_orders
            from
            m_loan m_loan
            left join (
            Select
                *
                from
                r_enum_value
                where
                enum_name = 'loan_status_id'
            ) enum_val2 on enum_val2.enum_id = m_loan.loan_status_id
            left join m_client m_client on m_client.id = m_loan.client_id
            left join m_staff staff on staff.id = m_client.staff_id
            left join ml_client_details ml_client_details on ml_client_details.client_id = m_client.id
            left join m_office m_office on m_office.id = m_client.office_id
            left join m_code_value m_code_value on m_code_value.id = ml_client_details.COUNTRY_cd_Country8
            group by
            m_client.id,
            m_client.external_id,
            m_office.name,
            m_code_value.code_value,
            ml_client_details.Available_Credi11,
            ml_client_details.Delinquency12
            having
            count(
                CASE
                WHEN enum_val2.enum_value = 'Active'
                and m_loan.expected_maturedon_date <= current_date then 1
                end
            ) >= 1
            and ml_client_details.Delinquency12 <> 1"""
    
    records = pd.read_sql_query(query, engine)
    
    return records 

def get_external_id_for_client_id(client_id):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user = MUSONI_DB_CONNECTION_USERNAME,pwd = MUSONI_DB_CONNECTION_PASSWORD , host = MUSONI_DB_CONNECTION_HOST,port = MUSONI_DB_CONNECTION_PORT,db = MUSONI_DB_CONNECTION_DB))
    query = "select external_id from m_client where id=%s"
    records = pd.read_sql_query(query, engine, params=(client_id, ))
    return records['external_id'][0]

def update_delinquency_to_musoni(client_id):
    logging.info("updating delinquency in musoni for client_id {}".format(str(client_id)))
    try:
        payload = {
            "delinquency":1,
        }
        musoni_utils.update_client_credit_details(client_id, payload)
    except:
        logging.exception("error in updating reason for client %s", client_id)
                    
def update_delinquency_to_ims_query(customer_id):
    logging.info("updating ims table %s", customer_id)
    query = "update customers set delinquency=1, updated_at=now() where id=%s"
    cursor = conn.cursor()
    try:
        cursor.execute(query, (customer_id, ))
        return True, 'Success'
    except:
        logging.exception("error in updating table")
        cursor.execute("ROLLBACK")
        return True, 'Exception'
    finally:
        conn.commit()

@click.command
@click.option('--run-date', default=(date.today() - timedelta(days=1)).strftime("%Y-%m-%d"))
def run_delinquency_script(run_date):
    error_log = []
    logging.info("running delinquency job for the date %s", run_date)
    try:
        records = fetch_overdue_customers_for_date(run_date)
        for record in records:
            logging.info("processing record %s", record)
            update_delinquency_to_musoni(record['clientId'])
            update_delinquency_to_ims_query(get_external_id_for_client_id(record['clientId']))
    except Exception as e:
        logging.exception("error in performing delinquency update")
        error_log.append(str(e))
          
    if  len(error_log)>=1:
        logging.exception("Summary : \n\n Script ran with the following errors -- %s",(" ".join(error_log)))
        sys.exit(1)
    else:
        logging.info("Script ran successfully")
        sys.exit(0)

if __name__=="__main__":
    run_delinquency_script()  # type: ignore
        


