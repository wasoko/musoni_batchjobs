import sys
import logging
import csv
import click
import psycopg2.extras

from crm_connector import *

logging.basicConfig(
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )

def update_credit_limit_to_ims_db(customer_id, credit_limit, delinquency):
    cursor = conn.cursor()
    query = """update customers set credit_limit = {}, delinquency = {} where id = '{}'"""
    cursor.execute(query.format(credit_limit, delinquency, customer_id))
    conn.commit()

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def update_IMS_DB(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    update_credit_limit_to_ims_db(record['customer_id'], record['credit_limit'], record['delinquency'])
                except:
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
            writer = csv.DictWriter(open("clients_reactivation_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)


commands.add_command(update_IMS_DB, 'update_IMS_DB')

if __name__ == '__main__':
    commands()
