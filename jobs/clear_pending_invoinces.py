from datetime import datetime
import uuid
import datetime as dt
import psycopg2
import psycopg2.extras
import pandas as pd
import csv
import sys
import click
from config import *
import logging

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

@click.group()
def comamnds():
    pass

# @click.command()
def close_old_invoice(invoice_id, payment_id, amount):
    invoice_allocation_id = str(uuid.uuid4())
    customer_invoice_allocation_id = str(uuid.uuid4())

    get_invoice_query = "select * from customer_invoices where id=%s"
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute(get_invoice_query, (invoice_id, ))
    data = cursor.fetchall()[0]

    invoice_allocation_ins_query = "insert into invoice_payment_allocations(id, customer_invoice_id, created_at, updated_at, txn_type) values(%s, %s, %s, %s, %s)"
    customer_invoice_payments = "insert into customer_invoice_payments(id, invoice_payment_allocation_id, customer_payment_id, amount, created_at, updated_at) values(%s, %s, %s, %s, %s, %s)"
    update_customer_invoice_query = "update customer_invoices set aasm_state=%s, balance=0 where id=%s"
    update_customer_payments_query = "update customer_payments set aasm_state=%s, balance=0 where id=%s"

    cursor.execute(invoice_allocation_ins_query, (invoice_allocation_id, invoice_id, datetime.utcnow(), datetime.utcnow(), 'credit'))
    cursor.execute(customer_invoice_payments, (customer_invoice_allocation_id, invoice_allocation_id, payment_id, amount, datetime.utcnow(), datetime.utcnow()))
    cursor.execute(update_customer_invoice_query, ('cleared', invoice_id, ))
    cursor.execute(update_customer_payments_query, ('cleared', payment_id, ))
    conn.commit()




# @click.command()
def main():
    reader = open("payments_clear_sql.sql")
    cursor = conn.cursor()
    for line in reader.readlines():
        logging.info(line)
        try:
            cursor.execute(line)
            conn.commit()
        except:
            logging.exception('exception while running query')
            conn.rollback()

if __name__ == '__main__':
    #close_old_invoice('6f3c0f4e-20a9-4f87-9fcb-7772e69e3a81', 'cdb2fd2c-5a67-4822-aa0c-c97e8237e250', 3000)
    main()