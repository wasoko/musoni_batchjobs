#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/manage_skip_kyc.py add-customers-esign customers_to_process.csv
popd
