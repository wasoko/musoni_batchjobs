#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
if [ -z "$ARGS" ]
  then
    echo "If ARGS is not passed then it runs with the default of 30 days"
    /usr/bin/python3 jobs/blacklist_clients.py blacklist_clients
  else
    echo "ARGS is passed running for"
    echo ${ARGS}
    /usr/bin/python3 jobs/blacklist_clients.py blacklist_clients --max-default-days ${ARGS}
fi
popd