#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/third_party_payment_handler.py process-payment-file payment_file.csv
popd
