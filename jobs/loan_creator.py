import sys
import requests
from requests.auth import HTTPBasicAuth
from loan_principal_change import approve_loan, disburse_loan
from config import *
import logging
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import csv
import click

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def get_saving_account_id(client_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'clients/{client_id}/accounts'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.get(url, headers=headers, auth=auth, params = {},json={})
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()['savingsAccounts'][0]['id']

def create_loan(client_id, external_id, saving_account_id, principal, disburse_date, approve_date, submitted_date ):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = {
        "locale": "en_GB",
        "dateFormat": "yyyy-mm-dd",
        "loanType": "individual",
        "clientId": client_id,
        "syncDisbursementWithMeeting": False,
        "productId": 28,
        "externalId": external_id,
        "fundId": 5,
        "loanOfficerId": 8,
        "loanPurposeId": 38,
        "principal": principal,
        "loanTermFrequency": 1,
        "loanTermFrequencyType": 1,
        "numberOfRepayments": 1,
        "repaymentEvery": 1,
        "repaymentFrequencyType": 1,
        "interestType": 1,
        "interestCalculationPeriodType": 0,
        "interestRatePerPeriod": 0.0,
        "amortizationType": 1,
        "expectedDisbursementDate": disburse_date,
        "interestChargedFromDate": disburse_date,
        "graceOnPrincipalPayment": 0,
        "graceOnInterestPayment": 0,
        "graceOnInterestCharged": 0,
        "submittedOnDate": disburse_date,
        "allowPartialPeriodInterestCalcualtion": False,
        "transactionProcessingStrategyId": 1,
        "repayPrincipalEvery": 1,
        "repayInterestEvery": 1,
        "charges": []
    }
    logging.info("payload for loan creation %s", payload)
    response = requests.post(url, headers=headers, auth=auth, params = {},json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()


def apply_charges_to_loan(loan_id, charge_id, charge, due_date):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}/charges'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = {
        "chargeId": charge_id,
        "locale": "en",
        "amount": charge,
        "dateFormat": "dd MMMM yyyy",
        "dueDate": due_date
    }
    response = requests.post(url, headers=headers, auth=auth, params = {},json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def create_loans(file_path):
    output_record = []
    due_delta = relativedelta(days=4)
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                if record['can_migrate'] ==  'yes':
                    logging.info("processing record %s", record)
                    try:
                        disburse_on_date = parse(record['disbursed_on'], dayfirst=True).date()
                        due_date = (disburse_on_date + due_delta).strftime("%d %b %Y")
                        disburse_on_date_str = disburse_on_date.strftime("%Y-%m-%d")
                        print(disburse_on_date_str)
                        loan_details = create_loan(record['client_id'], record['external_id'], '', record['Balance'], disburse_on_date_str, disburse_on_date_str, disburse_on_date_str)
                        approve_loan(loan_details['loanId'], disburse_on_date_str, disburse_on_date_str,'Tanzania loans')
                        disburse_loan(loan_details['loanId'], record['Balance'],disburse_on_date_str,'Tanzania loans')
                        if float(record['charge_remaining']):
                            apply_charges_to_loan(loan_details['loanId'], record['charge_id'], record['charge_remaining'], due_date)
                        record['loan_id'] = loan_details['loanId']
                        record['status'] = 'success'
                    except:
                        record['loan_id'] = 0
                        record['status'] = 'fail'
                        logging.exception("error in processing the record %s", record)
                    output_record.append(record)
                else:
                    record['loan_id'] = 0 
                    record['status'] = 'not migrated'

    except:
        logging.exception("error in processing file")
    if output_record:
            writer = csv.DictWriter(open("loan_principal_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)


commands.add_command(create_loans)

if __name__ == '__main__':
    commands()