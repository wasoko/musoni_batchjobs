#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/musoni_payment_processor.py update_payment_for_loan
  # python connectors/musoni_payment_update.py  2022-03-11
  # we can give the date
popd
