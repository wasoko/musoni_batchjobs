CREATE TABLE IF NOT EXISTS public.musoni_payment_update_tracker
(
    id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    confirmation_code character varying COLLATE pg_catalog."default" NOT NULL,
    status BOOLEAN NOT NULL DEFAULT false,
    reason character varying COLLATE pg_catalog."default",
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);