#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/client_manager.py migrate-clients-dar-east-and-west-to-central
popd