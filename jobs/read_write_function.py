# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 16:49:27 2022

@author: I2262
"""

import pandas as pd
import datetime
import requests
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from gspread_formatting import *
import gspread_formatting as gsf
from google.oauth2 import service_account
import time
import pygsheets
from googleapiclient import discovery
import gspread_dataframe as gd

google_sheet_file = r"fs-apicheck-5101f5d05391.json"
spreadsheet_key  = "" # key can be found in the url

def batch_get_data(sheets_to_read,spreadsheet_key,service):
   
    gs_dataframes = {}
    try:
        ranges = [sheet_name+"!A:BB" for sheet_name in sheets_to_read]
    except Exception as e:
        print(e)
        ranges = [sheet_name+"!A:AS" for sheet_name in sheets_to_read]

    value_render_option  = "UNFORMATTED_VALUE"
    request = service.spreadsheets().values().batchGet(spreadsheetId=spreadsheet_key, ranges=ranges, valueRenderOption=value_render_option).execute()

    for item in request['valueRanges']:        
        temp = pd.DataFrame().from_dict(item['values'])
        temp.columns = temp.loc[0]
        temp = temp[1:]
        gs_dataframes[item['range'].split("!")[0].replace("'","")]  = temp
    return gs_dataframes        


# write one sheet at a time
def write_to_sheet(sheet_name,dataframe,key,frozen_cols):
    
    scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name(google_sheet_file, scope)
    gc = gspread.authorize(credentials)    
    
    
    spreadsheet_key = key
    sh = gc.open_by_key(spreadsheet_key)
    dataframe.replace(np.nan, '', inplace=True)
  
    
    try:

        worksheet = sh.worksheet(sheet_name)
    
        worksheet.update([dataframe.columns.values.tolist()] + dataframe.values.tolist())
        
                # Convert column to number 
                
        end_column = columntoletter(len(dataframe.columns))
        # Divide RGB color by 255
        fmt = cellFormat(
            backgroundColor=gsf.color(0.9,0.7,0.7),
            textFormat=textFormat(bold=True),
            horizontalAlignment='CENTER'
            )
    
        format_cell_range(worksheet,'A1:AH1', fmt)
        
        set_column_width(worksheet, 'M:V', 120)
        set_frozen(worksheet, cols=frozen_cols)
        
        return "success",dataframe
    except Exception as e:
        print(e)
        
        
# append to an existing sheet
new_dataframe = pd.DataFrame() # new data to be added
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name(google_sheet_file, scope)
gc = gspread.authorize(credentials)    
ws = gc.open_by_key(spreadsheet_key).worksheet("1 DPD")
existing = gd.get_as_dataframe(ws)
columns = [col for col in existing.columns if  "Unnamed" in col]
existing.drop(columns,axis=1,inplace=True)
existing = existing[existing['pk'].notnull()]
updated = existing.append(new_dataframe)

gd.set_with_dataframe(ws, updated)
        
