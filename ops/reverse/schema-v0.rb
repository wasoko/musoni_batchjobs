# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 0) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "delivery_waves", id: false, force: :cascade do |t|
    t.string "id", limit: 36, null: false
    t.string "wave_name", null: false
    t.time "delivery_start_time", null: false
    t.time "delivery_end_time", null: false
    t.time "pick_cut_off_time", null: false
    t.string "pick_cut_off_day"
    t.string "organization_region_id", null: false
    t.string "status", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "created_by"
    t.string "updated_by"
    t.time "pick_up_start_time"
    t.time "pick_up_end_time"
    t.string "serviceable_area"
    t.index ["id"], name: "delivery_waves_id_key", unique: true
  end

  create_table "dis_schema_history", primary_key: "installed_rank", id: :integer, default: nil, force: :cascade do |t|
    t.string "version", limit: 50
    t.string "description", limit: 200, null: false
    t.string "type", limit: 20, null: false
    t.string "script", limit: 1000, null: false
    t.integer "checksum"
    t.string "installed_by", limit: 100, null: false
    t.datetime "installed_on", precision: nil, default: -> { "now()" }, null: false
    t.integer "execution_time", null: false
    t.boolean "success", null: false
    t.index ["success"], name: "dis_schema_history_s_idx"
  end

  create_table "incoming_goods_details", id: { type: :string, limit: 255 }, force: :cascade do |t|
    t.datetime "created_at", precision: nil, default: -> { "now()" }
    t.datetime "updated_at", precision: nil, default: -> { "now()" }
    t.string "entity_code", limit: 255
    t.text "pulpo_payload"
    t.string "queue_subscription_error", limit: 255
    t.boolean "status"
    t.string "pulpo_message_id", limit: 255
  end

  create_table "logistics_order_details", id: { type: :string, limit: 36 }, force: :cascade do |t|
    t.string "sales_order_number"
    t.string "loginext_reference_id"
    t.jsonb "loginext_route_planning_json"
    t.jsonb "loginext_shipped_json"
    t.jsonb "loginext_delivered_json"
    t.jsonb "pulpo_package_json"
    t.jsonb "update_order_json"
    t.string "order_status"
    t.string "source"
    t.string "destination"
    t.boolean "is_message_sent", default: false
    t.string "trip_name"
    t.string "distribution_center"
    t.string "deliver_branch"
    t.datetime "deliver_end_time", precision: nil
    t.string "agent_phone_no"
    t.string "return_branch"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.jsonb "loginext_packed_json"
    t.integer "pulpo_sales_order_id"
    t.jsonb "pulpo_create_sales_order_response_json"
    t.jsonb "pulpo_create_sales_order_request_json"
    t.boolean "is_trip_sent_to_oms", default: false, null: false
    t.string "business_model"
    t.string "delivery_agent_phone_no"
    t.boolean "is_deliver"
    t.boolean "is_pickup"
    t.string "pickup_agent_phone_no"
    t.datetime "pickup_end_time", precision: nil
    t.datetime "pickup_start_time", precision: nil
    t.boolean "is_shipment_created"
    t.jsonb "loginext_shipment_update_json_to_retry"
    t.index ["sales_order_number"], name: "log_ord_det_ord_num_idx", unique: true
    t.index ["sales_order_number"], name: "sales_order_number_uniq_002", unique: true
  end

  create_table "order_delivery_schedules", id: false, force: :cascade do |t|
    t.string "id", limit: 36, null: false
    t.string "delivery_wave_id", limit: 36
    t.jsonb "sales_order", null: false
    t.string "sales_order_id", limit: 36, null: false
    t.string "loginext_order_id"
    t.string "organization_region_id", limit: 36, null: false
    t.boolean "loginext_status", default: false
    t.boolean "order_status", default: true
    t.datetime "expected_delivery_time", precision: nil
    t.datetime "order_created_at", precision: nil, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "sales_order_number"
    t.boolean "is_order_cancelled"
    t.index ["id"], name: "order_delivery_schedules_id_key", unique: true
    t.index ["sales_order_number"], name: "sales_order_number_uniq", unique: true
  end

  create_table "pickup_order_details", id: { type: :string, limit: 36 }, force: :cascade do |t|
    t.string "sales_order_number", null: false
    t.string "loginext_reference_id"
    t.string "trip_name"
    t.string "agent_phone_no"
    t.jsonb "loginext_route_planning_json"
    t.jsonb "start_trip_json"
    t.jsonb "order_picked_up_json"
    t.jsonb "order_delivered_json"
    t.string "status"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["sales_order_number"], name: "pickup_ord_det_ord_num_idx", unique: true
    t.index ["sales_order_number"], name: "sales_order_number_uniq_001", unique: true
  end

  create_table "pickup_order_details_aud", id: false, force: :cascade do |t|
    t.string "id", limit: 36, null: false
    t.integer "rev", null: false
    t.integer "revtype", limit: 2
    t.string "sales_order_number", null: false
    t.string "loginext_reference_id"
    t.string "trip_name"
    t.string "agent_phone_no"
    t.string "status"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "product_sync_details", id: false, force: :cascade do |t|
    t.string "id", limit: 36, null: false
    t.jsonb "request"
    t.jsonb "response"
    t.string "product_id", limit: 36
    t.string "sku", limit: 36
    t.string "pulpo_product_id", limit: 36
    t.string "country_code", limit: 36
    t.boolean "status"
    t.string "pulpo_status", limit: 36
    t.string "pulpo_error_message"
    t.datetime "sent_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["product_id"], name: "product_id_index"
    t.index ["product_id"], name: "product_unique", unique: true
    t.index ["sku"], name: "sku_index"
  end

  create_table "purchase_order_sync_details", id: false, force: :cascade do |t|
    t.string "id", limit: 36, null: false
    t.jsonb "request"
    t.string "pulpo_purchase_order_id", limit: 36
    t.string "purchase_order_id"
    t.string "pulpo_warehouse_id", limit: 36
    t.string "country_code", limit: 36
    t.string "pulpo_status", limit: 36
    t.string "queue_subscription_error"
    t.string "pulpo_response_body"
    t.datetime "sent_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "type", limit: 36
    t.index ["purchase_order_id"], name: "purchase_order_sync_details_order_number", unique: true
  end

  create_table "revinfo", primary_key: "rev", id: :integer, default: nil, force: :cascade do |t|
    t.bigint "revtstmp"
  end

  create_table "shedlock", primary_key: "name", id: { type: :string, limit: 64 }, force: :cascade do |t|
    t.datetime "lock_until", precision: 3
    t.datetime "locked_at", precision: 3
    t.string "locked_by", limit: 255
  end

  create_table "supplier_sync_details", id: false, force: :cascade do |t|
    t.string "id", limit: 36, null: false
    t.jsonb "request"
    t.string "supplier_id", limit: 36
    t.string "pulpo_supplier_id", limit: 36
    t.string "country_code", limit: 36
    t.string "tenant_id", limit: 36
    t.boolean "status"
    t.string "pulpo_status", limit: 36
    t.string "queue_subscription_error"
    t.string "pulpo_response_body"
    t.datetime "sent_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["supplier_id", "country_code"], name: "supplier_id_country_code", unique: true
    t.index ["supplier_id", "country_code"], name: "supplier_id_country_code_index", unique: true
  end

  add_foreign_key "pickup_order_details_aud", "revinfo", column: "rev", primary_key: "rev", name: "pickup_order_details_revinfo_fkey"
end
