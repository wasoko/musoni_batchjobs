import sys
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import csv
import click

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def adjust_payment(loan_id, transaction_id, date, amount, reason):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'{loan_id}/transactions/{transaction_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = {
        "locale": "en_GB",
        "dateFormat": "dd MMMM yyyy",
        "transactionDate": date,
        "transactionAmount": amount,
        "note": reason
    }
    response = response.post(url, headers=headers, auth=auth, params = {},json=payload)
    logging.info("response received %s", response.json())
    if response.status != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def search_loan(external_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'search'
    params = {'query': external_id}
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.get(url, headers=headers, auth=auth, params = params,json={})
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()[0]['entityId']

def get_loan_details(loan_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.get(url, headers=headers, auth=auth, params = {},json={})
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def post_payment(loan_id, amount, payment_date, note):
    logging.info("posting the payment to loan %s %s %s", loan_id, amount, payment_date)
    headers = MUSONI_HEADERS
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    url = MUSONI_BASE_URL + 'loans/{}/transactions'.format(loan_id)
    params = {'command': 'repayment'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "receiptNumber": "DUMMY_PAYMENT_MATCH_IMS_APR_24",
        "transactionDate": payment_date,
        "transactionAmount": amount,
        "paymentTypeId": "218",
        "note": note,
        }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def process_record(invoice_number, amount, payment_date, note):
    loan_id = search_loan(invoice_number)
    loan_details = get_loan_details(loan_id)
    # if amount != loan_details['summary']['totalOutstanding']:
    #     if amount<=0:
    #         if loan_details['summary']['totalOutstanding'] > 0:
    #             balance =  loan_details['summary']['totalOutstanding']
    #     else:
    #         balance = loan_details['summary']['totalOutstanding'] - amount
    #     logging.info("transaction amount to be posted to loan %s %s", loan_id, balance)
    post_payment(loan_id, amount, payment_date, note)
    loan_details_after_update = get_loan_details(loan_id)
    return loan_details_after_update

def process_record_with_loan_id(loan_id, amount, payment_date, note):
    post_payment(loan_id, amount, payment_date, note)
    loan_details_after_update = get_loan_details(loan_id)
    return loan_details_after_update

@click.group()
def commands():
    pass

@click.command()
def process_correction():
    invoice_number = 'ke_cinv_1487836'
    amount = 41446
    payment_date = '31 Mar 2022'
    note = 'correcting amount as of 31 Mar 2022'
    process_record(invoice_number, amount, payment_date, note)


@click.command()
@click.argument('file-path')
def process_csv_file(file_path):
    error_count = 0
    success_count = 0
    total_records_to_process = 0
    error_records = []
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            total_records_to_process =len(list(reader))
            fp.seek(0)
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    loan_details = process_record_with_loan_id(record['Account No'], int(record['final_correction_amount']), record['payment_date'], record['note'])
                    success_count = success_count + 1
                    record['outstanding_after_payment'] = loan_details['summary']['totalOutstanding']
                    record['status'] = 'success'
                except:
                    error_count = error_count + 1
                    error_records.append(record)
                    record['outstanding_after_payment'] = 0
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    
    if output_record:
            writer = csv.DictWriter(open("loan_correction_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)
    
commands.add_command(process_csv_file)
commands.add_command(process_correction)

if __name__ == '__main__':
    commands()