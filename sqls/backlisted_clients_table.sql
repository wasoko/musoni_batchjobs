  CREATE TABLE IF NOT EXISTS fs_blacklisted_clients (
        id character varying(36) COLLATE pg_catalog."default" NOT NULL,
        client_ims_id varchar(100) NOT NULL UNIQUE,
        client_musoni_id  int NOT NULL UNIQUE,
        reason varchar(200),
        source varchar(50),
        created_at date,
        updated_at date
      );