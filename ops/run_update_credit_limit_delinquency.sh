#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/reinitiate_credit_limit_ims.py $CLIENT_ID $BALANCE $D_FLAG
popd
