#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
    echo "ARGS is passed runing for" 
    echo ${DATE_OF_CHANGE} ${SYSTEM} ${TRIGGERED_BY}
    /usr/bin/python3 jobs/update_cl_delinquency_in_IMS_DB.py update_IMS_DB credit_limit_update.csv
popd
