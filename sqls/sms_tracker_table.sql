CREATE TABLE IF NOT EXISTS public.musoni_sms_tracker
(
    id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    mobile_number character varying(20) COLLATE pg_catalog."default" NOT NULL,
    message character varying COLLATE pg_catalog."default",
    template_name character varying(50) COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default",
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);

