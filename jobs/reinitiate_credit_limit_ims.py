import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import zipfile
import io
import json
import datetime
import time
import psycopg2
from io import StringIO
import csv
import click
import numpy as np
import logging
import os
import sys
import warnings
from config import *
from crm_connector import get_auth_token, get_customer_details, update_customer_details

warnings.filterwarnings("ignore")  

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout),logging.FileHandler('AL_update_MusoniIms_v2.log')])

auth_response  = get_auth_token()
token = auth_response['token_details']['access_token']

error_log = []

@click.command
@click.option("--customer-id", prompt=True)
@click.option("--credit-limit", prompt=True)
@click.option("--delinquency", default=0, prompt=True)
def update_credit_limit(customer_id, credit_limit, delinquency):
    logging.info("updating the customer credit limit %s %s", customer_id, credit_limit)
    try:
        customer_details = get_customer_details(token, customer_id)
        customer_details['credit_limit']= credit_limit
        customer_details['delinquency'] = delinquency
        customer_details['language_id'] = customer_details['language']['id']
        customer_details['organization_location_id'] = customer_details['organization_location']['id']
        response = update_customer_details(token, customer_id, customer_details)
        logging.info("updated the credit for the customer response %s", response)
    except:
        logging.exception("error in updating the credit limit for the customer %s", customer_id)

if __name__ == '__main__':
   update_credit_limit()