import json
import requests
from config import *
from common_auth_login import *
import sys

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def get_customer_details(auth_token, customer_id):
    url  = CRM_CUSTOMER_API_URL + customer_id
    headers = {"Authorization": "Bearer " + auth_token}
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        raise Exception('error in getting customer_details ' + str(response.json()))
    return response.json()

def update_customer_details(auth_token, customer_id, payload):
    url  = CRM_CUSTOMER_API_URL + customer_id
    headers = {"Authorization": "Bearer " + auth_token}
    logger.info('payload is %s', json.dumps(payload))
    response = requests.patch(url, headers=headers, json=payload)
    if response.status_code != 200:
        raise Exception('error in updating customer_details ' + str(response.json()))
    return response.json()

def update_penalty_fees(payload):
    url = OMS_PENALTY_FEE_URL
    headers = {"Authorization": OMS_PENALTY_FEE_TOKEN}
    logger.info('payload is %s', json.dumps(payload))
    response = requests.post(url, headers=headers, json=payload)
    if not response.ok:
        raise Exception('error in updating customer_details ' + str(response.json()))
    return response.json()