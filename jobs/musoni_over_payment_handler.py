from datetime import datetime
import logging
import zipfile
import time
import csv
import sys
import io
from io import StringIO
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import click
from rich.prompt import Prompt
from rich.progress import track
from rich.console import Console
from rich.table import Table
from config import *

AUDIT_REPORT_CSV = "over_payment_allocation.csv"

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def fetch_data(method, relative_url, params=None, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.status_code)
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + response.json()['message'])
    return response.json()

def get_all_saving_account_positive_balance():
    logging.info("fetching all client details")
    data_export_id = MUSONI_SA_WITH_POSITIVE_BALANCE_ID
    client_det  = fetch_data("GET",'data-export/prepared/addToQueue',{"tenantIdentifier":MUSONI_TENANT_ID,"exportId": data_export_id,"fileFormat":"csv"})
    logging.info("report queued")
    time.sleep(5)
    if client_det:
        response = fetch_data("GET",'data-export/prepared/{0}/attachment/direct-link'.format(str(client_det)))
        if response:
            if '.zip' in response['value']:
                r = requests.get(response['value'])
                zf = zipfile.ZipFile(io.BytesIO(r.content))
                file = zf.namelist()[0]
                all_accounts = pd.read_csv(zf.open(file),delimiter = ';')
            else:
                all_accounts = pd.read_csv(response['value'],delimiter=';')
    logging.info("report downloaded")
    return all_accounts

def fetch_all_loans_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?'.format(client_id)
    method = 'GET'
    response  = fetch_data(method, relative_url)
    return response.get('loanAccounts', [])

def get_earliest_unpaid_loan(client_id):
    logging.info("getting the oldest active loan  for client %s", client_id)
    all_loans = fetch_all_loans_for_client(client_id)
    active_loans = [loan for loan in all_loans if loan['status']['active']]
    loans_order_by_id = sorted(active_loans, key=lambda loan: loan['id'])
    return loans_order_by_id[0] if loans_order_by_id else None

def withdraw_amount(saving_account_no, amount):
    relative_url = 'savingaccounts/{}/transactions?'.format(saving_account_no)
    method = 'POST'
    params = {'command': 'withdrawal'}
    payload={
        "locale": "en",
        "dateFormat": "dd MMMM yyyy",
        "transactionDate": datetime.now().strftime(COMMON_DATE_FORMAT_CONVERSION),
        "transactionAmount": amount,
        "paymentTypeId": "14",
    }
    response  = fetch_data(method, relative_url, params=params, payload=payload)
    return True

def repay_loans(client_id, amount):
    all_loans = fetch_all_loans_for_client(client_id)
    active_loans = [loan for loan in all_loans if loan['status']['active']]
    loans_order_by_id = sorted(active_loans, key=lambda loan: loan['id'])
    loans_amount_payed = []
    for index, loan in enumerate(loans_order_by_id):
        loan_balance = loan['loanBalance']
        if loan_balance < amount and not loan['id'] == loans_order_by_id[-1]['id']:
            make_loan_repayment(loan['id'], loan_balance)
            amount = amount - loan_balance
            loans_amount_payed.append({'id': loan['id'], 'amount':loan_balance})
        else:
            make_loan_repayment(loan['id'], amount)
            loans_amount_payed.append({'id': loan['id'], 'amount':amount})
            amount = 0
        if not amount:
            break
    return loans_amount_payed

def display_result(final_results, success, error):
    table = Table(title="Match data")
    table.add_column("Confirmation Code", justify="center", style="cyan",)
    table.add_column("Amount",justify="center")
    table.add_column("Invoice", justify="center")
    table.add_column("Customer", justify="center")
    table.add_column("Reason", justify="center")
    for record in final_results:
        table.add_row(str(record['code']), str(record['amount']), str(record['invoice']), str(record['customer']), str(record['reason']))
    console = Console()
    console.print(table)

def is_repayment(record):
    return True if record['Total Repaid'] else False

@click.group()
def commands():
    pass


@click.command()
def do_payment_audit():
    success_count = 0
    error_count = 0
    final_results = []
    error_records = []
    try:
        for record in get_all_saving_account_positive_balance().to_dict("records"):
            logging.info("processing record %s", record)
            try:
                client_id = record['Client Id']
                saving_account_no = record['Account No']
                balance_amount = record['Account Balance Derived']
                result = withdraw_amount(saving_account_no, balance_amount)
                if result:
                    loans = repay_loans(client_id, balance_amount)
                logging.info("amount transferred details %s %s %s", client_id, loans)
                success_count = success_count + 1
            except:
                error_count = error_count + 1
                error_records.append(record)
                logging.exception("error in processing the record %s", record)
    except:
        logging.exception("error in processing file")
    finally:
        csv_header = ['code', 'amount', 'invoice', 'customer', 'reason']
        if final_results:
            writer = csv.DictWriter(open(AUDIT_REPORT_CSV, "w"), fieldnames=csv_header)
            writer.writeheader()
            writer.writerows(final_results)
        display_result(final_results, success_count, error_count)


commands.add_command(do_payment_audit)


if __name__ == '__main__':
   commands() 
   # print(get_all_repayments().to_dict("records"))