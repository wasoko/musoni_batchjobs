import os

DB_CONNECTION_HOST = 'ims-db.local'
DB_CONNECTION_DB = 'prd'
DB_CONNECTION_PORT = '5432'
DB_CONNECTION_USERNAME = 'doadmin'
DB_CONNECTION_PASSWORD = 'ui5joots85vxjf9z'

READ_DB_CONNECTION_HOST = 'ims-db.local'
READ_DB_CONNECTION_DB = 'prd'
READ_DB_CONNECTION_PORT = '5432'
READ_DB_CONNECTION_USERNAME = 'doadmin'
READ_DB_CONNECTION_PASSWORD = 'ui5joots85vxjf9z'

MUSONI_DB_CONNECTION_USERNAME = 'musoni'
MUSONI_DB_CONNECTION_PASSWORD = 'zzJjncAEHta7Blgt'
MUSONI_DB_CONNECTION_HOST = 'npi-db.local'
MUSONI_DB_CONNECTION_PORT = '3306'
MUSONI_DB_CONNECTION_DB = 'musoni'

# READ_DB_CONNECTION_HOST = 'db-backup-12am-eat-9feb-do-user-6348608-0.b.db.ondigitalocean.com'
# READ_DB_CONNECTION_DB = 'sokowatch_erp_production'
# READ_DB_CONNECTION_PORT = '25060'
# READ_DB_CONNECTION_USERNAME = 'doadmin'
# READ_DB_CONNECTION_PASSWORD = 'ui5joots85vxjf9z'

CREDIT_TILL_NUMBERS = '239356'
 
MUSONI_USERNAME = 'MusoniIntegrationJob'
MUSONI_PASSWORD = 'Musoni@123'

BASE_WASOKO_API_URL = 'https://api.wasoko.cloud/'

MUSONI_BASE_URL = 'https://api.live.irl.musoniservices.com/v1/'

MUSONI_API_KEY = "pjlee5DUiZ1IwonJOmJcP4nu6DhJcljq5a9q4VwX"
MUSONI_TENANT_ID = 'wasoko'
MUSONI_HEADERS = {
    'X-Fineract-Platform-TenantId': MUSONI_TENANT_ID,
    'x-api-key': MUSONI_API_KEY
}

CI_AUTH_URL= 'https://api.wasoko.cloud/security/auth/login'
CI_AUTH_USERNAME =  'MusoniIntegrationJob@wasoko.com'
CI_AUTH_PASSWORD= 'Musoni@123'
CI_AUTH_APPLICATIONID = 'b5318e50-2192-4647-9c90-1e569f49fe69'
CRM_CUSTOMER_API_URL = "https://api.wasoko.cloud/crm/v1/customers/"
OMS_PENALTY_FEE_URL = "https://api.wasoko.cloud/oms/penalty_fee"
OMS_PENALTY_FEE_TOKEN = '$2a$12$qDsWBWC8H0W3bnFrgzXYWuuWnulL9453c1/EsII1QDhPSGXM.j4N.'

NOTIFIER_URL= 'https://api.wasoko.cloud/notifier/notify'
SANDBOX_SMS = False


MUSONI_PAYMENT_AUDIT_DATA_EXPORT_ID = 19
MUSONI_SA_WITH_POSITIVE_BALANCE_ID = 22
UPDATE_CREDIT_LIMIT_DATA_EXPORT_CLIENTS = 25
UPDATE_CREDIT_LIMIT_DATA_EXPORT_LOANS = 24
UPDATE_AVAILABLE_LIMIT_TO_MUSONI = True
UPDATE_AVAILABLE_LIMIT_TO_IMS = False
UPDATE_DELINQUENCY_TO_MUSONI = True
UPDATE_DELINQUENCY_TO_IMS = True
BRANCH_FILTER_LIST = [22,23]


DEFAULT_STAFF_FOR_COUNTRY = {
    "kenya": 55, #William 
    "côte d'ivoire": 57, #Stanley
    "uganda": 16, #Moses
    "rwanda": 20, #Jasmine
    "tanzania": 66, #Victor
}
CLIENTS_SKIP_FOR_CLOSE = [120324, 139498, 139497]
CLOSE_CLIENT_FILTER_TEXT = 'Close Client'

BLACKLIST_WRITEOFF_REASON_CODES = [457, 458, 459, 462, ]

OFFICES_ALLOWED_IMS_UPDATE = 'all'

FS_PAYMENT_BASE_URL = "https://api.wasoko.cloud/fs-musoni-payments-integration/"
FS_PAYMENT_API_URL = f'{FS_PAYMENT_BASE_URL}payments'
FS_PAYMENT_ALLOCATION_API_URL = f'{FS_PAYMENT_BASE_URL}paymentAllocation'
FS_PAYMENT_API_KEY = "$2a$10$JqIdKrRpzlUq6ONz5XWToupVMMc00fcip4zFBlSHa09.as827RwRa"



TZ_PENALTY_LOAN_PRODUCT_ID_LIST = [29, 14]
COUNTRY_CHARGES_ID_MAP = {
    "TZS": "12"
}

COUNTRY_PRODUCT_ID_MAP = {
    "TZS": [29, 14],
    'KES': 'all',
    'RWF': 'all',
    'XOF': 'all',
    'UGX': 'all',
}

COUNTRY_SMS_ENABLED_BRANCHES = {
    'XOF': 'all',
    'UGX': 'all',
}


CLIENT_PROFILE_FIELD_MAPPING = {
        'credit_limit': 'Credit_Limit10',
        'available_limit':'Available_Credi11',
        'delinquency':'Delinquency12',
        'client_id': 'client_id',
        'date': 'submittedon_date',
        'blacklist_reason': 'Blacklist_Reaso13',
        'virtual_account_no': 'Virtual_Account14',
        'loan_product_id': 'Kenya_Loan_Product_Assignment_cd_Loan_Product_As9',
}

ENABLE_BLACKLIST_CHECK = True

ENABLE_PENALTY_REPORT_TO_RA = True


LOAN_TO_PRODUCT_ID_MAP = {
    # kenya
    401: 'e37a8849-9185-4c00-a4f6-2d925cff20c9',
    402: '3d3525af-bb21-433e-bd29-bfa4e0c71a15',
    403: '0bea52b4-a79d-4d39-9a00-5c8b7ce0ca51',
    404: 'b78625a8-d7b8-481b-a96f-c04460fffb06',
    # rwanda
    468: '5bfabaad-191c-4a7a-aea6-c5fa5d21e6c1',
    469: '23eda8e2-2919-4935-9637-4391fb72cdf6',
    470: '85594eef-026f-4c55-8367-29478fcd8343',
    # uganda
    464: 'ea0ff478-14cb-48f9-99e7-c2c8926178d4',
    465: '2c755688-2133-4895-bbb5-0805b9c60d73',
    466: 'b01fcd9b-ec60-4874-a6c2-18f0c89cbcf7',
    467: 'cc11ae14-25a5-48ff-9150-cf6e5ff95082',
    # Côte d''Ivoire
    445: '11d975a8-327c-458a-be44-694c65a78278',
    # tanzania
    # 471: '',
    # 473: '',
    477: '7101071f-3767-44d0-97c9-87450c590b17',
    # 477: '',
    # 478: ''
}

BRANCH_OFFICE_IDS_MAP = {
    1: {
        'name': 'Nairobi East',
        'office_id': 6
    },
    2: {
        'name': 'Thika',
        'office_id': 10
    },
    3: {
        'name': 'Ruyenzi',
        'office_id': 30
    },
    4: {
        'name': 'Kigali',
        'office_id': 14
    },
    5: {
        'name': 'Dar es Salaam East',
        'office_id': 16
    },
    6: {
        'name': 'Dar es Salaam West',
        'office_id': 17
    },
    7: {
        'name': 'Dar es Salaam Central',
        'office_id': 17
    }
}

FIELD_REP_ROLE = [15]

CHECK_LATEST_MUSONI_DATA_DUMP_AVAILABLE = False

WEBHOOK_URL = "https://chat.googleapis.com/v1/spaces/AAAAOHjA_tA/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&" \
              "token=CDicGfMGE4dkkz_WwEW2dhmHgFHy5qwJSLqS2D5wjIk"