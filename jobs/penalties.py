import uuid
import warnings
import click
from datetime import datetime, timedelta
from sqlalchemy import create_engine
import psycopg2.extras
from musoni_utils import *
from crm_connector import update_penalty_fees


psycopg2.extras.register_uuid()

warnings.filterwarnings("ignore")

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout), logging.FileHandler('AL_update_MusoniIms_v2.log')])

error_log = []

conn = psycopg2.connect(
    database=DB_CONNECTION_DB,
    user=DB_CONNECTION_USERNAME,
    password=DB_CONNECTION_PASSWORD,
    host=DB_CONNECTION_HOST,
    port=DB_CONNECTION_PORT
)

engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                host=MUSONI_DB_CONNECTION_HOST,
                                                                                port=MUSONI_DB_CONNECTION_PORT,
                                                                                db=MUSONI_DB_CONNECTION_DB))
DPD_MAP = {
    "-1": "PLUS_1",
    "-8": "PLUS_8",
    "-15": "PLUS_15"
}

COUNTRY_PENALTIES_PERCENTAGE_MAP = {
    "TZ_DPD_PLUS_1": "1",
    "TZ_DPD_PLUS_8": "1",
    "TZ_DPD_PLUS_15": "2.5"
}

COUNTRY_CURRENCY_LIST_TO_PROCESS = {'TZS'}



def get_non_reported_records_for_days(interval_days=0):
    start_date = (datetime.now() - timedelta(days=interval_days)).strftime("%Y-%m-%d")
    logging.info("fetching penalities on and after %s", start_date)
    query = "select * from fs_penalties_tracker where status = true and ra_track_id is null and date(created_at)>=%s"
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute(query, (start_date,))
    return cursor.fetchall()

def get_loan_details(loan_id):
    query = '''select * from m_loan where id=%(loan_id)s'''
    records = pd.read_sql_query(query, engine, params={'loan_id': loan_id})  # type: ignore
    logging.info("records count: %s", len(records))
    return records.to_dict("records")

def get_penalty_template(record, DPD):
    template_name = ''
    if record['currency_code'] == 'TZS':
        template_name = "TZ_" + DPD
    return template_name


def insert_penalty_tracker_table(loan_id, template_name, principal_outstanding_derived, principal_amount,
                                 penalty_percentage, due_date, charge_id, musoni_id, message, status):
    logging.info("updating penalty tracker table with following detail %s", (loan_id, template_name,
                                                                             principal_outstanding_derived,
                                                                             principal_amount, penalty_percentage,
                                                                             due_date, charge_id, message, status))
    cursor = conn.cursor()
    insert_query = '''insert into 
    fs_penalties_tracker(id, loan_id, template_name, principal_outstanding_derived, principal_amount, penalty_percentage, due_date, charge_id, message, status, created_at, updated_at, musoni_track_id) 
    values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    '''
    cursor.execute(insert_query,
                   (uuid.uuid4(), loan_id, template_name, principal_outstanding_derived, principal_amount,
                    penalty_percentage, due_date, charge_id, message, status, datetime.now(), datetime.now(), musoni_id))
    conn.commit()

def update_penalty_tracker_table(id, penalty_track_id):
    cursor = conn.cursor()
    try:
        update_query = "update fs_penalties_tracker set ra_track_id=%s, updated_at=%s where id=%s"
        cursor.execute(update_query, (penalty_track_id, datetime.now(), id))
        conn.commit()
    except:
        logging.exception("error in updating the table")
        conn.rollback()

def apply_penalty(record, penalty_percentage, charge_id):
    logging.info("Applying charges for loan Id: %s %s %s", record['id'], penalty_percentage, charge_id)
    payload = {
        "charge_id": charge_id,
        "amount_or_percentage": penalty_percentage,
        "due_date": str(record['expected_maturedon_date'])
    }
    response = apply_charges(record['id'], payload)
    return response

def is_penalty_applied(loan_id, penalty_template_name):
    logging.info("checking penalty applied to the loan already %s", (loan_id, penalty_template_name))
    cursor = conn.cursor()
    query = "select * from fs_penalties_tracker where loan_id=%s and template_name=%s and status=true";
    cursor.execute(query, (loan_id, penalty_template_name))
    return True if cursor.rowcount > 0 else False

def process_records(records, DPD):
    for index, record in records.iterrows():
        try:
            penalty_template_name = get_penalty_template(record, DPD)
          
            penalty_percentage = COUNTRY_PENALTIES_PERCENTAGE_MAP.get(penalty_template_name)
            charge_id = COUNTRY_CHARGES_ID_MAP.get(record['currency_code'])
            logging.info("Penalty Template: %s and Penalty Percentage: %s", penalty_template_name, penalty_percentage)
            if is_penalty_applied(record['id'], penalty_template_name):
               logging.info("penalty processed already for the loan, skipping %s", record['id'])
               raise Exception('Penalty applied processed')
            response_json = apply_penalty(record, penalty_percentage, charge_id)
            if response_json.get('resourceId'):
                logging.info("Penalty applied successfully for loan: %s", record['id'])
                insert_penalty_tracker_table(record['id'], penalty_template_name, record['principal_outstanding_derived'],
                                             record['principal_amount'], penalty_percentage,
                                             record['expected_maturedon_date'], charge_id,
                                             response_json['resourceId'], 'Penalty applied successfully', True)
            else:
                logging.info("error in applying penalty for the loan: %s", record['id'])
                insert_penalty_tracker_table(record['id'], penalty_template_name, record['principal_outstanding_derived'],
                                             record['principal_amount'], penalty_percentage,
                                             record['expected_maturedon_date'], charge_id, None,
                                             response_json['developerMessage'], False)
                
        except Exception as ex:
            logging.exception("exception while applying penalty for the loan: %s", record['id'])
            insert_penalty_tracker_table(record['id'], 'error', record['principal_outstanding_derived'],
                                         record['principal_amount'], '0',
                                         record['expected_maturedon_date'], '0', None, str(ex), False)


def fetch_and_process_penalties(currency_code_list, time_delta_in_days, DPD, from_date):
    logging.info("getting records eligible for penalties %s", (currency_code_list, time_delta_in_days, DPD, from_date, TZ_PENALTY_LOAN_PRODUCT_ID_LIST))
    date = (from_date + timedelta(days=time_delta_in_days)).strftime("%Y-%m-%d")
    logging.info("picking records for the date %s and DPD %s ", date, time_delta_in_days)
    query = '''select ml.id, ml.external_id, ml.office_id, ml.product_id, ml.currency_code, 
    ml.principal_outstanding_derived, ml.principal_amount, ml.expected_maturedon_date from m_loan ml 
    where ml.principal_outstanding_derived > 0 and currency_code in %(currency_list)s and loan_status_id != 600 and 
    expected_maturedon_date =  %(maturity_date)s and ml.product_id in %(product_Id_list)s'''
    records = pd.read_sql_query(query, engine, params={'currency_list': currency_code_list, 'maturity_date': date,
                                                       'product_Id_list': TZ_PENALTY_LOAN_PRODUCT_ID_LIST})  # type: ignore
    logging.info("records count: %s", len(records))
    process_records(records, DPD)

@click.command
@click.option('--run-date', default=datetime.today().strftime("%Y-%m-%d"))
def apply_penalities(run_date):
    try:
        from_date = datetime.strptime(run_date, "%Y-%m-%d")
        for dpd, dpd_value in DPD_MAP.items():
            logging.info("Processing Penalties for customers having DPD_" + dpd_value)
            fetch_and_process_penalties(COUNTRY_CURRENCY_LIST_TO_PROCESS, int(dpd), 'DPD_' + dpd_value, from_date)
    except Exception as e:
        error_log.append(str(e))

    if len(error_log) >= 1:
        logging.info("Summary : \n\n Script ran with the following errors -- %s", (" ".join(error_log)))
        sys.exit(1)
    else:
        logging.info("Script ran successfully")
        sys.exit(0)

@click.command
def report_penalities_to_tax_authority():
    if not ENABLE_PENALTY_REPORT_TO_RA:
        logging.info("Penalty reporting to RA is disabled exiting")
        return
    records_to_process = get_non_reported_records_for_days()
    logging.info("no of records to process %s", len(records_to_process))
    for record in records_to_process:
        try:
            logging.info("reporting to tax authority %s", record)
            loan_details = get_loan_details(record['loan_id'])
            logging.info("reporting to tax authority %s", loan_details)
            penalty_amount = record['principal_outstanding_derived'] * (record['penalty_percentage'] / 100)
            payload = [
                    {
                        "originalInvoiceNumber": loan_details[0]['external_id'],
                        "overdueDays": record['template_name'].split('_')[-1],
                        "penaltyFee": penalty_amount,
                        "penaltyType": "PENALTY"
                    }

                ]
            logging.info("payload for the reporting to tax authority %s", payload)
            response = update_penalty_fees(payload)
            logging.info("response from the oms penalty fee API %s", response)
            update_penalty_tracker_table(record['id'], response['penaltyFeeInvoiceId'][0])
        except:
            logging.exception("error in reporting penalty for the record %s", record['loan_id'])
    


@click.group            
def commands():
    pass


commands.add_command(apply_penalities, "apply-penalties")
commands.add_command(report_penalities_to_tax_authority, "report-penalties")

if __name__ == "__main__":
    commands()
