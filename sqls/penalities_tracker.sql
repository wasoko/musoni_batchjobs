CREATE TABLE IF NOT EXISTS public.fs_penalties_tracker
(
    id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    loan_id int NOT NULL,
    template_name character varying(50) COLLATE pg_catalog."default",
    total_outstanding_derived double precision ,
    principal_amount double precision ,
    penalty_percentage double precision ,
    due_date date,
    charge_id int,
    message character varying COLLATE pg_catalog."default",
    status BOOLEAN NOT NULL DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);

ALTER TABLE fs_penalties_tracker RENAME COLUMN total_outstanding_derived TO principal_outstanding_derived;
ALTER TABLE fs_penalties_tracker ADD COLUMN ra_track_id character varying(36);
ALTER TABLE fs_penalties_tracker ADD COLUMN musoni_track_id integer;
ALTER TABLE fs_penalties_tracker ADD CONSTRAINT fs_penalties_track_pk PRIMARY KEY (id);