import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import zipfile
import io
import json
import datetime
import time
import psycopg2
from io import StringIO
import csv
import numpy as np
import logging
import os
import sys
import warnings
from config import *
from crm_connector import get_auth_token, get_customer_details, update_customer_details

warnings.filterwarnings("ignore")  

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout),logging.FileHandler('AL_update_MusoniIms_v2.log')])

loandet_url = "loans"
# Fetch custom client details for all customers at once using data export api
addto_queue_url = "data-export/prepared/addToQueue" # returns report id
reportdet_url  = "data-export/prepared/{}/attachment/direct-link"
clientdet_url = "clients"
update_clientdet_url = "datatables/ml_client_details/{}"

date = datetime.datetime.now()

logging.info("------- %s --------",str(date))

auth_response  = get_auth_token()
token = auth_response['token_details']['access_token']

error_log = []



def update_credit_limit(customer_id, credit_limit, delinquency):
    logging.info("updating the customer credit limit %s %s", customer_id, credit_limit)
    try:
        customer_details = get_customer_details(token, customer_id)
        customer_details['credit_limit']= credit_limit
        customer_details['delinquency'] = delinquency
        customer_details['language_id'] = customer_details['language']['id']
        customer_details['organization_location_id'] = customer_details['organization_location']['id']
        response = update_customer_details(token, customer_id, customer_details)
        logging.info("updated the credit for the customer response %s", response)
    except:
        logging.exception("error in updating the credit limit for the customer %s", customer_id)



def api_call(method,relativeurl,params=None,data=None):
    url =  MUSONI_BASE_URL+relativeurl
    auth = HTTPBasicAuth(MUSONI_USERNAME , MUSONI_PASSWORD)
    response = requests.request(method,url,headers = MUSONI_HEADERS,auth = auth,params  = params ,data = data)
    logging.info("Received code %s as response to the api call %s",response.status_code,url)
    if response.status_code not in(200,201):
        if method == "PUT" and response.status_code == 404:
            
            response = requests.request("POST",url,headers = MUSONI_HEADERS,auth = auth,params  = params ,data = data)
            logging.info("Received code %s as response to the trial api call %s method(POST) ",response.status_code,url)

        else:                
            raise Exception('API call to the relative URL "{}" returned a status code of {} due to "{}"'.format(relativeurl,response.status_code,response))
    return response.json()


# update available credit limit


def create_payload(client_id,client_data):
        return json.dumps({"Available_Credi11":client_data[client_data['Client Id']==client_id]['Available_Limit'].values[0],"Delinquency12":int(client_data[client_data['Client Id']==client_id]['is_delinquent'].values[0]),"locale":"en_GB"})
    
    
def data_from_file(response):
     if '.zip' in response['value']:
                  r = requests.get(response['value'])
                  zf = zipfile.ZipFile(io.BytesIO(r.content))
                  file = zf.namelist()[0]
                  data = pd.read_csv(zf.open(file),delimiter = ';')
     else:
                  data = pd.read_csv(response['value'],delimiter=';')
     return data

    
def get_client_details():
     logging.info("Fetching all and custom client details")
     client_det  = api_call("GET",addto_queue_url,{"tenantIdentifier":"wasoko","exportId":UPDATE_CREDIT_LIMIT_DATA_EXPORT_CLIENTS,"fileFormat":"csv"})
     time.sleep(20)
     if client_det:
         response = api_call("GET",reportdet_url.format(str(client_det)))
         
         if response:
            client_data = data_from_file(response)
            
     return client_data
    
def calculate_available_limit(loans_per_client,client_data,delinquency_per_client):
    logging.info("Calculating Available Limit")

    client_limit = pd.merge(loans_per_client,client_data[['Client Id','External Id','Client Details - Credit Limit', 'Client Details - Available Credit Limit',"Client Details - Delinquency"]],how='inner',on = 'Client Id')
    client_limit = pd.merge(client_limit[['Client Id', 'External Id', 'Client Details - Credit Limit',
       'Client Details - Available Credit Limit',"Client Details - Delinquency",
       'Outstanding_amount']],delinquency_per_client,how='left',on = 'Client Id')
    
    
    client_limit['Client Details - Credit Limit'] = client_limit['Client Details - Credit Limit'].astype(float).fillna(1)

    # Available Credit Limit = Credit Limit - Current Outstanding balance
    client_limit['AvailableCreditLimit'] = client_limit['Client Details - Credit Limit'] - client_limit['Outstanding_amount'].fillna(0)
    
    # If a customer has outstanding balance then the available limit will be set to 0 else it will be reset to the actual credit limit
    client_limit['Available_Limit'] = client_limit.apply(lambda x:1 if x["Outstanding_amount"] > 0 else x['AvailableCreditLimit'],axis=1) 
    
    #Delinquency12
    client_limit['overdue_amount'] = client_limit['overdue_amount'].fillna(0)
    client_limit['is_delinquent'] = client_limit.apply(lambda x:1 if x['overdue_amount'] >0  else 0,axis=1)

    client_limit['is_delinquency_modified'] = np.where(client_limit['Client Details - Delinquency'] == client_limit['is_delinquent'],False,True)
    
    
    # Compare existing available limit in client data fetched from Musoni with the available limit calculated. If it is modified then set to True
    client_limit['is_limit_changed'] = np.where(client_limit['Client Details - Available Credit Limit'] == client_limit['Available_Limit'],False,True)
    
    # Fetch only client id's whose limit has changed.
    al_client_ids = list(client_limit['Client Id'][client_limit['is_limit_changed']==True].unique())
    delinquency_client_ids = list(client_limit['Client Id'][client_limit['is_delinquency_modified']==True].unique())
    
    return client_limit,al_client_ids,delinquency_client_ids


def update_available_limit():
    
    logging.info("Fetching Loan details from Musoni")
    try:
        start = datetime.datetime.now()
        loan_det  = api_call("GET",addto_queue_url,{"tenantIdentifier":"wasoko","exportId":UPDATE_CREDIT_LIMIT_DATA_EXPORT_LOANS,"fileFormat":"csv"})
        time.sleep(20)
        if loan_det:
            response = api_call("GET",reportdet_url.format(str(loan_det)))
         
            if response:
               loan_data = data_from_file(response)
        print(datetime.datetime.now() - start )

        if not loan_data.empty:
                loan_data['Outstanding_amount'] = loan_data['Principal Outstanding Derived'] .apply(lambda x: 0 if x < 0  else x)
                loans_per_client = loan_data.groupby("Client Id",as_index=False)["Outstanding_amount"].sum()
                
                # Calculate Delinquency
                
             #   delinquency_df = loan_data[loan_data['status.value']=="Overdue"]
                loan_data['overdue_amount'] = loan_data["Principal Overdue Derived"] .apply(lambda x: 0 if x < 0  else x)
                delinquency_per_client = loan_data.groupby("Client Id",as_index=False)["overdue_amount"].sum()
                   
                client_data = get_client_details()
                
                if len(client_data)>=1 and len(loan_data)>=1:
                   client_limit,al_client_ids,delinquency_client_ids = calculate_available_limit(loans_per_client,client_data,delinquency_per_client)
                  
                client_ids = list(set(al_client_ids + delinquency_client_ids))
                for client_id in client_ids:
                    try:
                        
                        response = api_call("PUT",update_clientdet_url.format(str(client_id)),{"tenantIdentifier":"sokowatch","apptable":"m_client"},data = create_payload(client_id,client_limit))                        
                        logging.info("%s",str(response))
                    except Exception as e:
                        logging.exception("Error - %s while updating client data for client id - %s",str(e),str(client_id ))
                 
                return client_limit,client_ids

    except Exception as e:
        raise Exception("Error '{}' caught while processing func : update_available limit()".format(str(e) ))
        
def update_credit_limit_toIMS(client_limit,client_ids):
    logging.info("Inside func: update_credit_limit_toIMS() ")
    try:
        temp = client_limit[["External Id",'Client Id',"Client Details - Available Credit Limit","Available_Limit", 'Client Details - Delinquency','is_delinquent']]
        
        temp = temp[temp['Client Id'].isin(client_ids)]
        temp['Client Id'] = temp['Client Id'].astype('str')
        temp['Client Details - Delinquency'] = temp['Client Details - Delinquency'] .fillna("")
   
    except Exception as e:
        raise Exception("Error {} while updating credit limit to customers table in IMS".format(str(e)))

    for idx,row in temp.iterrows():
        data  = {'customer_id':row['External Id'],"Credit Limit":row['Available_Limit'],'Delinquency':row['is_delinquent']}

        try:
            update_credit_limit(row['External Id'], row['Available_Limit'],row['is_delinquent'])
            logging.info("Updating Credit Limit and Delinquency to %s",str(data) )
        except Exception as e:
            logging.exception("Error '%s' caught while updating credit limit data to %s",str(e),str(data) )

            
         
        


if __name__=="__main__":
    try:
        if UPDATE_AVAILABLE_LIMIT_TO_MUSONI == True:

            client_limit,client_ids = update_available_limit()
            
        # if UPDATE_AVAILABLE_LIMIT_TO_IMS  == True :

        #     update_credit_limit_toIMS(client_limit,client_ids)   

    except Exception as e:
        error_log.append(str(e))
        
   
    if  len(error_log)>=1:
        logging.info("Summary : \n\n Script ran with the following errors -- %s",(" ".join(error_log)))
        sys.exit(1)
    else:
        logging.info("Script ran successfully")
        sys.exit(0)
        


