import logging
import sys
import csv
from typing import Tuple
import click
from sqlalchemy import create_engine
from config import *
from musoni_utils import *
import numpy as np

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


@click.group
def commands():
    pass

def get_all_client_with_positive_balance():
    records = download_report_from_data_export(MUSONI_SA_WITH_POSITIVE_BALANCE_ID)
    return records

def process_record(customer_id):
    payload = {
        'customer_ids': [customer_id, ]
    }
    headers = {"Authorization": FS_PAYMENT_API_KEY}
    response = requests.post(FS_PAYMENT_ALLOCATION_API_URL, headers=headers, json=payload)
    logging.info("posting payment to payments api %s", response.text)
    if response.status_code != 200:
        return False, response.json()
    return True, 'success'

def get_client_with_open_loans(batch_records):
    logging.info("checking open loans check")
    if SKIP_OPEN_LOAN_CHECK:
        client_ids = [str(rec['External Id']) for rec in batch_records if str(rec['External Id']) != 'nan']
        return client_ids
    logging.info("checking musoni db for any open loans")
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user = MUSONI_DB_CONNECTION_USERNAME,pwd = MUSONI_DB_CONNECTION_PASSWORD , host = MUSONI_DB_CONNECTION_HOST,port = MUSONI_DB_CONNECTION_PORT,db = MUSONI_DB_CONNECTION_DB))
    conn = engine.connect()
    client_id_list = [str(rec['Client Id']) for rec in batch_records]
    query = 'select distinct mc.external_id from m_client mc join m_loan ml on mc.id = ml.client_id where ml.total_outstanding_derived > 0 and ml.client_id in %s'
    response = conn.execute(query, set(client_id_list))
    response_list = response.fetchall()
    client_external_id_list = [rec[0] for rec in response_list]
    return client_external_id_list

@click.command
def process_cash_payments():
    records =  get_all_client_with_positive_balance()
    output_records = []
    result_statics = {
        'success': 0,
        'fail': 0,
        'no_open_loans': 0,
        'total_records': len(records)
    }

    logging.info("Total records with positive balance is %s",len(records))

    for i in range(0, len(records), 100):
        logging.info("processing batch %s to %s records",i,i+99)
        batch_records = records[i:i+100]
        logging.info("batch record : %s",batch_records)
        try:
            batch_client = get_client_with_open_loans(batch_records)
            logging.info("batch clients : %s",batch_client)
            for external_id in batch_client:
                response =  None
                if not external_id:
                    logging.info("external ID is missing skipping this record")
                    continue
                
                logging.info('processing record %s', external_id)

                try:
                    response, message = process_record(external_id)
                    logging.info('record processed and response are %s %s', response, message)
                except:
                    logging.exception('error in processing record %s', external_id)    
                
                if response:
                    result_statics['success'] = result_statics['success'] + 1
                    record = {'external_id': external_id,'status':'success'}
                    output_records.append(record)
                else:
                    result_statics['fail'] = result_statics['fail'] + 1
                    record = {'external_id': external_id,'status':'fail'}
                    output_records.append(record)
        except:
            logging.exception('error in processing batch at record %s', i)

        

    logging.info("process summary %s", result_statics)

    if output_records:
            writer = csv.DictWriter(open("payment_allocation_job_result.csv", "w"), fieldnames=output_records[0].keys())
            writer.writeheader()
            writer.writerows(output_records)

commands.add_command(process_cash_payments, 'process-payments')

if __name__ == '__main__':
    commands()