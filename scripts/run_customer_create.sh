#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  if [ -z "$ARGS" ]
  then
    echo "ARGS is not passed runing for previous day"
    /usr/bin/python3 jobs/client_manager.py create-customer 
  else
    echo "ARGS is passed runing for"
    echo ${ARGS}
    /usr/bin/python3 jobs/client_manager.py create-customer --run-date ${ARGS}
  fi
popd
