#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 --version
  /usr/bin/python3 -m pip --version
  /usr/bin/python3 -m pip install -r requirements.txt
  #pip freeze
  #pip install --upgrade pip
  #pip install --upgrade <package-name>
  #setup.py
  #pip check
popd
