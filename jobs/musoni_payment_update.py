from datetime import datetime
import sys
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import uuid

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

def fetch_data(method, relative_url, params=None, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s %s", response.status_code, response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + response.json()['message'])
    return response.json()

def fetch_client_ids_for_phone(phone_number):
    logging.info("searching client for phone_number %s", phone_number)
    relative_url = 'search'
    params = {'query':str(phone_number).replace('+254','0'), 'resource': 'clients'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def fetch_all_loans_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?'.format(client_id)
    method = 'GET'
    response  = fetch_data(method, relative_url)
    return response.get('loanAccounts', [])

def post_repayment_to_loan(loan_id, amount, payment_date, code):
    logging.info("posting the payment to loan %s %s %s %s", loan_id, amount, payment_date, code)
    relative_url = 'loans/{}/transactions'.format(loan_id)
    method = 'POST'
    params = {'command': 'repayment'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "receiptNumber": code,
        "transactionDate": payment_date,
        "transactionAmount": amount,
        "paymentTypeId": "218",
        "note": "loan repay payment",
        "accountNumber": "",
        "checkNumber": "",
        "routingCode": "",
        "bankNumber": ""
        }
    response  = fetch_data(method, relative_url, params, payload)
    return response


def get_earliest_unpaid_loan(phone_number):
    logging.info("getting the oldest active loan  for phone_number %s", phone_number)
    client_details = fetch_client_ids_for_phone(phone_number)
    if client_details:
        client_id = client_details[0]['entityId']
        logging.info("client ID from musoni for phone number %s", client_id)
        all_loans = fetch_all_loans_for_client(client_id)
        active_loans = [loan for loan in all_loans if loan['status']['active']]
        loans_order_by_id = sorted(active_loans, key=lambda loan: loan['id'])
        return loans_order_by_id[0] if loans_order_by_id else None

def update_repayment_for_loan(phone_number, payment_amount, date, code):
    logging.info("posting payment for phone_no %s amount %s date %s code %s", phone_number, payment_amount, date, code)
    try:
        loan_to_be_updated = get_earliest_unpaid_loan(phone_number)
        logging.info("oldest loan fetched %s", loan_to_be_updated)
        if loan_to_be_updated:
            resp = post_repayment_to_loan(loan_to_be_updated['id'], payment_amount, date, code)
            if resp.get('loanTransactionId'):
                logging.info("paymented successfully posted for %s", phone_number)
                return True, resp.get('loanTransactionId')
            return False, 'failed to update'
        return False, 'no loan found the customer'
    except Exception as e:
        logging.exception('error in updating the payment')
        return False, str(repr(e))


def fetch_all_payments(date=datetime.today()):
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute('select * from payment_confirmations where till_number=%s and date(created_at)=%s order by transaction_time', (CREDIT_TILL_NUMBERS,date))
    logging.info("no of rows to process %s", cursor.rowcount)
    rows = cursor.fetchall()
    return rows


def update_tracker_table(confirmation_code, status, reason):
    logging.info("updating tracker table with following detail %s", (confirmation_code, status, reason))
    cursor = conn.cursor()
    cursor.execute('insert into musoni_payment_update_tracker values (%s, %s, %s, %s, %s, %s)', (uuid.uuid4(), confirmation_code, status, reason, datetime.now(), datetime.now()))
    conn.commit()

def is_payment_captured(confirmation_code):
    cursor = conn.cursor()
    cursor.execute('select * from musoni_payment_update_tracker where confirmation_code=%s and status=%s', (confirmation_code, 'true'))
    return True if cursor.rowcount else False


if __name__ == '__main__':
    logging.info("execution started")
    psycopg2.extras.register_uuid()
   
    if len(sys.argv) > 1:
        logging.info("run date given running for %s", sys.argv[1])
        run_date = datetime.strptime(sys.argv[1], '%Y-%m-%d')
    else:
        logging.info("run date not given running for today %s", datetime.today().date())
        run_date = datetime.today().date()

    payments_to_processed=fetch_all_payments(run_date)
    
    for record in payments_to_processed:
        logging.info("processing record %s", record)

        if not is_payment_captured(record['confirmation_code']):
            status, response = update_repayment_for_loan(record['sender_number'], record['transaction_amount'], record['transaction_time'].strftime(COMMON_DATE_FORMAT_CONVERSION), record['confirmation_code'])
            update_tracker_table(record['confirmation_code'], status, response)
            logging.info("post payment final results %s, %s", status, response)
        else:
            logging.info("confirmation code used already skipping %s", record['confirmation_code'])

    logging.info("execution ended")