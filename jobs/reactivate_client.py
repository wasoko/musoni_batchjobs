import sys
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import csv
import click

logging.basicConfig(
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def reactivate_client(clientId, reactivation_date):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'clients/{clientId}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'reactivate'}
    payload = {
        "dateFormat": "yyyy-MM-dd",
        "locale": "en_GB",
        "reactivationDate": reactivation_date
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def activate_client(clientId, activation_date):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'clients/{clientId}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'activate'}
    payload = {
        "dateFormat": "yyyy-MM-dd",
        "locale": "en_GB",
        "activationDate": activation_date
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def create_loans(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    reactivation_details = reactivate_client(record['Client Id'], record['disbursed_on'])
                    activation_details = activate_client(record['Client Id'], record['disbursed_on'])
                    record['resourceId'] = activation_details['resourceId']
                    record['status'] = 'success'
                except:
                    record['loan_id'] = 0
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
            writer = csv.DictWriter(open("clients_reactivation_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)


commands.add_command(create_loans)

if __name__ == '__main__':
    commands()