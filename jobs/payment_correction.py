from datetime import datetime
from pydoc import cli
import sys
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import uuid
import csv
import click

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def adjust_payment(loan_id, transaction_id, date, amount, reason):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'{loan_id}/transactions/{transaction_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = {
        "locale": "en_GB",
        "dateFormat": "dd MMMM yyyy",
        "transactionDate": date,
        "transactionAmount": amount,
        "note": reason
    }
    response = response.post(url, headers=headers, auth=auth, params = {},json=payload)
    if response.status != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def process_csv_file(file_path):
    error_count = 0
    success_count = 0
    total_records_to_process = 0
    error_records = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            total_records_to_process =len(list(reader))
            fp.seek(0)
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    adjust_payment(record['loan_id'], record['transaction_id'], record['date'], record['amount'], record['reason'])
                    success_count = success_count + 1
                except:
                    error_count = error_count + 1
                    error_records.append(record)
                    logging.exception("error in processing the record %s", record)
    except:
        logging.exception("error in processing file")

click.add_command(process_csv_file)

if __name__ == '__main__':
    commands()