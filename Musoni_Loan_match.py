import pandas as pd
import datetime

loan_data = pd.read_csv(r"ims_apr_24.csv")

musoni_loans = pd.read_csv(r"musoni_apr_24_3.csv",delimiter = ';')

musoni_loans['Disbursed On Date'] = pd.to_datetime(musoni_loans['Disbursed On Date'])
musoni_loans = musoni_loans[musoni_loans['Disbursed On Date'].dt.date <= datetime.datetime.strptime('2022-04-24',"%Y-%m-%d").date()]

# Find Modified Loans


musoni_loans = musoni_loans[~musoni_loans['External Id'].isna()]
temp = pd.merge(musoni_loans[['Account No','External Id','Principal Amount','Principal Outstanding Derived','Total Outstanding Derived','External Client ID','Client Name','Client Id','Submitted On Date','Approved On Date','Disbursed On Date','Loan Officer Name','Product Name','Staff Name']],loan_data,how='inner',left_on='External Id',right_on='external_id')

temp['Balance_difference'] = temp.apply(lambda x: 'True' if x['Principal Outstanding Derived']!= x['Balance'] else 'False',axis=1)

musoni_data =temp[temp['Balance_difference']=='True']
musoni_data['Balance_amount_diff'] = temp['Principal Outstanding Derived'] -temp['Balance']

def concatenate(row):
    return row['Client Name'] + "({0})".format(row['Client Id'])

musoni_data['Client Name/ Group Name'] = musoni_data.apply(lambda x: concatenate(x),axis=1)

musoni_data['# of Repayments'] = 1
musoni_data['Payment Type'] = 'Products'
musoni_data['Loan Type'] = 'Individual'


def reverse(s):
    if len(s) <= 1:
        return s.strip()

    return (s[1].strip()) +" "+ s[0].strip().capitalize()

musoni_data['Staff Name changed']  = musoni_data['Staff Name'].apply(lambda x: reverse(x.split(",")))

            
musoni_data  = musoni_data[['Branch','loan_type','External Client ID','Client Name/ Group Name','Account No','Product Name','Staff Name changed','Submitted On Date','Approved On Date','Disbursed On Date','Payment Type','Principal Amount','Principal Outstanding Derived','Balance','Balance_amount_diff','# of Repayments','External Id']]

musoni_data.to_excel("musoni_vs_ims_apr_24_v3.xlsx",index=False)

# # Find missing Loans

# open_loans = loan_data[loan_data['Balance']>0]

# missing_loans  = open_loans[~open_loans['external_id'].isin(musoni_loans['External Id'])]

# client_data = pd.read_csv(r"Client_Data_Imported_20220314123634.csv",delimiter = ';')

# missing_loans = pd.merge(missing_loans,client_data[['Client Id','Client Name','External Id','Staff Name']],how='left',right_on='External Id',left_on='customer_id')


# def concatenate(row):
#     return row['Client Name'] + "({0})".format(row['Client Id'])

# missing_loans['Client Name/ Group Name'] = missing_loans.apply(lambda x: concatenate(x),axis=1)

# missing_loans['# of Repayments'] = 1
# missing_loans['Payment Type'] = 'Products'
# missing_loans['Loan Type'] = 'Individual'


# def reverse(s):
#     if len(s) <= 1:
#         return s.strip()

#     return (s[1].strip()) +" "+ s[0].strip().capitalize()

# missing_loans['Staff Name changed']  = missing_loans['Staff Name'].apply(lambda x: reverse(x.split(",")))

# missing_loans   = missing_loans[["Branch","loan_type","external_id","Client Name/ Group Name","product","Staff Name changed","submitted_on","approved_on","disbursed_on","Payment Type","Balance","# of Repayments","external_id"]]


# missing_loans.to_excel("Musoni Missing Loans April 21.xlsx",index=False)
