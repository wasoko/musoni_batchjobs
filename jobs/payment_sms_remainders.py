from re import template
import uuid
import warnings
import click
from datetime import datetime, timedelta
from sqlalchemy import create_engine
import psycopg2.extras
from musoni_utils import *
import sms_templates
import pandas as pd
import csv
from musoni_db_check import check_latest_data_available


psycopg2.extras.register_uuid()

warnings.filterwarnings("ignore")

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout), logging.FileHandler('AL_update_MusoniIms_v2.log')])

error_log = []

conn = psycopg2.connect(
    database=DB_CONNECTION_DB,
    user=DB_CONNECTION_USERNAME,
    password=DB_CONNECTION_PASSWORD,
    host=DB_CONNECTION_HOST,
    port=DB_CONNECTION_PORT
)

engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                host=MUSONI_DB_CONNECTION_HOST,
                                                                                port=MUSONI_DB_CONNECTION_PORT,
                                                                                db=MUSONI_DB_CONNECTION_DB))

DPD_MAP = {
    "4": "MINUS_4",
    "1": "MINUS_1",
    "0": "0",
    "-1": "PLUS_1",
    "-2": "PLUS_2",
    "-3": "PLUS_3",
    "-4": "PLUS_4",
    "-5": "PLUS_5",
    "-7": "PLUS_7",
    "-8": "PLUS_8",
    "-9": "PLUS_9",
    "-10": "PLUS_10",
    "-11": "PLUS_11",
    "-12": "PLUS_12",
    "-13": "PLUS_13",
    "-14": "PLUS_14",
    "-15": "PLUS_15",
    "-16": "PLUS_16",
    "-25": "PLUS_25",
    "-26": "PLUS_26",
    "-27": "PLUS_27",
    "-28": "PLUS_28",
    "-29": "PLUS_29"
}

COUNTRY_CURRENCY_MAP = {
    "Uganda": "UGX",
    "Rwanda": "RWF",
    "IvoryCoast": "XOF",
    "Kenya": "KES",
    "Tanzania": "TZS"
}

TIMEZONE_CURRENCY_MAP = {
    "EAT": ("UGX", "TZS"),
    "UTC": ("XOF",)
}

KENYA_COUNTRY_CODE = '+254'


def fetch_loans_with_due_date(currency_code_list, time_delta_in_days):
    date = (datetime.today() + timedelta(days=time_delta_in_days)).strftime("%Y-%m-%d")
    logging.info("picking records for the date %s and DPD %s ", date, time_delta_in_days)
    query = '''select ml.id, ml.external_id, ml.office_id, ml.product_id, mc.mobile_no, ml.currency_code, ml.total_outstanding_derived, ml.expected_maturedon_date from m_loan ml 
    join m_client mc on mc.id = ml.client_id where ml.total_outstanding_derived > 0 and 
    currency_code in %(currency_list)s and loan_status_id != 600 and expected_maturedon_date =  %(maturity_date)s'''
    records = pd.read_sql_query(query, engine, params={'currency_list': currency_code_list, 'maturity_date': date})
    logging.info("records count: %s", len(records))
    return records

def fetch_retry_list():
    date = (datetime.today()).strftime("%Y-%m-%d")
    logging.info("picking retry records for the date %s", date)
    cursor = conn.cursor()
    cursor.execute('select id, mobile_number from musoni_sms_tracker where status != %s and date(created_at) = %s',('Notification queued for delivery',date))
    result_set = cursor.fetchall()
    retry_df = pd.DataFrame(result_set, columns =['uuid', 'mobile_no']) 
    return retry_df

def update_sms_tracker_table(mobileNo, message, template_name, status, record, retry_flag):
    logging.info("updating sms tracker table with following detail %s", (mobileNo, message, template_name, status))
    cursor = conn.cursor()
    if retry_flag:
        cursor.execute('update musoni_sms_tracker set retries=retries+1, status=%s, updated_at=%s where id=%s',(status, datetime.now(), record['uuid']))
    else:
        cursor.execute('insert into musoni_sms_tracker values (%s, %s, %s, %s, %s, %s, %s, %s)',
                   (uuid.uuid4(), mobileNo, message, template_name, status, datetime.now(), datetime.now(), 0))  
    conn.commit()


def get_template(record, DPD):
    template_name = ''
    if record['currency_code'] == COUNTRY_CURRENCY_MAP.get('IvoryCoast'):
        template_name = "IC_" + DPD
    if record['currency_code'] == COUNTRY_CURRENCY_MAP.get('Uganda'):
        template_name = "UG_" + DPD
    if record['currency_code'] == COUNTRY_CURRENCY_MAP.get('Rwanda'):
        template_name = "RW_" + DPD
    if record['currency_code'] == COUNTRY_CURRENCY_MAP.get('Kenya'):
        template_name = "KE_" + DPD
    if record['currency_code'] == COUNTRY_CURRENCY_MAP.get('Tanzania'):
        template_name = "TZ_" + DPD

    return template_name


def get_mobile_no(record):
    if record['currency_code'] == 'KES' and record['mobile_no'] and record['mobile_no'].startswith('0'):
        mobile_no = record['mobile_no'].replace('0', KENYA_COUNTRY_CODE, 1)
        return mobile_no
    else:
        return record['mobile_no']
    
def is_branch_enabled_for_sms(currency_code, office_id):
    logging.info("checking branch enabled for the sms or not %s %s", currency_code, office_id)
    return COUNTRY_SMS_ENABLED_BRANCHES[currency_code] == 'all' or office_id in COUNTRY_SMS_ENABLED_BRANCHES[currency_code]

def is_product_enabled_for_sms(currency_code, product_id):
    logging.info("checking loan product enabled for the sms or not %s %s %s", currency_code, product_id, COUNTRY_PRODUCT_ID_MAP)
    return COUNTRY_PRODUCT_ID_MAP[currency_code] == 'all' or product_id in COUNTRY_PRODUCT_ID_MAP[currency_code]

def is_mobile_number_available(mobile_no):
    logging.info("checking if mobile_no is available %s", mobile_no)
    if mobile_no:
        return True
    else:
        return False

def check_pre_conditions(record):
    condition_results = [
        is_product_enabled_for_sms(record['currency_code'], record['product_id']), 
        is_branch_enabled_for_sms(record['currency_code'], record['office_id']),
        is_mobile_number_available(record['mobile_no'])
    ]
    logging.info("results of call conditions %s", condition_results)
    return all(condition_results)

def process_records(records, DPD, retry_flag):
    for index, record in records.iterrows():
        logging.info("validating pre conditions\n %s", record)
        if not check_pre_conditions(record):
            continue
        logging.info("conditions passed sending the SMS")
        template_name = get_template(record, DPD)
        if sms_templates.SMS_TEMPLATES_MAP.get(template_name,None):
            mobile_no = record['mobile_no']
            try:
                if template_name == 'TZ_DPD_MINUS_4':
                    message = sms_templates.SMS_TEMPLATES_MAP[template_name].format(record['total_outstanding_derived'], (datetime.today() + timedelta(days=4)).strftime("%d %B %Y"))
                else:
                    message = sms_templates.SMS_TEMPLATES_MAP[template_name].format(record['total_outstanding_derived'])

                notifier_response = send_sms(mobile_no, message)

                if notifier_response['status']:
                    update_sms_tracker_table(mobile_no, message, template_name, notifier_response['successDescription'], record, retry_flag)
                else:
                    update_sms_tracker_table(mobile_no, message, template_name, notifier_response['errorDescription'], record, retry_flag)

            except Exception as ex:
                logging.exception("error in sending the sms")
                error_log.append(str(ex))
                update_sms_tracker_table(mobile_no, 'NA', template_name, 'Exception while calling api', record, retry_flag)

def send_all_messages(currency_code_list, retry_flag):
    error_log = []
    try:
        for dpd, dpd_value in DPD_MAP.items():
            logging.info("Sending SMS'es for customers having DPD_" + dpd_value)
            records_df = fetch_loans_with_due_date(currency_code_list, int(dpd))
            if(len(records_df) > 0):
                records_df['mobile_no'] = records_df.apply(lambda x: get_mobile_no(x),axis=1)
                if retry_flag:
                    logging.info("Retrying the failed SMS")
                    retry_records_df = fetch_retry_list()
                    final_df = retry_records_df.merge(records_df, how='inner', on='mobile_no')
                    if(len(final_df) > 0):
                        process_records(final_df, 'DPD_' + dpd_value, retry_flag)
                else:
                    process_records(records_df, 'DPD_' + dpd_value, retry_flag)

    except Exception as e:
        error_log.append(str(e))
    
    return error_log


@click.group()
def commands():
    pass


@click.command()
def process_sms_for_current_day():
    check_latest_data_available()
    try:
        logging.info(MUSONI_DB_CONNECTION_DB)
        currency_list = []
        retry_flag = False
        now = datetime.now()
        logging.info("processes start at %s", now)

        utc_run_hour_actual_country_timezone_map = {
            6: 'EAT',
            7: 'CAT',
            9: 'UTC'
        }

        if now.minute >=15:
            retry_flag=True

        actual_timezone_to_run = utc_run_hour_actual_country_timezone_map.get(now.hour)

        if actual_timezone_to_run:
            currency_list = TIMEZONE_CURRENCY_MAP.get(actual_timezone_to_run)
        else:
            logging.info("Script ran out of running window. Exited gracefully.")
            sys.exit(0)

        currency_code_list = currency_list
        logging.info(currency_code_list)

        error_log.extend(send_all_messages(currency_code_list, retry_flag))        

    except Exception as e:
        error_log.append(str(e))

    if len(error_log) >= 1:
        logging.info("Summary : \n\n Script ran with the following errors -- %s", (" ".join(error_log)))
        sys.exit(1)
    else:
        logging.info("Script ran successfully")
        sys.exit(0)


@click.command()
@click.argument('currency-code')
@click.argument('retry-flag')
def process_sms_for_country(currency_code, retry_flag):
    check_latest_data_available()
    currency_code_list = (currency_code.upper(),)
    retry_flag = retry_flag == 'True'
    logging.info("processing the sms for currency / country  %s", currency_code_list)
    error_log.extend(send_all_messages(currency_code_list, retry_flag))

    if len(error_log) >= 1:
        logging.info("Summary : \n\n Script ran with the following errors -- %s", (" ".join(error_log)))
        sys.exit(1)
    else:
        logging.info("Script ran successfully")
        sys.exit(0)

@click.command()
@click.argument('file-path')
def ad_hoc_sms_communication(file_path):
    output_record = []
    template_name = 'AD_HOC'
    try:
        df = pd.read_excel(file_path, converters={'Phone number': str}, keep_default_na=False)
        for idx, record in df.iterrows():
            record = record.to_dict()
            logging.info("processing record %s", record)

            mobile_no = record['Phone number']
            message = record['Message']

            if not mobile_no.startswith('+'):
                logging.info("Make sure the phone numbers have the format (“+” “Country code” ”Phone No.” ) -Example - +254702222222")
                record['status'] = False
                record['comment'] = "Wrong Phone number format"
                output_record.append(record)
                continue

            if message and mobile_no:
                try:
                    notifier_response = send_sms(mobile_no, message)
                    record['status'] = notifier_response['status']
                    record['comment'] = notifier_response['successDescription']
                    output_record.append(record)
                except Exception as e:
                    record['status'] = False
                    record['comment'] = str(e)
                    output_record.append(record)
                    logging.exception("exception wile processing %s", record)
            else:
                logging.info("Skipping record; Please make sure the Phone number and Message values are not empty")
                record['status'] = False
                record['comment'] = "Missing Phone number/message"
                output_record.append(record)
                continue

            if notifier_response['status']:
                update_sms_tracker_table(mobile_no, message, template_name, notifier_response['successDescription'],
                                         record, False)
            else:
                update_sms_tracker_table(mobile_no, message, template_name, notifier_response['errorDescription'],
                                         record, False)
    except Exception as e:
        logging.exception("error in processing file", e)
        sys.exit(1)

    if output_record:
        writer = csv.DictWriter(open("failed_ad_hoc_sms_communications.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


commands.add_command(process_sms_for_country, 'process-sms-for-country')
commands.add_command(process_sms_for_current_day, 'auto-process-for-all')
commands.add_command(ad_hoc_sms_communication, 'ad-hoc-sms-communication')

if __name__ == "__main__":
    commands()
