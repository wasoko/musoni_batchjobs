import requests
import logging
import sys
from config import *

logging.basicConfig(
    encoding='utf-8',
    level=logging.DEBUG,
    handlers=[logging.StreamHandler(sys.stdout)])

def call_api(method, relative_url):
    logging.info("calling vas API endpoint(%s:%s)", method, relative_url)
    headers = SELCOM_HEADERS
    url = VAS_API_URL + relative_url
    response = requests.request(method, url, headers=headers).json()
    logging.info("response received %s", response)

    if response.get('successCode') not in (200, 201):
        logging.info("Request failed with error: (%s:%s)", response.get('status'), response.get('error'))
    return response

def get_agent_balance(customer_id):
    logging.info("fetching agent balance for customer_id %s", customer_id)
    relative_url = 'balance/{}'.format(customer_id)
    response = call_api('GET', relative_url)
    return response