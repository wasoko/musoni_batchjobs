import sys
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import csv
import click
import json

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def search_loan(external_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'search'
    params = {'query': external_id}
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.get(url, headers=headers, auth=auth, params = params,json={})
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()[0]['entityId']


def get_loan_details(loan_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.get(url, headers=headers, auth=auth, params = {},json={})
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def undo_disburse(loan_id, note):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command': 'undoDisbursal'}
    payload = {'note': note}
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def undo_approval(loan_id, note):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command': 'undoApproval'}
    payload = {'note': note}
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def update_principal(loan_id, loan_details, disburse_date, principal_amount):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = {
        "locale": "en",
        "dateFormat": "yyyy-MM-dd",
        "productId": loan_details['loanProductId'],
        "principal": principal_amount,
        "loanTermFrequency": loan_details['termFrequency'],
        "loanTermFrequencyType": loan_details['termPeriodFrequencyType']['id'],
        "numberOfRepayments": loan_details['numberOfRepayments'],
        "repaymentEvery": loan_details['repaymentEvery'],
        "repaymentFrequencyType": loan_details['repaymentFrequencyType']['id'],
        "interestRatePerPeriod": loan_details['interestRatePerPeriod'],
        "interestType": loan_details['interestType']['id'],
        "interestCalculationPeriodType": loan_details['interestCalculationPeriodType']['id'],
        "amortizationType": loan_details['amortizationType']['id'],
        "expectedDisbursementDate": disburse_date,
        "submittedOnDate": disburse_date,
        "transactionProcessingStrategyId": loan_details['transactionProcessingStrategyId']
    }
    logging.info(" payload to request %s", json.dumps(payload))
    response = requests.put(url, headers=headers, auth=auth, params = {},json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def approve_loan(loan_id, approval_date, disburse_date, note):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'approve'}
    payload = {
        "dateFormat": "yyyy-MM-dd",
        "locale": "en_GB",
        "approvedOnDate": approval_date,
        "expectedDisbursementDate": disburse_date,
        "note": note
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json())
    return response.json()

def disburse_loan(loan_id, amount, disburse_date, note):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'disburse'}
    payload = {
        "dateFormat": "yyyy-MM-dd",
        "locale": "en_GB",
        "transactionAmount":amount,
        "actualDisbursementDate": disburse_date,
        "note": note,
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def update_loan(loan_id, payload):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = payload
    response = requests.put(url, headers=headers, auth=auth,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def withdraw(loan_id, note):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'loans/{loan_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command': 'withdrawnByApplicant'}
    payload = {
        "note": note,
        "locale": "en",
        "dateFormat": "dd MMMM yyyy",
        "withdrawnOnDate": "08 December 2023"
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()


@click.group()
def commands():
    pass


@click.command()
@click.argument('file-path')
def undo_loan_disburse(file_path):
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    loan_id = search_loan(record['external_id'])
                    #loan_id = record['Account No']
                    undo_disburse(loan_id, 'wrongly created loan')
                    undo_approval(loan_id, 'wrongly created loan')
                    update_loan(loan_id, {"externalId": ""})
                    withdraw(loan_id, 'wrongly created loan')
                except:
                    logging.exception("error in processing the record %s", record)
    except:
        logging.exception("error in processing file")

@click.command()
@click.argument('invoice')
def withdraw_invoice(invoice):
    try:
        loan_id = search_loan(invoice)
        undo_disburse(loan_id, 'wrongly created loan')
        undo_approval(loan_id, 'wrongly created loan')
        # update_loan(loan_id, {"externalId": ""})
        withdraw(loan_id, 'wrongly created loan')
    except:
        logging.exception("error in processing the record %s", invoice)

@click.command()
def withdraw_invoices():
    invoices = ['KEINV033170','KEINV033099','KEINV021789','KEINV022116','KEINV023160','KEINV024110','KEINV023735','KEINV028049','KEINV053012','KEINV052288','KEINV052256','KEINV052691','KEINV054615','KEINV055496','KEINV057402','KEINV057141','KEINV058776','KEINV058791','KEINV058545','KEINV060772','KEINV062104','KEINV061958','KEINV061098','KEINV065290','KEINV064935','KEINV065416','KEINV065552','KEINV066882','KEINV067758','KEINV068728','KEINV068076','KEINV068944','KEINV069100','KEINV069222','KEINV068776','KEINV069433','KEINV068869','KEINV069273','KEINV069550','KEINV069160','KEINV069193','KEINV069817','KEINV068883','KEINV069944','KEINV069821','KEINV069441','KEINV068815','KEINV069294','KEINV069732','KEINV069383','KEINV069163','KEINV069557','KEINV070810','KEINV071815','KEINV072054','KEINV073749','KEINV073344','KEINV073078','KEINV073217','KEINV074996','KEINV077130','KEINV077713','KEINV079599','KEINV079868','KEINV079321','KEINV080354','KEINV080369','KEINV082461','KEINV082503','KEINV082584','KEINV082600','KEINV084269','KEINV083398','KEINV082560','KEINV084659','KEINV085879','KEINV085902','KEINV085822','KEINV086278','KEINV087150','KEINV086712','KEINV087332','KEINV087409','KEINV085922','KEINV086636','KEINV088464','KEINV089262','KEINV089198','KEINV087849','KEINV090197','KEINV089727','KEINV089777','KEINV090811','KEINV089600','KEINV089971','KEINV089974','KEINV091112','KEINV091326','KEINV092083','KEINV092136','KEINV092148','KEINV091756','KEINV092242','KEINV092139']
    for invoice in invoices:
        try:
            loan_id = search_loan(invoice)
            undo_disburse(loan_id, 'wrongly created loan')
            undo_approval(loan_id, 'wrongly created loan')
            update_loan(loan_id, {"externalId": ""})
            withdraw(loan_id, 'wrongly created loan')
        except:
            logging.exception("error in processing the record %s", invoice)

@click.command()
@click.argument('file-path')
def disburse_loans(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    # loan_id = search_loan(record['external_id'])
                    loan_id = record['Account No']
                    loan_details = get_loan_details(loan_id)
                    approve_loan(loan_id, record['Approved On Date'],record['Disbursed On Date'],'approving loan after correction as on 31 Mar 2022')
                    disburse_loan(loan_id, record['Balance'],record['Disbursed On Date'],'disbursing loan after correction as on 31 Mar 2022')
                    record['current_principal'] = loan_details['summary']['totalOutstanding']
                    record['status'] = 'success'
                except:
                    record['current_principal'] = 0
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
            writer = csv.DictWriter(open("loan_principal_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)

@click.command()
@click.argument('file-path')
def change_principal(file_path):
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    # loan_id = search_loan(record['external_id'])
                    loan_id = record['Account No']
                    loan_details = get_loan_details(loan_id)
                    update_principal(loan_id, loan_details, record['Disbursed Date*'], record['Balance'].replace(',',''))
                except:
                    logging.exception("error in processing the record %s", record)
                    logging.info("failed in updating the record %s", record['Account No'])
    except:
        logging.exception("error in processing file")

commands.add_command(undo_loan_disburse)
commands.add_command(change_principal)
commands.add_command(disburse_loans)
commands.add_command(withdraw_invoice)
commands.add_command(withdraw_invoices)

if __name__ == '__main__':
    commands()