#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  python jobs/blacklist_clients.py blacklist-risky-pre-approved-credit-clients
popd
