from asyncio.log import logger
import logging
import csv
import sys
from uuid import uuid4
import pandas as pd
import numpy as np
import click
from datetime import datetime
from common_auth_login import get_auth_token
import musoni_utils
import crm_connector
from config import *
import psycopg2
import psycopg2.extras

# auth_response =  get_auth_token()
# ims_auth_token = auth_response['token_details']['access_token']


conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

def update_credit_limit_in_musoni(record, date_of_change, is_customer_having_open_loan):
    try:
        logger.info("finding the client id for the external id")
        client = musoni_utils.get_client_for_external_id(record['external_id'])
        if client:
            client_id = client['entityId']
            new_limit = 0
            available_limit = 0
            if not is_blacklisted(record['external_id']) and not is_customer_having_open_loan:
                available_limit = record['new_limit']
            payload={
                'credit_limit': record['new_limit'],
                'available_limit': available_limit,
                'client': client_id,
                'date':  date_of_change
            }
            logger.info("updating the credit limit to the customer")
            musoni_utils.update_client_credit_details(client_id, payload)
            return True, 'success'
        else:
            return False, 'no client'
    except:
        logger.exception("exception while updating the limit %s", record['external_id'])
        return False, 'exception'

def update_credit_limit_in_ims(record):
    customer_id = record['external_id']
    credit_limit = record['new_limit']
    logging.info("updating the customer credit limit %s %s", customer_id, credit_limit)
    try:
        customer_details = crm_connector.get_customer_details(ims_auth_token, customer_id)
        customer_details['credit_limit']= credit_limit
        customer_details['language_id'] = customer_details['language']['id']
        customer_details['organization_location_id'] = customer_details['organization_location']['id']
        response = crm_connector.update_customer_details(ims_auth_token, customer_id, customer_details)
        logging.info("updated the credit for the customer response %s", response)
        return True, 'Success'
    except:
        logging.exception("error in updating the credit limit for the customer %s", customer_id)
        return False, 'Exception'

def get_existing_credit_limit(client_id):
    logging.info("getting the new limit for client")
    query = "select new_limit from musoni_credit_limit_update_tracker where client_id=%s and musoni = 'success' order by created_at desc limit 1";
    cursor = conn.cursor()
    try:
        cursor.execute(query, (client_id, ))
        row = cursor.fetchone()
        return row[0]
    except:
        return 0


def update_tracker_table(record, triggered_by, ims=None, musoni=None):
    logging.info("updating the tracker table")
    query = "insert into musoni_credit_limit_update_tracker(id, customer_id, existing_limit, new_limit, ims, musoni, created_at, triggered_by, reason) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    cursor = conn.cursor()
    update_reason = record['reason'] if record.get('reason') else "no-reason"
    # existing_limit = get_existing_credit_limit(record['external_id'])
    # logger.info('existing limit fetched %s', existing_limit)
    try:
        cursor.execute(query, (str(uuid4()), record['external_id'], None, record['new_limit'], ims, musoni, datetime.now(), triggered_by, update_reason))
    except:
        logging.exception("error in updating the tracker table")
        cursor.execute("ROLLBACK")
    finally:
        conn.commit()

def check_customer_having_open_loan(external_id):
    logging.info("checking the customer having open loan or not %s", external_id)
    client = musoni_utils.get_client_for_external_id(external_id)
    if client:
            client_id = client['entityId']
            loan_accounts = musoni_utils.get_all_loans_for_client(client_id)
            active_loans = [loan for loan in loan_accounts if loan['status']['active']]
            logging.info("total active loan customer has %s", active_loans)
            if active_loans:
                return True
    return False
        
def update_credit_limit_to_ims_query(customer_id, credit_limit, is_customer_having_open_loan):
    logging.info("updating ims table %s", customer_id)
    query = "update customers set credit_limit=%s, updated_at=now() where id=%s"
    cursor = conn.cursor()
    # existing_limit = get_existing_credit_limit(record['external_id'])
    # logger.info('existing limit fetched %s', existing_limit)
    try:
        if is_customer_having_open_loan:
            credit_limit=0
        cursor.execute(query, (credit_limit, customer_id))
        return True, 'Success'
    except:
        logging.exception("error in updating table")
        cursor.execute("ROLLBACK")
        return True, 'Exception'
    finally:
        conn.commit()

def is_blacklisted(external_id):
    if not ENABLE_BLACKLIST_CHECK:
        return False
        
    logging.info("fetching from ims table for client %s", external_id)
    query = "select count(*) from fs_blacklisted_clients where client_ims_id=%s"
    
    cursor = conn.cursor()
    delinquency_query = "select count(*) from customers where delinquency=1 and id=%s"

    try:
        cursor.execute(query,(external_id,))
        result = cursor.fetchone()
        cursor.execute(delinquency_query,(external_id,))
        delinquency_result = cursor.fetchone()
        return False if result[0] == 0 and delinquency_result[0] == 0 else True
    except:
        logging.exception("error in fetching")
        return True

def process_record(record, date_of_change, system, triggered_by):
    ims_message = musoni_message = None
    ims_result = musoni_result = None
    is_blacklisted_var = is_blacklisted(record['external_id'])
    is_customer_having_open_loan = check_customer_having_open_loan(record['external_id'])
    new_limit = record['new_limit']
    if is_blacklisted_var:
        ims_message='blacklisted'
        musoni_message='blacklisted'
        update_credit_limit_to_ims_query(record['external_id'], 0, False)
        update_tracker_table(record, triggered_by, ims=ims_message, musoni=musoni_message)
        return musoni_result, musoni_message, ims_result, ims_message
    else:
        if system in ['all', 'musoni']:
            logger.info('updating the credit limit in musoni')
            musoni_result, musoni_message = update_credit_limit_in_musoni(record, date_of_change, is_customer_having_open_loan)
    
        if system in ['all', 'ims']:
            logger.info('updating the credit limit in ims')
            ims_result, ims_message = update_credit_limit_to_ims_query(record['external_id'], new_limit, is_customer_having_open_loan)
    
        update_tracker_table(record, triggered_by, ims=ims_message, musoni=musoni_message)
        return musoni_result, musoni_message, ims_result, ims_message

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
@click.argument('triggered_by')
@click.option('--date-of-change', default=datetime.today().strftime("%d-%m-%Y"))
@click.option('--system', default='all', type=click.Choice(['musoni', 'ims', 'all'], case_sensitive=False))
def update_client_limit(file_path, triggered_by, date_of_change, system):
    logger.info("processing file %s date %s and systems %s", file_path, date_of_change, system)
    output_record = []
    try:
        try: 
            logging.info("try the excel processing")
            rows = pd.read_excel(file_path, na_filter=False)
        except:
            logging.info("failed file is not excel try it as csv")
            rows = pd.read_csv(file_path, na_filter=False)
        rows['new_limit'] = rows['new_limit'].astype(str)
        rows['new_limit'] = rows['new_limit'].str.replace(",", "")
        rows['new_limit'] = rows['new_limit'].astype(float)
        records = rows.to_dict("records")
        for record in records:
            logging.info("processing record %s", record)
            if record['external_id']:
                try:
                    if record.get('reason'):
                        musoni_result, musoni_message, ims_result, ims_message = process_record(record, date_of_change, system, triggered_by)
                        record['ims_status'] = ims_result
                        record['ims_message'] = ims_message
                        record['musoni_status'] = musoni_result
                        record['musoni_message'] = musoni_message
                    else:
                        update_tracker_table(record, triggered_by, ims="no-reason", musoni="no-reason")
                        record['ims_status'] = 'no reason'
                        record['ims_message'] = 'no reason'
                        record['musoni_status'] = 'no reason'
                        record['musoni_message'] = 'no reason'
                        logging.info("no reason info provided for record %s", record)
                except Exception as e:
                    record['ims_status'] = 'exception'
                    record['ims_message'] = 'exception'
                    record['musoni_status'] = 'exception'
                    record['musoni_message'] = 'exception'
                    logging.exception("error in processing the record %s with reason: %s ", record, str(e))
                    sys.exit(1)
                output_record.append(record)
            else:
                logging.info("Failure: No external id present for customer with mobile number: %s. Please check, update and run again.", record['Phone'])
                sys.exit(1)
    except Exception as e:
        logging.exception("Failure: File has empty or wrong entry in new_limit field throwing error: %s. Please check, update and run again.", str(e))
        sys.exit(1)

    if output_record:
            writer = csv.DictWriter(open("output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)
    sys.exit(0)


commands.add_command(update_client_limit, 'update-limit')

if __name__ == '__main__':
    commands()