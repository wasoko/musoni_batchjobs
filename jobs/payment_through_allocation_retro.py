from datetime import date, datetime, timedelta
import json
import csv
import sys
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from config import *
from savings_account_creator import *
from reactivate_client import * 
import logging
import uuid
import click
import time

logging.basicConfig(
    encoding='utf-:', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

def fetch_data(method, relative_url, params=None, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s %s", response.status_code, response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + str(response.json()['errors']))
    return response.json()

def fetch_client_ids_for_phone(phone_number):
    logging.info("searching client for phone_number %s", phone_number)
    relative_url = 'search'
    params = {'query':str(phone_number).replace('+254','0'), 'resource': 'clients'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def fetch_client_ids_for_external_id(external_id):
    logging.info("searching client for external_id %s", external_id)
    relative_url = 'clients'
    params = {'sqlSearch':"c.external_id='{}'".format(external_id), 'status': 300}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response['pageItems'][0]

def fetch_country_name_for_external_id(external_id):
    logging.info("searching country name for external_id %s", external_id)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    query = """SELECT oc2."name"  FROM customers c 
                join organization_locations ol on ol.id = c.organization_location_id 
                join organization_regions or2 on or2.id = ol.organization_region_id 
                join organization_cities oc on oc.id = or2.organization_city_id 
                join organization_countries oc2 on oc2.id = oc.organization_country_id 
                where c.id = '{}'"""
    cursor.execute(query.format(external_id))
    country_name = json.loads(json.dumps(cursor.fetchone()))
    logging.info("country name is {}".format(country_name))
    return country_name.get('name')

def fetch_saving_account_for_external_id(external_id):
    #TODO: Filter only Active saving account
    #TODO: if saving account is closed, reopen  & activate it
    #TODO: if No Saving account create one
    logging.info("searching saving for external_id %s", external_id)
    relative_url = 'savingsaccounts'
    params = {'sqlSearch':"c.external_id='{}'".format(external_id)}
    method = 'GET'
    saving_accounts  = fetch_data(method, relative_url, params)['pageItems']

    try:
        if saving_accounts:
            active_saving_accounts = list(filter(lambda x: x['status']['active'] == True, saving_accounts))
            if active_saving_accounts:
                saving_accounts = active_saving_accounts[0]
            else:
                closed_saving_accounts = list(filter(lambda x: x['status']['closed'] == True, saving_accounts))
                if closed_saving_accounts:
                    closed_saving_accounts = closed_saving_accounts[0]
                    reopen_savings_account(closed_saving_accounts['id'])
                    approve_savings_account(closed_saving_accounts['id'], date.today().strftime('%Y-%m-%d'))
                    activate_savings_account(closed_saving_accounts['id'], date.today().strftime('%Y-%m-%d'))
                    saving_accounts = closed_saving_accounts
                else:
                    approve_savings_account(saving_accounts[0]['id'], date.today().strftime('%Y-%m-%d'))
                    activate_savings_account(saving_accounts[0]['id'], date.today().strftime('%Y-%m-%d'))
                    saving_accounts = saving_accounts[0]

        else:
            client = fetch_client_ids_for_external_id(external_id)
            if not client['active']:
                reactivate_client(client['id'], date.today().strftime('%Y-%m-%d'))
                activate_client(client['id'], date.today().strftime('%Y-%m-%d'))
            country_name = fetch_country_name_for_external_id(str(external_id))
            savings_details = create_savings_account(client['id'], COUNTRY_PRODUCT_ID_MAP[country_name], client.get('staffId',''), date.today().strftime('%Y-%m-%d'), external_id)
            approve_savings_account(savings_details['savingsId'], date.today().strftime('%Y-%m-%d'))
            activate_savings_account(savings_details['savingsId'], date.today().strftime('%Y-%m-%d'))
            saving_accounts = fetch_data(method, relative_url, params)['pageItems'][0]

    except Exception as e:
        logging.exception('error in fetching account details')
        return False, str(repr(e)), {}

    logging.info("successfully fetched savings account: %s", saving_accounts)

    return saving_accounts   

def fetch_loan_for_external_id(external_id):
    logging.info("searching loans for external_id %s", external_id)
    relative_url = 'search'
    params = {'query':str(external_id), 'resource': 'loans'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def get_all_accounts_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?'.format(client_id)
    method = 'GET'
    response  = fetch_data(method, relative_url)
    return response

def post_repayment_to_loan(loan_id, amount, payment_date, code):
    logging.info("posting the payment to loan %s %s %s %s", loan_id, amount, payment_date, code)
    relative_url = 'loans/{}/transactions'.format(loan_id)
    method = 'POST'
    params = {'command': 'repayment'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "receiptNumber": code,
        "transactionDate": payment_date,
        "transactionAmount": amount,
        "paymentTypeId": "218",
        "note": "loan repay payment",
        "accountNumber": "",
        "checkNumber": "",
        "routingCode": "",
        "bankNumber": ""
        }
    response  = fetch_data(method, relative_url, params, payload)
    return response

def deposit_money(saving_account_id, payment_amount, date, code):
    relative_url = f'savingsaccounts/{saving_account_id}/transactions'
    params = {'command': 'deposit'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "transactionDate": date,
        "transactionAmount": payment_amount,
        "paymentTypeId": "177",
        "receiptNumber": code,
        }
    response  = fetch_data('POST', relative_url, params, payload)
    return True, response['resourceId']

def get_current_balance(saving_account_id):
    relative_url = f'savingsaccounts/{saving_account_id}/'
    response  = fetch_data('GET', relative_url, {}, {})
    return response['summary']['accountBalance']

def withdraw_money(saving_account_id, payment_amount, date, code):
    relative_url = f'savingsaccounts/{saving_account_id}/transactions'
    params = {'command': 'withdrawal'}
    payload = {
            "dateFormat": "dd MMMM yyyy",
            "locale": "en",
            "transactionDate": date,
            "transactionAmount": payment_amount,
            "paymentTypeId": "177",
            "receiptNumber": code,
        }
    response  = fetch_data('POST', relative_url, params, payload)
    return True, response['resourceId']

def deposit_money_to_saving_account(saving_account_id, payment_amount, date, code):
    try:
        status, transaction_id = deposit_money(saving_account_id, payment_amount, date, code)
        balance = get_current_balance(saving_account_id)
        return True, balance, transaction_id
    except:
        raise Exception(f'Error in depsiting money for {saving_account_id}')

def fetch_all_payments(date=datetime.today()):
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    query = """select ci.invoice_number,dn.customer_id,cip.amount,cpd.payment_refrence_code,ipa.created_at 
from customer_invoices ci 
join delivery_notes dn on ci.id = dn.customer_invoice_id
join invoice_payment_allocations ipa on ci.id = ipa.customer_invoice_id
join customer_invoice_payments cip on ipa.id = cip.invoice_payment_allocation_id
join customer_payments cp on cp.id = cip.customer_payment_id
join customer_payment_details cpd on cpd.customer_payment_id = cp.id
where ci.invoice_number in (
'ke_cinv_2311002',
'ke_cinv_2307902',
'ke_cinv_2359526',
'ke_cinv_2353092',
'ke_cinv_2403797',
'ke_cinv_2371062',
'ke_cinv_2398271',
'ke_cinv_2376741',
'ke_cinv_2354260',
'ke_cinv_2374847',
'ke_cinv_2403848',
'ke_cinv_2376395',
'ke_cinv_2367389',
'ke_cinv_2332181',
'ke_cinv_2336026',
'ke_cinv_2346020')
and ipa.created_at > '2022-06-26'
and cpd.payment_refrence_code in ('QFQ2DNTH5A','QFQ7DQJ8QP','QFR7FAEYJZ','QFR0H4VAL2','QFR0FD6ZNI','QFQ5DOS68B','QFQ1DMB2NP','QFQ3DJL02D','QFQ1DP74W5','QFQ7DMGZWT','QFQ6F4YWLU')
order by 1"""

    cursor.execute(query, ('no saving accounts for the client', date))
    logging.info("no of rows to process %s", cursor.rowcount)
    rows = cursor.fetchall()
    return rows

def update_tracker_table(confirmation_code, status, reason):
    logging.info("updating tracker table with following detail %s", (confirmation_code, status, reason))
    cursor = conn.cursor()
    cursor.execute('insert into musoni_payment_update_tracker values (%s, %s, %s, %s, %s, %s)', (uuid.uuid4(), confirmation_code, status, reason, datetime.now(), datetime.now()))
    conn.commit()

def is_payment_captured(confirmation_code):
    logging.info("confirmation code is %s", confirmation_code)
    cursor = conn.cursor()
    cursor.execute('select * from musoni_payment_update_tracker where confirmation_code=%s and status=%s', (confirmation_code, 'true'))
    return True if cursor.rowcount else False

def process_payment_allocations_to_musoni(external_id, invoice_id, transaction_date, payment_amount, allocation_date, code):
    logging.info("posting payment for phone_no %s amount %s date %s code %s", external_id, payment_amount, date, code)
    transaction_details = {}
    try:
        account = fetch_saving_account_for_external_id(external_id=external_id)
        transaction_details['saving_account']  = account['id']
        logging.info("account chosen to deposit %s", account)   

        try:
            loan = fetch_loan_for_external_id(external_id=invoice_id)[0]
            transaction_details['loan']  = loan['entityId']
        except IndexError:
            return False, "no loans found for invoice", transaction_details
        except Exception as e:
            return False, str(repr(e)), transaction_details
       

        '''
        deposit -> saving
        saving -> loan (withdraw and repay)
        '''       
        #TODO: Move this to account Transfer API
        '''
        deposit -> saving
        saving -> loan (transfer)
        '''
        status, balance, deposit_transaction_id = deposit_money_to_saving_account(account['id'], payment_amount, transaction_date,  code)
        transaction_details['loan']  = loan['entityId']
        transaction_details['deposit_transaction_id'] = deposit_transaction_id
        if status:
            logging.info("sleepting for a second before withdrawing money")
            time.sleep(1)
            try:
                status, transaction_id = withdraw_money(account['id'], payment_amount, allocation_date, f'LOAN_REPAY_{loan["entityId"]}')
                transaction_details['withdrawal_transaction_id'] = transaction_id
            except Exception as e:
                logging.exception('error in withdrawing money')
                return True, str(repr(e)), transaction_details

            if status:
                response = post_repayment_to_loan(loan['entityId'], payment_amount, allocation_date, f'SA_{account["id"]}_{transaction_id}')
                transaction_details['repay_transaction_id'] = response['resourceId']
            else:
                return True, "Failed in witdrawing money", transaction_details

        return True, "allocated amount", transaction_details
        
    except Exception as e:
        logging.exception('error in updating the payment')
        return False, str(repr(e)), {}


def process_payments(payments_to_process):
    outputs = []
    total_records = len(payments_to_process)
    i = 1
    for record in payments_to_process:
        logging.info("-----processing %s record remaining %s", i, (total_records-i) )
        if not is_payment_captured(f'{record["invoice_number"]}_{record["payment_refrence_code"]}_{str(record["amount"])}'):
            output_record = {}   
            status, response, transaction_details = process_payment_allocations_to_musoni(record['customer_id'], record['invoice_number'], date.today().strftime(COMMON_DATE_FORMAT_CONVERSION), record['amount'], date.today().strftime(COMMON_DATE_FORMAT_CONVERSION), record['payment_refrence_code'])
            #TODO: Compute Available limit and update Musoni
            update_tracker_table(f'{record["invoice_number"]}_{record["payment_refrence_code"]}_{str(record["amount"])}', status, str(response))
            logging.info("post payment final results %s, %s", status, response)
            output_record.update(record)
            output_record.update(transaction_details)
            outputs.append(output_record)
        logging.info("-----completed record processing")
        i = i+1
    if outputs:
        writer = csv.DictWriter(open(f'allocation_report.csv', "w"), fieldnames=outputs[0].keys())
        writer.writeheader()
        writer.writerows(outputs)
    logging.info("execution ended")

@click.group()
def commands():
    pass

@click.command()
@click.option('--run-date', default=datetime.today(), help="date for which we need to the app")
def process_payments_from_date(run_date):
    logging.info("execution started from the date %s", run_date)
    psycopg2.extras.register_uuid()
    payments_to_process=fetch_all_payments(run_date)
    process_payments(payments_to_process)



commands.add_command(process_payments_from_date)

if __name__ == '__main__':
    commands()