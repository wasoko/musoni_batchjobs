import os
from pickle import FALSE 

COMMON_DATE_FORMAT = 'dd MMMM YYYY'
COMMON_DATE_FORMAT_CONVERSION =  '%d %b %Y'

COUNTRY_NAME_ID_MAP = {
    'Kenya':'406',
    'Tanzania':'407',	 	  
    'Uganda':'408',
    'Rwanda':'409',
    'Côte d\'Ivoire': '444'
}

LOAN_PRODUCT_ID_MAP = {
    'Kenya_14_day_0%': '401',
    'Kenya_14_day_1%': '402',
    'Kenya_7_day_0%': '403',
    'Kenya_7_day_1%': '404',
    'Kenya Smart Phone Loan': '405'
}

CUSTOMER_PROFILE_ID_MAP = {
    'Kenya-1%': 'b78625a8-d7b8-481b-a96f-c04460fffb06',
    'Kenya-0%': '0bea52b4-a79d-4d39-9a00-5c8b7ce0ca51',
    'Kenya(14)-3%': 'fbe4e99e-de26-4970-9646-de8b679727eb',
    'Kenya(14)-0%': 'e37a8849-9185-4c00-a4f6-2d925cff20c9',
    'Kenya(14)-1%': '3d3525af-bb21-433e-bd29-bfa4e0c71a15',
    'Kenya-3%': '79662ef8-2f30-4b1c-afb4-ea55243fa0dd'
}

CUSTOMER_CATEGORY_MAP = {
    'Category 1': '410',
    'Category 2': '411',
    'Category 3': '412'
}

COUNTRY_PRODUCT_ID_MAP = {
    "TZS": [29, 14],
    'KES': 'all',
    'RWF': 'all',
    'XOF': 'all',
    'UGX': 'all',
}

DEFAULT_STAFF_FOR_COUNTRY = {
    "kenya": 55, #William
    "côte d'ivoire": 57, #Stanley
    "uganda": 16, #Moses
    "rwanda": 20, #Jasmine
    "tanzania": 66, #Victor
}

GENDER_IDS = {
    'male': 218,
    'female': 219,
}

MOBILE_PHON_EXTENSIONS = {
    # "kenya": "+254",
    "côte d'ivoire": "+2250",
    "tanzania": "+255",
    "uganda": "+256",
    "rwanda": "+250"
}

MISMATCH_MOBILE_PHONE_EXTENSIONS = {
    "kenya": "+254",
    "côte d'ivoire": "+2250",
    "tanzania": "+255",
    "uganda": "+256",
    "rwanda": "+250"
}

COUNTRY_CODES = {
    "kenya": "KE",
    "côte d'ivoire": "CI",
    "tanzania": "TZ",
    "uganda": "UG",
    "rwanda": "RW"
}

CREATE_CUSTOMER_COUNTRY_LIST = ('Côte d\'Ivoire', 'Uganda', 'Rwanda', 'Tanzania', 'Zanzibar')

OFFICES_ALLOWED_IMS_UPDATE = 'all'

SKIP_OPEN_LOAN_CHECK = False

MUSONI_CLIENT_CREATION_BATCH_SIZE = 1

CLIENT_PROFILE_FIELD_MAPPING = {
        'credit_limit': 'Credit_Limit10',
        'available_limit':'Available_Credi11',
        'delinquency':'Delinquency12',
        'client_id': 'client_id',
        'date': 'submittedon_date',
        'blacklist_reason': 'Blacklist_Reason14',
        'virtual_account_no': 'Virtual_Account14',
        'loan_product_id': 'Kenya_Loan_Product_Assignment_cd_Loan_Product_As9',
        
}

COUNTRY_SMS_ENABLED_BRANCHES = {
    'TZS': 'all',
    'RWF': 'all',
    'XOF': 'all',
    'UGX': 'all',
}

ENABLE_BLACKLIST_CHECK = True

ENABLE_PENALTY_REPORT_TO_RA = True

# current_env = os.getenv('ENV', 'local')

current_env = os.getenv('MUSONI_JOBS_ENV', 'local')

BASE_WASOKO_API_URL = 'https://dev.d.api.wasoko.cloud/'

CHECK_LATEST_MUSONI_DATA_DUMP_AVAILABLE = False

if current_env == 'local':
    from .local import *
elif current_env == 'dev':
    from .dev import *
elif current_env == 'qa':
    from .qa import *
elif current_env == 'tst':
    from .qa import *
elif current_env == 'uat':
    from .uat import *
elif current_env == 'prod':
    from .prod import *
elif current_env == 'prd':
    from .prod import *
#   BASE_WASOKO_API_URL = 'https://api.wasoko.cloud/'

print(BASE_WASOKO_API_URL)

CI_AUTH_URL= f'{BASE_WASOKO_API_URL}security/auth/login'
CRM_CUSTOMER_API_URL = f'{BASE_WASOKO_API_URL}crm/v1/customers/'
OMS_PENALTY_FEE_URL = f'{BASE_WASOKO_API_URL}oms/penalty_fee'
NOTIFIER_URL= f'{BASE_WASOKO_API_URL}notifier/notify'

