#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/user_manager.py migrate-frs-from-file-to-branch frs_file.csv ${TO_BRANCH_FLAG}
popd
