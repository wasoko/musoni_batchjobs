import uuid
import warnings
import click
import psycopg2.extras
from musoni_utils import *
import traceback

psycopg2.extras.register_uuid()

warnings.filterwarnings("ignore")

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout), logging.FileHandler('AL_update_MusoniIms_v2.log')])



@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def add_customers_to_esign(file_path):
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    cursor = conn.cursor()
    output_record = []
    insert_query = """INSERT INTO public.preapproved_credit_customers (id,customer_id,approved_credit_limit,approved_loan_product,consent_status,kyc_grace_period,kyc_status,consented_at,created_at,updated_at,risk_segment,country,musoni_client_id) VALUES (%s, %s, %s, %s, 'NOT_GIVEN', 45, 't', NULL, now(), now(), 'Category 1', %s, %s)"""
    update_query = """update public.preapproved_credit_customers set approved_credit_limit=%s, approved_loan_product=%s, updated_at=now() where customer_id=%s"""
    try:
        with open(file_path) as fp:
            df = pd.read_csv(fp)
            records = df.to_dict("records")
            for record in records: 
                try:
                    logging.info("saving the record %s", record)
                    musoni_id = record.get('Musoni ID') if record.get('Musoni ID') else record.get('Musoni ID ')
                    new_limit = int(float(str(record['New Limit']).replace(',','')))
                    cursor.execute("select * from public.preapproved_credit_customers where customer_id=%s", (record['Client ID'],))
                    if cursor.rowcount == 0:
                        cursor.execute(insert_query, (str(uuid.uuid4()), record['Client ID'], new_limit, record['product'], record['country'], int(str(musoni_id).split('-')[-1])))
                    else:
                        cursor.execute(update_query, (new_limit, record['product'], record['Client ID']))
                    conn.commit()
                except:
                    print(traceback.format_exc())
                    conn.rollback()
                    output_record.append(record)
    except:
        print(traceback.format_exc())
        sys.exit(1)
    print('failed cases:',  output_record)

commands.add_command(add_customers_to_esign, 'add-customers-esign')

if __name__ == '__main__':
    commands()