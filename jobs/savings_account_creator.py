import sys
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import csv
import click

logging.basicConfig(
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

def create_savings_account(client_id, product_id, officer_id, approval_date, external_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + 'savingsaccounts'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    payload = {
    "dateFormat": "yyyy-MM-dd",
    "submittedOnDate": approval_date,
    "locale": "en_GB",
    "productId": product_id,
    "clientId": client_id,
    "fieldOfficerId": officer_id,
    "externalId": external_id
    }
    logging.info("payload for creating savings account %s", payload)
    response = requests.post(url, headers=headers, auth=auth, params = {},json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def approve_savings_account(savings_account_id, approval_date):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'savingsaccounts/{savings_account_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'approve'}
    payload = {
        "dateFormat": "yyyy-MM-dd",
        "locale": "en_GB",
        "approvedOnDate": approval_date
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json())
    return response.json()

def reopen_savings_account(savings_account_id):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'savingsaccounts/{savings_account_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'reopen'}
    payload = {
        "note": "Reopen"
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def activate_savings_account(savings_account_id, activation_date):
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + f'savingsaccounts/{savings_account_id}'
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    params = {'command':'activate'}
    payload = {
        "dateFormat": "yyyy-MM-dd",
        "locale": "en_GB",
        "activatedOnDate": activation_date
    }
    response = requests.post(url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s", response.json())
    if response.status_code != 200:
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def create_loans(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    savings_details = create_savings_account(record['Client Id'], record['Product Id'], record['Field Officer Id'], record['Activation Date'], record['External Id'])
                    approve_savings_account(savings_details['savingsId'], record['Activation Date'])
                    activate_savings_account(savings_details['savingsId'], record['Activation Date'])
                    record['savings_account_id'] = savings_details['savingsId']
                    record['status'] = 'success'
                except:
                    record['loan_id'] = 0
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
            writer = csv.DictWriter(open("loan_principal_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)


commands.add_command(create_loans)

if __name__ == '__main__':
    commands()