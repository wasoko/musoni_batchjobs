#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/user_manager.py create-field-reps confirmed_field_reps_file.csv
popd
