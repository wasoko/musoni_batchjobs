import requests
from config import *
import logging
logger = logging.getLogger(__name__)

def get_auth_token():
    payload = {
        'emailId': CI_AUTH_USERNAME,
        'password': CI_AUTH_PASSWORD,
        'applicationId': CI_AUTH_APPLICATIONID,
        'emailLogin': True
    }
    response = requests.request('post', CI_AUTH_URL, json=payload)
    logger.info("response received %s %s", response.status_code, response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + str(response.json()))
    return response.json()['data']

if __name__ == '__main__':
    print(get_auth_token())