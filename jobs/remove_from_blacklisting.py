import requests
import json
import psycopg2
import psycopg2.extras
from config import *
import click

@click.group()
def commands():
    pass

def update_available_limit(client):
    url = f"https://api.wasoko.cloud/api/fs-credit/v1/updateAvailableCreditLimit/{client}"
    headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJhcHBsaWNhdGlvbklkXCI6XCJiNTMxOGU1MC0yMTkyLTQ2NDctOWM5MC0xZTU2OWY0OWZlNjlcIixcInVzZXJJZFwiOlwiZTVkZjBkN2EtZWRmOC00MWFjLTg4M2YtYzcwNDE2ZTcxMWU2XCIsXCJlbWFpbElkXCI6XCJNdXNvbmlJbnRlZ3JhdGlvbkpvYkB3YXNva28uY29tXCIsXCJmaXJzdE5hbWVcIjpcIk11c29uaVwiLFwibGFzdE5hbWVcIjpcIkFkbWluXCIsXCJhdmF0YXJcIjpcIlwiLFwidXNlclR5cGVcIjpcIndlYlwiLFwib3JnUmVnaW9uSWRcIjpcIjgwNzI0NGMxLWQ0OWQtNGM4Zi1iMmFkLTlkYTdlMzlmMmMwNlwifSIsImlhdCI6MTY4NTM3MjM1MiwiZXhwIjoxNjg1OTc3MTUyfQ.W2fTSdrYhmswyOonjGONGv6o6jiGEGRPbgsmKdbzoBo'
    }
    response = requests.request("PUT", url, headers=headers)

    print(response.text)
    

def clear_blacklisted(client):
    
    url = f"https://api.live.irl.musoniservices.com/v1/datatables/ml_client_details/{client}/?apptable=m_client"

    payload = json.dumps({
      "dateFormat": "dd-MM-yyyy",
      "locale": "en_GB",
      "submittedon_date": "28-06-2022",
      "Blacklist_Reaso13": ""
    })

    headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic TXVzb25pSW50ZWdyYXRpb25Kb2I6TXVzb25pQDEyMw==',
      'x-api-key': MUSONI_API_KEY,
      'X-Fineract-Platform-TenantId': 'wasoko'
     }

    response = requests.request("PUT", url, headers=headers, data=payload)

    print(response.text)

def remove_customer_from_blacklist(client):   
    connection = psycopg2.connect(user=DB_CONNECTION_USERNAME, password=DB_CONNECTION_PASSWORD, host=DB_CONNECTION_HOST, port=DB_CONNECTION_PORT, database=DB_CONNECTION_DB)
    cursor = connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    query = "delete from fs_blacklisted_clients where client_musoni_id=%s"
    cursor.execute(query, (client,))
    update_kyc_status = "update preapproved_credit_customers set kyc_status=true, updated_at=now() where musoni_client_id=%s"
    cursor.execute(update_kyc_status, (client,))
    connection.commit()
    
@click.command()
@click.argument('client')
def remove_customer_from_blacklisting(client):
    clients_list  = client.split(',')
    for client_id in clients_list:
        remove_customer_from_blacklist(client_id)
        clear_blacklisted(client_id)
        update_available_limit(client_id)

commands.add_command(remove_customer_from_blacklisting, 'remove')

if __name__ == '__main__':
    commands()