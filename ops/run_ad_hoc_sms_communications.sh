#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/payment_sms_remainders.py ad-hoc-sms-communication sms_notifications.xlsx
popd
