from asyncio.log import logger
import sys
import logging
from uuid import uuid4
import click
import pandas as pd
from config import *
import psycopg2
import psycopg2.extras
from sqlalchemy import create_engine
import musoni_utils
from datetime import datetime, timedelta

logging.basicConfig(
    encoding='utf-8',
    level=logging.DEBUG,
    handlers=[logging.StreamHandler(sys.stdout)])

@click.group()
def commands():
    pass


@click.command()
def blacklist_risky_pre_approved_credit_clients():
    postgres_conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    postgres_cursor = postgres_conn.cursor()

    try:
        # Get the clients from preapproved_credit_customers
        postgres_query = """
                SELECT pcc.id, pcc.customer_id, pcc.approved_credit_limit, pcc.approved_loan_product, pcc.consent_status, pcc.kyc_grace_period,
                pcc.kyc_status, pcc.consented_at, pcc.created_at, pcc.updated_at, pcc.risk_segment, pcc.musoni_client_id
                FROM preapproved_credit_customers pcc
                LEFT JOIN fs_blacklisted_clients fbc ON pcc.musoni_client_id = fbc.client_musoni_id
                WHERE pcc.created_at < CURRENT_DATE - INTERVAL '1 day' * pcc.kyc_grace_period
                AND fbc.client_musoni_id IS NULL;
            """
        postgres_cursor.execute(postgres_query)
        preapproved_clients = postgres_cursor.fetchall()

        # Check if preapproved_clients is empty
        if not preapproved_clients:
            logging.info("No clients found in preapproved_credit_customers that meet the criteria.")
            return

        # Iterate over the clients to update their credit profile and fs_blacklisted_clients
        for client in preapproved_clients:
            customer_id = client[1]
            musoni_client_id = str(client[11])

            # Update fs_blacklisted_clients
            insert_query = """
                               INSERT INTO fs_blacklisted_clients (id, client_ims_id, client_musoni_id, reason, source, created_at, updated_at)
                               VALUES (%s, %s, %s, %s, %s, now(), now())
                           """

            logger.info("Updating the clients fs_blacklisted_clients with client:: %s", musoni_client_id)
            postgres_cursor.execute(insert_query, (str(uuid4()), customer_id, musoni_client_id, 'Pending for KYC', 'Musoni close kyc job'))

            # Call musoni_utils.update_client_credit_details API
            payload = {
                'client': musoni_client_id,
                'date': datetime.now().strftime('%Y-%m-%d'),
                'date_format': 'yyyy-mm-dd',
                'blacklist_reason': 'Pending for KYC'
            }
            logger.info("Updating the clients credit limit for:: %s", musoni_client_id)
            try:
                musoni_utils.update_client_credit_details(musoni_client_id, payload)
                logging.info("Successfully updated client credit details for musoni_client_id: %s", musoni_client_id)
            except Exception as e:
                logging.exception("Error in updating client credit details for musoni_client_id: %s", musoni_client_id)

            # Remove credit profile from customers
            remove_credit_query = """
                    UPDATE customers
                    SET credit_limit = 0
                    WHERE id = %s
                """
            postgres_cursor.execute(remove_credit_query, (customer_id,))
            postgres_conn.commit()
        logging.info("Blacklisting process completed successfully.")

    except Exception as e:
        logging.exception("Error in blacklisting clients: %s", str(e))
    finally:
        # Close the PostgreSQL connection
        postgres_cursor.close()
        postgres_conn.close()




@click.command()
@click.option('--max-default-days', default=30)
def blacklist_risky_clients(max_default_days):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    risky_clients_query = '''
    with  loan_data as (
     select
          client_id,
          id as loan_id,
          external_id,
          total_outstanding_derived as balance,
          expected_maturedon_date,
          DATEDIFF(NOW(), m_loan.expected_maturedon_date) as dpd

    from m_loan
    where total_outstanding_derived > 0 and DATE(disbursedon_date)>= DATE('2022-07-01')
    )
    
    select  m_client.external_id as client_ims_id,
            loan_data.client_id as client_musoni_id,
            "risky_client" as reason,
            "musoni_job" as source,
            DATE_FORMAT(NOW(), '%%Y-%%m-%%d') as created_at    
    from loan_data
    left join m_client m_client on m_client.id = loan_data.client_id
    left join m_staff staff on staff.id = m_client.staff_id
    left join ml_client_details ml_client_details on ml_client_details.client_id = m_client.id
    left join m_office m_office on m_office.id  = m_client.office_id
    left join m_code_value m_code_value on m_code_value.id  = ml_client_details.COUNTRY_cd_Country8    
    group by 
            m_client.external_id,
            loan_data.client_id,
            m_client.display_name,
            m_client.mobile_no,      
            m_office.name,
            m_code_value.code_value
    having count(CASE WHEN dpd > %s then loan_id end) > 0
    '''
    try:
        try:
            table_fetch_cursor = conn.cursor()
            table_fetch_cursor.execute("SELECT client_ims_id FROM fs_blacklisted_clients")
            blacklisted_clients = [item[0] for item in table_fetch_cursor.fetchall()]
            logging.info("blacklisted clients:: %s", blacklisted_clients)
            risky_clients = pd.read_sql_query(risky_clients_query,
                                              engine, params=[max_default_days, ]).to_records(index=False)

            logging.info("risky clients:: %s", risky_clients)
        except:
            logging.exception("error in fetching clients")

        try:
            if blacklisted_clients is None:
                distinct_risky_clients = risky_clients
            else:
                distinct_risky_clients = [c for c in risky_clients if c['client_ims_id'] not in blacklisted_clients]
                # distinct_risky_clients = [c for c in risky_clients if c['client_ims_id'] in ["04ca4a0c-7d40-4f35-9b06-4a342a785b44"]]

            logging.info("distinct_risky_clients:: %s", distinct_risky_clients)
            remove_credit_profile_cursor = conn.cursor()
            table_population_cursor = conn.cursor()
            remove_credit_profile = """
                           UPDATE customers 
                           SET credit_limit = 1
                           WHERE id = %s;
                       """
            insert = """
                INSERT INTO
                    fs_blacklisted_clients (id, client_ims_id, client_musoni_id, reason, source, created_at, updated_at)
                VALUES (%s, %s, %s, %s, %s, %s, %s);"""

            for rec in distinct_risky_clients:
                table_population_cursor.execute(insert, (str(uuid4()), rec[0], str(rec[1]), 'Defaulted above {} days'.format(max_default_days) , rec[3], rec[4], rec[4]))
                client_ims_id = rec[0]
                musoni_client_id = rec[1]
                payload = {
                    'client': musoni_client_id,
                    'date': rec[4],
                    'date_format': 'yyyy-mm-dd',
                    'blacklist_reason': 'Defaulted above {} days'.format(max_default_days)
                }
                logger.info("updating the clients credit limit for: %s", musoni_client_id)
                try:
                    musoni_utils.update_client_credit_details(musoni_client_id, payload)
                except:
                    logger.exception("error in updating reason for client %s", rec)
                logger.info("updating ims to ensure customer is no longer a credit client: %s", client_ims_id)
                remove_credit_profile_cursor.execute(remove_credit_profile, (client_ims_id,))
            conn.commit()
            conn.close()
        except:
            logging.exception("failed in blacklist table population")
    except:
        logging.exception("error in creating table")
        conn.close()


@click.command()
@click.option('--writeoff-date', default=datetime.today().strftime("%Y-%m-%d"))
def blacklist_write_off_clients(writeoff_date):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    placeholders = ", ".join(["%s" for _ in BLACKLIST_WRITEOFF_REASON_CODES])
    write_off_clients_query = '''            
    select  m_client.external_id as client_ims_id,
            loan.client_id as client_musoni_id,
            mcv.code_value as reason,
            "musoni_job" as source,
            DATE_FORMAT(NOW(), '%%Y-%%m-%%d') as created_at   
    from m_loan loan
    left join (select * from r_enum_value  where enum_name = 'loan_status_id')enumval on enumval.enum_id = loan.loan_status_id
    left join m_client on m_client.id  = loan.client_id
    left join m_office orr on orr.id = loan.office_id
    left join m_code_value mcv on mcv.id=loan.writeoff_reason_cv_id
    where enumval.enum_value = 'Written-Off' and 
    loan.writeoff_reason_cv_id in ({}) and
    loan.writtenoffon_date = %s
    group by
            loan.client_id,
            m_client.external_id,
            loan.writeoff_reason_cv_id,
            m_client.display_name,
            m_client.mobile_no,
            mcv.id              
    '''.format(placeholders)

    try:
        try:
            table_fetch_cursor = conn.cursor()
            table_fetch_cursor.execute("SELECT client_ims_id FROM fs_blacklisted_clients")
            blacklisted_clients = [item[0] for item in table_fetch_cursor.fetchall()]
            logging.info("blacklisted clients:: %s", blacklisted_clients)
            write_off_clients = pd.read_sql_query(write_off_clients_query, engine,  params=(*BLACKLIST_WRITEOFF_REASON_CODES, writeoff_date)).to_records(index=False)
            logging.info("write off clients:: %s", write_off_clients)
        except:
            logging.exception("error in fetching clients")
        try:
            if blacklisted_clients is None:
                distinct_write_off_clients = write_off_clients
            else:
                # distinct_write_off_clients = [c for c in write_off_clients if c['client_ims_id'] in ["04ca4a0c-7d40-4f35-9b06-4a342a785b44"]]
                distinct_write_off_clients = [c for c in write_off_clients if c['client_ims_id'] not in blacklisted_clients]

            logging.info("distinct_write_off_clients:: %s", distinct_write_off_clients)
            table_population_cursor = conn.cursor()
            remove_credit_profile_cursor = conn.cursor()
            remove_credit_profile = """
                UPDATE customers 
                SET customer_profile_id = NULL
                WHERE id = %s;
            """
            insert = """
                INSERT INTO
                    fs_blacklisted_clients (id, client_ims_id, client_musoni_id, reason, source, created_at, updated_at)
                VALUES (%s, %s, %s, %s, %s, %s, %s);
            """

            if len(distinct_write_off_clients) > 0:
                for rec in distinct_write_off_clients:
                    try:
                        logging.info("client:: %s", rec)
                        table_population_cursor.execute(insert, (str(uuid4()), rec[0], str(rec[1]), rec[2], rec[3], rec[4], rec[4]))
                        client_ims_id = rec[0]
                        musoni_client_id = rec[1]
                        payload = {
                            'credit_limit': 0,
                            'client': musoni_client_id,
                            'date': rec[4],
                            'date_format': 'yyyy-mm-dd',
                            'blacklist_reason': rec[2]
                        }
                        logger.info("updating the clients credit limit for: %s", musoni_client_id)
                        musoni_utils.update_client_credit_details(musoni_client_id, payload)
                        logger.info("updating ims to ensure customer is no longer a credit client: %s", client_ims_id)
                        remove_credit_profile_cursor.execute(remove_credit_profile, (client_ims_id, ))
                    except:
                        logger.exception("error in adding record to blacklisted %s", rec)
                conn.commit()
            else:
                logger.info("no distinct write off clients found")
            conn.close()
        except:
            logging.exception("failed in blacklist table population")
    except:
        logging.exception("error in creating table")
        conn.close()

@click.command()
@click.argument('file-path')
def retro_blacklisting(file_path):
    df = pd.read_csv(file_path)
    records = df.to_dict("records")
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    insert_query = """
            INSERT INTO
                fs_blacklisted_clients (id, client_ims_id, client_musoni_id, reason, source, created_at, updated_at)
            VALUES (%s, %s, %s, %s, %s, now(), now());"""    
    remove_credit_profile = """
                UPDATE customers 
                SET credit_limit = 1
                WHERE id = %s;
            """
    cursor = conn.cursor()
    logging.info("starting to process the records %s", df.count)
    for record in records:
        try:
            cursor.execute(insert_query, (str(uuid4()), record['customer_id'], str(record['client_id']), record['reason'] , 'musoni_retro_job'))
            client_ims_id = record['customer_id']
            musoni_client_id = record['client_id']
            payload = {
                'client': musoni_client_id,
                'date': datetime.today().strftime("%Y-%m-%d"),
                'date_format': 'yyyy-mm-dd',
                'blacklist_reason': record['reason']
            }
            logger.info("updating the clients credit limit for: %s", musoni_client_id)
            try:
                musoni_utils.update_client_credit_details(musoni_client_id, payload)
            except:
                logger.exception("error in updating reason for client %s", record)
            logger.info("updating ims to ensure customer is no longer a credit client: %s", client_ims_id)
            cursor.execute(remove_credit_profile, (client_ims_id,))
            conn.commit()
        except:
            logging.exception("error in processing the records %s", record)
            conn.rollback()


commands.add_command(blacklist_risky_pre_approved_credit_clients, 'blacklist-risky-pre-approved-credit-clients')
commands.add_command(blacklist_risky_clients, 'blacklist_clients')
commands.add_command(blacklist_write_off_clients, 'write_off_clients')
commands.add_command(retro_blacklisting, 'run-for-old-cases')

if __name__ == '__main__':
    commands()
