from cmath import log
from datetime import datetime
import sys
import csv
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import click
from rich.prompt import Prompt
from rich.progress import track
from rich.console import Console
from rich.table import Table

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


def call_musoni_to_create_client_profile(client_id, payload):
    relative_url = f"datatables/ml_client_details/{client_id}/".format(client_id=client_id)
    logging.info("calling musoni API endpoint(%s) with params and payload %s ", relative_url, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.post(url, headers=headers, auth=auth, params = {},json=payload)
    logging.info("response received %s %s", response.status_code, response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + response.json()['defaultUserMessage'])
    return response.json()

def get_payload(data):
    payload =     {
        "town": None,
        "address": None,
        "state": None,
        "submittedon_userid": None,
        "submittedon_date": None,
        "Customer_Risk_Segment_cd_Customer_Risk_S7": CUSTOMER_CATEGORY_MAP[data['Customer Risk Segment (aka Category)']],
        "COUNTRY_cd_Country8": COUNTRY_NAME_ID_MAP[data['Country']],
        "Kenya_Loan_Product_Assignment_cd_Loan_Product_As9": LOAN_PRODUCT_ID_MAP[data['Loan Product Assignment']],
        "Credit_Limit10": data['Credit Limit'],
        "Available_Credi11": None,
        "Delinquency12": None,
        "locale":"en"
    }
    return payload

@click.group()
def commands():
    pass

@click.command()
def create_client_profile():
   client_id = Prompt.ask('client id')
   country = Prompt.ask('country', default='Kenya')
   product = Prompt.ask('loan product')
   category = Prompt.ask('client risk segment')
   credit_limit = Prompt.ask('credit limit')
   data = {
       'Customer Risk Segment (aka Category)': category,
       'Country': country,
       'Loan Product Assignment': product,
       'Credit Limit': credit_limit,
   }
   payload = get_payload(data)
   call_musoni_to_create_client_profile(client_id, payload)

def display_result(total, success, error):
    table = Table(title="Client Profile creation")

    table.add_column("Total", justify="center", style="cyan",)
    table.add_column("Success",justify="center", style="green")
    table.add_column("Error", justify="center", style="red")

    table.add_row(str(total), str(success), str(error))
    console = Console()
    console.print(table)

@click.command()
@click.argument('file-path')
def process_csv_file(file_path):
    error_count = 0
    success_count = 0
    total_records_to_process = 0
    error_records = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            total_records_to_process =len(list(reader))
            fp.seek(0)
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    payload = get_payload(record)
                    call_musoni_to_create_client_profile(record['Musoni Client ID'], payload=payload)
                    success_count = success_count + 1
                except:
                    error_count = error_count + 1
                    error_records.append(record)
                    logging.exception("error in processing the record %s", record)
    except:
        logging.exception("error in processing file")
    finally:
        display_result(total_records_to_process, success_count, error_count)

commands.add_command(create_client_profile)
commands.add_command(process_csv_file)

if __name__ == '__main__':
    commands()