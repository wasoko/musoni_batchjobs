CREATE TABLE IF NOT EXISTS public.musoni_credit_limit_update_tracker (
    id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    customer_id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    existing_limit varchar(20) NULL,
    new_limit varchar(20) NULL,
    ims varchar(20) NULL,
    musoni varchar(20) NULL,
    triggered_by varchar(20) NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);