import sys
import logging
import csv
import click
import pandas as pd
import musoni_utils
from config import *
import secrets
import string
from datetime import datetime
from sqlalchemy import create_engine



logging.basicConfig(
    encoding='utf-8',
    level=logging.DEBUG,
    handlers=[logging.StreamHandler(sys.stdout)])


def get_office_id_by_name(offices, office_name):
    for office in offices:
        if office['name'].lower() == office_name.lower():
            return office['id']
    raise Exception(f"No office found for then given office name ${office_name}")


def create_staff_and_user_from_record(record, office_id):
    staff_payload = {}
    user_payload = {}
    digits = string.digits
    try:
        staff_payload["officeId"] = office_id
        staff_payload["isLoanOfficer"] = "true"
        staff_payload["isActive"] = "true"
        staff_payload["firstname"] = record['Email'].split('@')[0]
        staff_payload["lastname"] = record['Name']
        staff_payload["externalId"] = record["ID"]
        staff_payload["mobileNo"] = record["Phone number"]
        staff_payload["joiningDate"] = datetime.today().strftime("%Y-%m-%d")
        staff_payload["dateFormat"] = 'yyyy-mm-dd'
        staff_payload["locale"] = 'en'

        logging.info("Payload used for staff creation:: %s", staff_payload)

        # Making the POST call to /staff api, with the above payload to creat staff.
        staff_response = musoni_utils.create_staff(staff_payload)

        # Capturing all the important values for the output file.
        # Assuming the Staff creation was successful.
        record['isActive'] = staff_payload["isActive"]
        record['isLoanOfficer'] = staff_payload["isLoanOfficer"]
        record['joiningDate'] = staff_payload["joiningDate"]
        record['dateFormat'] = staff_payload["dateFormat"]
        record['locale'] = staff_payload["locale"]

        # Adding the Staff ID to the payload to be used for User creation.
        user_payload["staffId"] = staff_response['resourceId']

        user_payload["firstname"] = staff_payload['firstname']
        user_payload["lastname"] = staff_payload['lastname']
        user_payload["officeId"] = staff_payload['officeId']
        user_payload["username"] = staff_payload["firstname"]

        # Fail safe in case there is no email value
        user_payload["email"] = record['Email'] if record[
            'Email'] else f"{user_payload['username']}@wasokofs.com"
        user_payload["roles"] = FIELD_REP_ROLE
        user_payload["sendPasswordToEmail"] = False

        # Generating a unique password for each record.
        user_payload["password"] = "wasoko" + ''.join([secrets.choice(digits) for d in range(3)])
        user_payload["repeatPassword"] = user_payload["password"]
        logging.info("Payload used for User creation:: %s", user_payload)

        # Making the POST call to /users api, with payload to creat a user.
        response = musoni_utils.create_user(user_payload)

        # Capturing all the important values for the output file.
        # Assuming the User creation was successful.
        record["staffId"] = staff_response['resourceId']
        record['Username'] = user_payload["username"]
        record['Password'] = user_payload["password"]
        record['officeId'] = response['officeId']
        record['userId'] = response['resourceId']
        record['status'] = True
        record['comment'] = 'Processed Successfully'
    except Exception as e:
        # Empty fields added to output file for uniformity and to prevent Value Error.
        record['officeId'] = ""
        record['staffId'] = ""
        record['userId'] = ""
        record['Username'] = ""
        record['locale'] = ""
        record['isActive'] = ""
        record['joiningDate'] = ""
        record['Password'] = ""
        record['isLoanOfficer'] = ""
        record['dateFormat'] = ""

        record['status'] = False
        record['comment'] = str(e)
        return e
    logging.info("record after processing :: %s", record)
    return record


def get_staff_by_external_id(record):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))
    query = '''
     SELECT  m_appuser.id as id,
          m_staff.id as staff_id,
          m_appuser.office_id,
          m_appuser.firstname,
          m_appuser.lastname,
                   email,
                   external_id
          from m_appuser join m_staff on m_staff.id = m_appuser.staff_id 
          where m_staff.external_id = '{}';
          '''

    return pd.read_sql_query(query.format(record['ID']), engine).to_dict('records')

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def delete_users(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            df = pd.read_csv(fp, dtype=str)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record %s", record)
                try:
                    response = musoni_utils.delete_user(record['resourceId'])
                    record['status'] = True
                    record['comment'] = 'Deleted Successfully'
                    logging.info("response:: %s", response)
                except Exception as e:
                    record['status'] = False
                    record['comment'] = str(e)
                    logging.exception("error in processing the records %s", record)

                output_record.append(record)

    except Exception as e:
        logging.exception("error in processing this file:: ", e)
        sys.exit(1)

    if output_record:
        writer = csv.DictWriter(open("deleting_users_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)

@click.command()
@click.argument('file-path')
def create_field_reps(file_path):
    output_record = []
    offices = musoni_utils.get_offices()
    try:
        with open(file_path) as fp:
            df = pd.read_csv(fp, keep_default_na=False, dtype=str)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record %s", record)
                staff_payload = {}
                user_payload = {}
                digits = string.digits
                try:
                    # Getting the office Id value via an external function.
                    office_id = get_office_id_by_name(offices, record['branch'])
                    staff_payload["officeId"] = office_id
                    staff_payload["isLoanOfficer"] = "true"
                    staff_payload["isActive"] = "true"
                    staff_payload["firstname"] = record['email'].split('@')[0]
                    staff_payload["lastname"] = f"{record['first_name']} {record['last_name']}"
                    staff_payload["externalId"] = record["id"]
                    staff_payload["mobileNo"] = record["phone_number"]
                    staff_payload["joiningDate"] = datetime.today().strftime("%Y-%m-%d")
                    staff_payload["dateFormat"] = 'yyyy-mm-dd'
                    staff_payload["locale"] = 'en'

                    logging.info("Payload used for staff creation:: %s", staff_payload)

                    # Making the POST call to /staff api, with the above payload to creat staff.
                    staff_response = musoni_utils.create_staff(staff_payload)

                    # Capturing all the important values for the output file.
                    # Assuming the Staff creation was successful.
                    record['isActive'] = staff_payload["isActive"]
                    record['isLoanOfficer'] = staff_payload["isLoanOfficer"]
                    record['joiningDate'] = staff_payload["joiningDate"]
                    record['dateFormat'] = staff_payload["dateFormat"]
                    record['locale'] = staff_payload["locale"]

                    # Adding the Staff ID to the payload to be used for User creation.
                    user_payload["staffId"] = staff_response['resourceId']

                    user_payload["firstname"] = staff_payload['firstname']
                    user_payload["lastname"] = staff_payload['lastname']
                    user_payload["officeId"] = staff_payload['officeId']
                    user_payload["username"] = staff_payload["firstname"]

                    # Fail safe in case there is no email value
                    user_payload["email"] = record['email'] if record['email'] else f"{user_payload['username']}@wasokofs.com"
                    user_payload["roles"] = FIELD_REP_ROLE
                    user_payload["sendPasswordToEmail"] = False

                    # Generating a unique password for each record.
                    user_payload["password"] = "wasoko" + ''.join([secrets.choice(digits) for d in range(3)])
                    user_payload["repeatPassword"] = user_payload["password"]
                    logging.info("Payload used for User creation:: %s", user_payload)

                    # Making the POST call to /users api, with payload to creat a user.
                    response = musoni_utils.create_user(user_payload)

                    # Capturing all the important values for the output file.
                    # Assuming the User creation was successful.
                    record["staffId"] = staff_response['resourceId']
                    record['Username'] = user_payload["username"]
                    record['Password'] = user_payload["password"]
                    record['officeId'] = response['officeId']
                    record['userId'] = response['resourceId']
                    record['status'] = True
                    record['comment'] = 'Processed Successfully'

                except Exception as e:
                    # Empty fields added to output file for uniformity and to prevent Value Error.
                    record['officeId'] = ""
                    record['staffId'] = ""
                    record['userId'] = ""
                    record['Username'] = ""
                    record['locale'] = ""
                    record['isActive'] = ""
                    record['joiningDate'] = ""
                    record['Password'] = ""
                    record['isLoanOfficer'] = ""
                    record['dateFormat'] = ""

                    record['status'] = False
                    record['comment'] = str(e)
                    logging.exception("error in processing the records %s", record)
                logging.info("record after processing :: %s", record)
                output_record.append(record)


    except Exception as e:
        logging.exception("error in processing this file:: ", e)
        sys.exit(1)

    if output_record:
        writer = csv.DictWriter(open("creating_field_reps_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)

@click.command()
@click.argument('file-path')
@click.argument('to-branch-flag')
def migrate_frs_from_file_to_branch(file_path, to_branch_flag):
    output_record = []
    branch_to_office_map = {
        'TO_THIKA': {
            'officeId': BRANCH_OFFICE_IDS_MAP[2]['office_id']
        },
        'TO_KIGALI': {
            'officeId': BRANCH_OFFICE_IDS_MAP[4]['office_id']
        },
        'TO_DAR_CENTRAL': {
            'officeId': BRANCH_OFFICE_IDS_MAP[7]['office_id']
        },
    }
    to_branch_office_id = branch_to_office_map[to_branch_flag]['officeId']
    try:
        with open(file_path) as fp:
            df = pd.read_csv(fp, keep_default_na=False, dtype=str)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record:: %s", record)

                reader = get_staff_by_external_id(record)
                logging.info("staff with external_id:: %s", reader)

                try:
                    if not reader:
                        response = create_staff_and_user_from_record(record, to_branch_office_id)
                        logging.info("creating_staff_and_user_response:: %s", response)
                    else:
                        logging.info("client details :: %s", reader[0])
                        user_id = reader[0]['id']
                        staff_id = reader[0]['staff_id']
                        staff_update_payload = {
                            'officeId': to_branch_office_id
                        }
                        staff_update_response = musoni_utils.update_staff_by_id(staff_id, staff_update_payload)
                        user_update_payload = {
                            'officeId': to_branch_office_id,
                            'staffId': staff_update_response['resourceId']
                        }
                        musoni_utils.update_user_by_id(user_id, user_update_payload)

                    record['status'] = 'success'
                    record['new_office_id'] = to_branch_office_id
                except Exception as e:
                    record['status'] = 'fail'
                    record['message'] = str(e)
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)


    except Exception as e:
        logging.exception("error in processing this file:: ", e)
        sys.exit(1)

    if output_record:
        writer = csv.DictWriter(open("migrating_field_reps_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


commands.add_command(create_field_reps, 'create-field-reps')
commands.add_command(migrate_frs_from_file_to_branch, 'migrate-frs-from-file-to-branch')

if __name__ == '__main__':
    commands()
