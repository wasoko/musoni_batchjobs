#!/bin/bash -e
#use cases

#./ops/app.sh build
#./ops/app.sh run
#./ops/app.sh

DIR=$( cd "$( dirname "$0" )" && pwd )
export ACT=$1

echo "Ruby version = $(ruby --version)"
echo "gem version = $(gem --version)"
echo "bundle version = $(bundle --version)"
echo "bundle installing"
bundle install

pushd $DIR/..
  if [ "${ACT}" == 'build' ] ; then
    rails assets:precompile
    touch tmp/src.json; cp tmp/src.json public/info.json
  elif [ "${ACT}" == 'run' ] ; then
    rails s
  else
    #export ENV=dev
    #export ENVTYPE=d
    rails db:migrate
  fi
popd
