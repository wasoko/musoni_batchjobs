from datetime import date, datetime
import json
import csv
import sys
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import uuid
import click
import time

logging.basicConfig(
    encoding='utf-:', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

def fetch_data(method, relative_url, params=None, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s %s", response.status_code, response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + str(response.json()['errors']))
    return response.json()

def fetch_client_ids_for_phone(phone_number):
    logging.info("searching client for phone_number %s", phone_number)
    relative_url = 'search'
    params = {'query':str(phone_number).replace('+254','0'), 'resource': 'clients'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def fetch_saving_account_for_external_id(external_id):
    logging.info("searching saving for external_id %s", external_id)
    relative_url = 'search'
    params = {'query':str(external_id), 'resource': 'savings'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def fetch_loan_for_external_id(external_id):
    logging.info("searching loans for external_id %s", external_id)
    relative_url = 'search'
    params = {'query':str(external_id), 'resource': 'loans'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def get_all_accounts_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?'.format(client_id)
    method = 'GET'
    response  = fetch_data(method, relative_url)
    return response 

def post_repayment_to_loan(loan_id, amount, payment_date, code):
    logging.info("posting the payment to loan %s %s %s %s", loan_id, amount, payment_date, code)
    relative_url = 'loans/{}/transactions'.format(loan_id)
    method = 'POST'
    params = {'command': 'repayment'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "receiptNumber": code,
        "transactionDate": payment_date,
        "transactionAmount": amount,
        "paymentTypeId": "218",
        "note": "loan repay payment",
        "accountNumber": "",
        "checkNumber": "",
        "routingCode": "",
        "bankNumber": ""
        }
    response  = fetch_data(method, relative_url, params, payload)
    return response

def deposit_money(saving_account_id, payment_amount, date, code):
    relative_url = f'savingsaccounts/{saving_account_id}/transactions'
    params = {'command': 'deposit'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "transactionDate": date,
        "transactionAmount": payment_amount,
        "paymentTypeId": "177",
        "receiptNumber": code,
        }
    response  = fetch_data('POST', relative_url, params, payload)
    return True, response['resourceId']

def get_current_balance(saving_account_id):
    relative_url = f'savingsaccounts/{saving_account_id}/'
    response  = fetch_data('GET', relative_url, {}, {})
    return response['summary']['accountBalance']

def withdraw_money(saving_account_id, payment_amount, date, code):
    relative_url = f'savingsaccounts/{saving_account_id}/transactions'
    params = {'command': 'withdrawal'}
    payload = {
            "dateFormat": "dd MMMM yyyy",
            "locale": "en",
            "transactionDate": date,
            "transactionAmount": payment_amount,
            "paymentTypeId": "177",
            "receiptNumber": code,
        }
    response  = fetch_data('POST', relative_url, params, payload)
    return True, response['resourceId']

def deposit_money_to_saving_account(saving_account_id, payment_amount, date, code):
    try:
        status, transaction_id = deposit_money(saving_account_id, payment_amount, date, code)
        balance = get_current_balance(saving_account_id)
        return True, balance, transaction_id
    except:
        raise Exception(f'Error in depsiting money for {saving_account_id}')

def fetch_all_payments(date=datetime.today()):
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    query = """select ci.invoice_number, pc.customer_id, cip.amount, cpa.payment_amount, cpa.payment_refrence_code, pc.transaction_amount, pc.transaction_time, cfp.payment_amount, cfp.created_at, ipa.created_at, cfp.sales_order_id
                from invoice_payment_allocations ipa
                join customer_invoice_payments cip on ipa.id=cip.invoice_payment_allocation_id
                join customer_payment_details cpa on cip.customer_payment_id = cpa.customer_payment_id
                join payment_confirmations pc on pc.confirmation_code = cpa.payment_refrence_code
                left join credit_fee_payments cfp on cfp.payment_refrence_code = cpa.payment_refrence_code
                join customer_invoices ci on ci.id = ipa.customer_invoice_id
                left join sales_orders so on so.id = cfp.sales_order_id
                left join delivery_notes dn on dn.id=so.delivery_note_id
                left join customer_invoices cif on cif.id = dn.customer_invoice_id
                where date(ipa.created_at) = %s and pc.till_number = %s and ((ci.invoice_number = cif.invoice_number and cip.amount = cfp.payment_amount) = false or (ci.invoice_number = cif.invoice_number and cip.amount = cfp.payment_amount) is null) and (cfp.status = true or cfp.status is null)"""
    cursor.execute(query, (date, CREDIT_TILL_NUMBERS))
    logging.info("no of rows to process %s", cursor.rowcount)
    rows = cursor.fetchall()
    return rows

def update_tracker_table(confirmation_code, status, reason):
    logging.info("updating tracker table with following detail %s", (confirmation_code, status, reason))
    cursor = conn.cursor()
    cursor.execute('insert into musoni_payment_update_tracker values (%s, %s, %s, %s, %s, %s)', (uuid.uuid4(), confirmation_code, status, reason, datetime.now(), datetime.now()))
    conn.commit()

def is_payment_captured(confirmation_code):
    cursor = conn.cursor()
    cursor.execute('select * from musoni_payment_update_tracker where confirmation_code=%s and status=%s', (confirmation_code, 'true'))
    return True if cursor.rowcount else False

def process_payment_allocations_to_musoni(external_id, loan_id, payment_amount, allocation_date):
    transaction_details = {
        'savings_account': '',
        'withdrawal_transaction_id': '',
        'repay_transaction_id': ''
    }
    try:
        saving_accounts = fetch_saving_account_for_external_id(external_id=external_id)

        if not saving_accounts:
            return False, "no saving accounts for the client", transaction_details

        account = saving_accounts[0]
        transaction_details['saving_account']  = account['entityId']

        logging.info("account chosen to deposit %s", account)   
       
        status = True
        if status:
            logging.info("sleepting for a second before withdrawing money")
            time.sleep(1)
            try:
                status, transaction_id = withdraw_money(account['entityId'], payment_amount, allocation_date, f'LOAN_REPAY_{loan_id}')
                transaction_details['withdrawal_transaction_id'] = transaction_id
            except Exception as e:
                logging.exception('error in withdrawing money')
                return False, str(repr(e)), transaction_details

            if status:
                response = post_repayment_to_loan(loan_id, payment_amount, allocation_date, f'SA_{account["entityId"]}_{transaction_id}')
                transaction_details['repay_transaction_id'] = response['resourceId']
            else:
                return False, "Failed in posting money", transaction_details

        return True, "allocated amount", transaction_details
        
    except Exception as e:
        logging.exception('error in updating the payment')
        return False, str(repr(e)), {}


@click.group()
def commands():
    pass


@click.command()
@click.argument('file-path')
def process_csv_file(file_path):
    error_count = 0
    success_count = 0
    total_records_to_process = 0
    error_records = []
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            total_records_to_process =len(list(reader))
            fp.seek(0)
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    status, reason, transaction_details = process_payment_allocations_to_musoni(record['External Client ID'], record['Account No'], record['amount_to_post'], record['payment_date'])
                    success_count = success_count + 1
                    record.update(transaction_details)
                    record['status'] = status
                except:
                    error_count = error_count + 1
                    error_records.append(record)
                    record['outstanding_after_payment'] = 0
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    
    if output_record:
            writer = csv.DictWriter(open("loan_correction_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)

commands.add_command(process_csv_file)

if __name__ == '__main__':
    commands()