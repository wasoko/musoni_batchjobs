from datetime import date, datetime
import json
import sys
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from config import *
import logging
import uuid
import click

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )

def fetch_data(method, relative_url, params=None, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params,json=payload)
    logging.info("response received %s %s", response.status_code, response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + response.json()['developerMessage'])
    return response.json()

def fetch_client_ids_for_phone(phone_number):
    logging.info("searching client for phone_number %s", phone_number)
    relative_url = 'search'
    params = {'query':str(phone_number).replace('+254','0'), 'resource': 'clients'}
    method = 'GET'
    response  = fetch_data(method, relative_url, params)
    return response

def get_all_accounts_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?'.format(client_id)
    method = 'GET'
    response  = fetch_data(method, relative_url)
    return response 

def fetch_all_loans_for_client(client_id):
    accounts = get_all_accounts_for_client(client_id)
    return accounts.get('loanAccounts', [])

def fetch_saving_account_for_client(client_id):
    accounts = get_all_accounts_for_client(client_id)
    return accounts.get('savingsAccounts', [])

def post_repayment_to_loan(loan_id, amount, payment_date, code):
    logging.info("posting the payment to loan %s %s %s %s", loan_id, amount, payment_date, code)
    relative_url = 'loans/{}/transactions'.format(loan_id)
    method = 'POST'
    params = {'command': 'repayment'}
    payload = {
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "receiptNumber": code,
        "transactionDate": payment_date,
        "transactionAmount": amount,
        "paymentTypeId": "218",
        "note": "loan repay payment",
        "accountNumber": "",
        "checkNumber": "",
        "routingCode": "",
        "bankNumber": ""
        }
    response  = fetch_data(method, relative_url, params, payload)
    return response


def get_earliest_unpaid_loan(phone_number):
    logging.info("getting the oldest active loan  for phone_number %s", phone_number)
    client_details = fetch_client_ids_for_phone(phone_number)
    if client_details:
        client_id = client_details[0]['entityId']
        logging.info("client ID from musoni for phone number %s", client_id)
        active_loans = get_all_active_loans(client_id)
        loans_order_by_id = sorted(active_loans, key=lambda loan: loan['id'])
        return loans_order_by_id[0] if loans_order_by_id else None

def get_all_active_loans(client_id):
    all_loans = fetch_all_loans_for_client(client_id)
    active_loans = [loan for loan in all_loans if loan['status']['active']]
    return active_loans

def get_customer_id_for_phone(phone_number):
    client_details = fetch_client_ids_for_phone(phone_number)
    return client_details[0]['entityId'] if client_details else None

def deposit_money(saving_account_id, payment_amount, date, code):
    relative_url = f'savingsaccounts/{saving_account_id}/transactions'
    params = {'command': 'deposit'}
    payload = {
            "dateFormat": "dd MMMM yyyy",
            "locale": "en",
            "transactionDate": date,
            "transactionAmount": payment_amount,
            "paymentTypeId": "177",
            "receiptNumber": code,
        }
    response  = fetch_data('POST', relative_url, params, payload)
    return response

def get_current_balance(saving_account_id):
    relative_url = f'savingsaccounts/{saving_account_id}/'
    response  = fetch_data('GET', relative_url, {}, {})
    return response['summary']['accountBalance']

def transfer_funds_from_saving_to_loan(office_id, client_id, saving_account_id, loan_id, amount, date):
    relative_url = f'accounttransfers'
    params = {}
    payload = {
        "fromOfficeId": office_id,
        "fromAccountType": "2", #saving account
        "fromAccountId": saving_account_id,
        "toOfficeId": office_id,
        "toAccountType": "1", #loan account
        "toAccountId": loan_id,
        "transferDate": date,
        "transferAmount": amount,
        "transferDescription": "Transfer to the loan account",
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "fromClientId": client_id,
        "toClientId": client_id
    }
    response  = fetch_data('POST', relative_url, params, payload)
    return response


def withdraw_money(saving_account_id, payment_amount, date, code):
    relative_url = f'savingsaccounts/{saving_account_id}/transactions'
    params = {'command': 'withdrawal'}
    payload = {
            "dateFormat": "dd MMMM yyyy",
            "locale": "en",
            "transactionDate": date,
            "transactionAmount": payment_amount,
            "paymentTypeId": "177",
            "receiptNumber": code,
        }
    response  = fetch_data('POST', relative_url, params, payload)
    return True, response['resourceId']

def deposit_money_to_saving_account(saving_account_id, payment_amount, date, code):
    try:
        deposit_money(saving_account_id, payment_amount, date, code)
        balance = get_current_balance(saving_account_id)
        return True, balance
    except:
        raise Exception(f'Error in depsiting money for {saving_account_id}')

def fetch_all_payments(date=datetime.today()):
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute('select pc.* from payment_confirmations pc join musoni_payment_update_tracker mp on pc.confirmation_code = mp.confirmation_code where status=false and reason=%s and till_number=%s and date(pc.created_at)=%s order by transaction_time', ('no saving accounts for the client', CREDIT_TILL_NUMBERS, date))
    logging.info("no of rows to process %s", cursor.rowcount)
    rows = cursor.fetchall()
    return rows

def update_tracker_table(confirmation_code, status, reason):
    logging.info("updating tracker table with following detail %s", (confirmation_code, status, reason))
    cursor = conn.cursor()
    cursor.execute('insert into musoni_payment_update_tracker values (%s, %s, %s, %s, %s, %s)', (uuid.uuid4(), confirmation_code, status, reason, datetime.now(), datetime.now()))
    conn.commit()

def is_payment_captured(confirmation_code):
    cursor = conn.cursor()
    cursor.execute('select * from musoni_payment_update_tracker where confirmation_code=%s and status=%s', (confirmation_code, 'true'))
    return True if cursor.rowcount else False

def get_credit_fee_allocation(confirmation_code):
    cursor = conn.cursor()
    cursor.execute('select sum(payment_amount) from credit_fee_payments where payment_refrence_code=%s and payment_mode=%s and status=%s', (confirmation_code, 'mpesa', 'true'))
    response = cursor.fetchall()[0][0]
    logging.info("credit fee allocation for the code %s %s", confirmation_code, response)
    return response if response else 0


def sort_loans_by_disburse_date(loans=[]):
    list_to_date = lambda d_list: date(d_list[0], d_list[1], d_list[2])
    return sorted(loans, key=lambda loan: list_to_date(loan['timeline']['actualDisbursementDate']))

def filter_loans_before_transaction_date(transaction_date, loans=[]):
    list_to_date = lambda d_list: date(d_list[0], d_list[1], d_list[2])
    transaction_date = datetime.strptime(transaction_date, COMMON_DATE_FORMAT_CONVERSION).date()
    print(type(transaction_date))
    return filter(lambda loan: list_to_date(loan['timeline']['actualDisbursementDate']) < transaction_date, loans) 

def repay_loans(client_id, saving_account_id, amount, date, office_id=None):
    all_loans = fetch_all_loans_for_client(client_id)
    active_loans = [loan for loan in all_loans if loan['status']['active']]
    loans_to_repay = filter_loans_before_transaction_date(date, sort_loans_by_disburse_date(active_loans))
    loans_amount_payed = []
    logging.info("open loans to be reapied %s", loans_to_repay)
    for index, loan in enumerate(loans_to_repay):
        loan_balance = loan['loanBalance']
        if loan_balance < amount:
            #transfer_funds_from_saving_to_loan(office_id, client_id, saving_account_id, loan['id'], loan_balance, date)
            status, transaction_id = withdraw_money(saving_account_id, loan_balance, date, f'LOAN_REPAY_{loan["id"]}')
            post_repayment_to_loan(loan['id'], loan_balance, date, f'SA_{saving_account_id}_{transaction_id}')
            amount = amount - loan_balance
            loans_amount_payed.append({'id': loan['id'], 'amount':loan_balance})
        elif loan_balance >= amount:
            transfer_funds_from_saving_to_loan(office_id, client_id, saving_account_id, loan['id'], amount, date)
            status, transaction_id = withdraw_money(saving_account_id, amount, date, f'LOAN_REPAY_{loan["id"]}')
            post_repayment_to_loan(loan['id'], amount, date, f'SA_{saving_account_id}_{transaction_id}')
            loans_amount_payed.append({'id': loan['id'], 'amount':amount})
            amount = 0
        if not amount:
            break
    return loans_amount_payed

def update_repayment_for_loan(phone_number, payment_amount, date, code):
    logging.info("posting payment for phone_no %s amount %s date %s code %s", phone_number, payment_amount, date, code)
    try:
       
        credit_fee_amount = get_credit_fee_allocation(code)
        logging.info("credit payment amount allocation %s for the reference code %s", credit_fee_amount, code)
        
        final_payment_amount = payment_amount - credit_fee_amount
        logging.info("computed final payment amount credit fee %s  payment %s final payment %s", credit_fee_amount, payment_amount, final_payment_amount)
        if final_payment_amount <= 0:
            return False, f"payment is allocated as credit fee {final_payment_amount}"
       
        clients = fetch_client_ids_for_phone(phone_number)
       
        if not clients:
            return False, "no client for the phone number"
        client = clients[0]
        logging.info("account chosen to deposit %s", client
        )
        saving_accounts = fetch_saving_account_for_client(client_id=client['entityId'])

        if not saving_accounts:
            return False, "no saving accounts for the client"

        account = saving_accounts[0]
        logging.info("account chosen to deposit %s", account)       
        logging.info("remove credit fee amount from payment amount")

        status, balance = deposit_money_to_saving_account(account['id'], final_payment_amount, date, code)
 
        logging.info('current balance in saving account for client %s', balance)
     
        if balance > 0:
            loans_paid = repay_loans(client['entityId'], account['id'], balance, date, office_id=client['officeId'])
            logging.info("loans paid %s", loans_paid)
            return True, f"loans paid: {str(loans_paid)}"
        else:
            return False, "saving account balance is 0"
 
    except Exception as e:
        logging.exception('error in updating the payment')
        return False, str(repr(e))

def process_payments_to_savings(phone_number, payment_amount, date, code):
    logging.info("posting payment for phone_no %s amount %s date %s code %s", phone_number, payment_amount, date, code)
    try:
        clients = fetch_client_ids_for_phone(phone_number)
        if not clients:
            return False, "no client for the phone number"
        client = clients[0]
        logging.info("account chosen to deposit %s", client
        )
        saving_accounts = fetch_saving_account_for_client(client_id=client['entityId'])

        if not saving_accounts:
            return False, "no saving accounts for the client"

        account = saving_accounts[0]
        logging.info("account chosen to deposit %s", account)       
        logging.info("remove credit fee amount from payment amount")

        credit_fee_amount = get_credit_fee_allocation(code)
        logging.info("credit payment amount allocation %s for the reference code %s", credit_fee_amount, code)
        final_payment_amount = payment_amount - credit_fee_amount

        logging.info("computed final payment amount credit fee %s  payment %s final payment %s", credit_fee_amount, payment_amount, final_payment_amount)
        if final_payment_amount <= 0:
            return False, f"payment is allocated as credit fee {final_payment_amount}"
 
        status, balance = deposit_money_to_saving_account(account['id'], final_payment_amount, date, code)
 
        logging.info('current balance in saving account for client %s', balance)

        return True, json.dumps({"client": client['entityId'], "saving": account['id']})
        
    except Exception as e:
        logging.exception('error in updating the payment')
        return False, str(repr(e))


def over_payment_handler(phone_number, date):
    logging.info("posting payment for phone_no %s date %s", phone_number, date)
    try:
        clients = fetch_client_ids_for_phone(phone_number)
        if not clients:
            return False, "no client for the phone number"
        client = clients[0]
        logging.info("account chosen to deposit %s", client
        )
        saving_accounts = fetch_saving_account_for_client(client_id=client['entityId'])

        if not saving_accounts:
            return False, "no saving accounts for the client"

        account = saving_accounts[0]
        logging.info("account chosen to deposit %s", account)
        print("-"*50, account['id'])
        balance = get_current_balance(account['id'])
        logging.info('current balance in saving account for client %s', balance)

        if balance > 0:
            loans_paid = repay_loans(client['entityId'], account['id'], balance, date)
            logging.info("loans paid %s", loans_paid)
            return True, f"loans paid: {str(loans_paid)}"
        else:
            return False, "saving account balance is 0"
    except Exception as e:
        logging.exception('error in updating the payment')
        return False, str(repr(e))

@click.group()
def commands():
    pass

@click.command()
def update_payment_for_loan():
    psycopg2.extras.register_uuid()
    phone_number = "0706314309"
    payment_amount = 10
    date = datetime(2022, 4, 1).date().strftime(COMMON_DATE_FORMAT_CONVERSION)
    code = "QD17M4LMMB"
    status, response = update_repayment_for_loan(phone_number, payment_amount, date, code)
    update_tracker_table(code, status, str(response))
    print(status, response)

@click.command()
def handle_over_payment():
    phone_number = "0719465291"
    date = datetime(2022, 4, 11).date().strftime(COMMON_DATE_FORMAT_CONVERSION)
    status, response = over_payment_handler(phone_number, date)
    print(status, response)

@click.command()
@click.option('--run_date')
def process_payments(run_date=datetime.today()):
    logging.info("execution started for the date %s", run_date)
    psycopg2.extras.register_uuid()

    payments_to_processed=fetch_all_payments(run_date)
    
    for record in payments_to_processed:
        logging.info("processing record %s", record)

        if not is_payment_captured(record['confirmation_code']):
            status, response = update_repayment_for_loan(record['sender_number'], record['transaction_amount'], record['transaction_time'].strftime(COMMON_DATE_FORMAT_CONVERSION), record['confirmation_code'])
            update_tracker_table(record['confirmation_code'], status, str(response))
            logging.info("post payment final results %s, %s", status, response)
        else:
            logging.info("confirmation code used already skipping %s", record['confirmation_code'])

    logging.info("execution ended")


commands.add_command(update_payment_for_loan)
commands.add_command(process_payments)
commands.add_command(handle_over_payment)

if __name__ == '__main__':
    commands()