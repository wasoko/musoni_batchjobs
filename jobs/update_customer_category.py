import array
from distutils.log import error
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import json
import datetime
import logging
import sys
import warnings
from config import *
from crm_connector import get_auth_token, get_customer_details, update_customer_details
from sqlalchemy import create_engine
import click
from rich.console import Console
from rich.table import Table

warnings.filterwarnings("ignore")  

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout),logging.FileHandler('AL_update_MusoniIms_v2.log')])

loandet_url = "loans"
# Fetch custom client details for all customers at once using data export api
addto_queue_url = "data-export/prepared/addToQueue" # returns report id
reportdet_url  = "data-export/prepared/{}/attachment/direct-link"
clientdet_url = "clients"
update_clientdet_url = "datatables/ml_client_details/{}"

date = datetime.datetime.now()

logging.info("------- %s --------",str(date))

auth_response  = get_auth_token()
token = auth_response['token_details']['access_token']

error_log = []


def api_call(method,relativeurl,params=None,data=None):
    url =  MUSONI_BASE_URL+relativeurl
    auth = HTTPBasicAuth(MUSONI_USERNAME , MUSONI_PASSWORD)
    response = requests.request(method,url,headers = MUSONI_HEADERS,auth = auth,params  = params ,data = data)
    logging.info("Received code %s as response to the api call %s",response.status_code,url)
    if response.status_code not in(200,201):
        if method == "PUT" and response.status_code == 404:
            
            response = requests.request("POST",url,headers = MUSONI_HEADERS,auth = auth,params  = params ,data = data)
            logging.info("Received code %s as response to the trial api call %s method(POST) ",response.status_code,url)

        else:                
            raise Exception('API call to the relative URL "{}" returned a status code of {} due to "{}"'.format(relativeurl,response.status_code,response))
    return response.json()



def update_customer_category_to_musoni(client_id):
    try:
        response = api_call("PUT", update_clientdet_url.format(str(client_id)),
        {"tenantIdentifier": "wasoko", "apptable": "m_client"},
        data=json.dumps({"Customer_Risk_Segment_cd_Customer_Risk_S7": CUSTOMER_CATEGORY_MAP['Category 1'],
        "Kenya_Loan_Product_Assignment_cd_Loan_Product_As9": LOAN_PRODUCT_ID_MAP['Kenya_14_day_0%'],
        "locale": "en_GB"}))
        logging.info("%s", str(response))
        logging.info("successfully updated in musoni for client_id {}".format(str(client_id)))

    except Exception as e:
        logging.exception("Error - %s while updating client data for client id - %s", str(e), str(client_id))


def update_customer_category_to_ims(customer_id):
    try:
        customer_details = get_customer_details(token, customer_id)
        # customer_details['customer_profile']['id'] = 'e37a8849-9185-4c00-a4f6-2d925cff20c9'
        customer_details['customer_profile']['id'] = CUSTOMER_PROFILE_ID_MAP['Kenya(14)-0%']
        customer_details['language_id'] = customer_details['language']['id']
        customer_details['organization_location_id'] = customer_details['organization_location']['id']
        response = update_customer_details(token, customer_id, customer_details)
        logging.info("updated the loan category for the customer response %s", response)
        logging.info("successfully updated loan category in IMS for customer %s", customer_id)
    except:
        logging.exception("error in updating the loan category for the customer %s", customer_id)



if __name__ == '__main__':
    client_id = click.prompt('Please enter credit id', type=int)
    customer_id = click.prompt('Please enter customer id')
    update_customer_category_to_musoni(client_id)
    update_customer_category_to_ims(customer_id)

