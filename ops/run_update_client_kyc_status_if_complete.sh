#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  python jobs/client_manager.py update-client-kyc-status-if-completed
popd