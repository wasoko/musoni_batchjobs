import logging
import sys

import click
import psycopg2.extras
from config import *
import vas_utils

logging.basicConfig(
    encoding='utf-8',
    level=logging.DEBUG,
    handlers=[logging.StreamHandler(sys.stdout)])

conn = psycopg2.connect(
    database=DB_CONNECTION_DB,
    user=DB_CONNECTION_USERNAME,
    password=DB_CONNECTION_PASSWORD,
    host=DB_CONNECTION_HOST,
    port=DB_CONNECTION_PORT
)


@click.group()
def commands():
    pass


@click.command()
def fetch_agent_balances():
    vas_agents = []
    try:
        table_fetch_cursor = conn.cursor()
        table_fetch_cursor.execute("SELECT * FROM vas_agents")
        vas_agents = table_fetch_cursor.fetchall()

    except:
        logging.exception("error in fetching vas_agents")

    if(len(vas_agents)):
        for agent in vas_agents:
            vas_utils.get_agent_balance(agent[1])



commands.add_command(fetch_agent_balances, 'fetch_balances')

if __name__ == '__main__':
    commands()
