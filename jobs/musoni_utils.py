import requests
from requests.models import Response
from requests.adapters import HTTPAdapter, Retry
import logging
import sys
import io
import time
import zipfile
from requests.auth import HTTPBasicAuth
import pandas as pd
from config import *

logging.basicConfig(
    encoding='utf-8',
    level=logging.DEBUG,
    handlers=[logging.StreamHandler(sys.stdout)])

def call_api(method, relative_url, params={}, payload={}):
    logging.info("calling musoni API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth, params = params, json=payload)
    logging.info("response received %s", response.json())
    if response.status_code not in (200, 201):
        if response.status_code == 404:
            raise Exception(f'{str(response.status_code)}:requested resource is not found')
        raise Exception(f"{str(response.status_code)}:{str(response.json()['errors'][0]['developerMessage'])}")
    return response.json()

def call_ims_notifier_api(method, relative_url, params={}, payload={}):
    logging.info("calling IMS Notifier API endpoint(%s:%s) with params and payload %s %s ",method, relative_url, params, payload)
    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('https://', adapter)
    response = session.post(relative_url, json=payload)
    logging.info("response received %s", response.json())
    if response.status_code not in (200, 201):
        raise Exception(str(response.status_code) + ':' + str(response.json()['errors'][0]['developerMessage']))
    return response.json()

def get_offices():
    url = 'offices/'
    return call_api('GET', url)

def get_staff():
    url = 'staff/'
    return call_api('GET', url)

def get_all_accounts_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?'.format(client_id)
    response  = call_api('GET', relative_url)
    return response

def get_all_active_clients():
    logging.info("fetching all clients")
    relative_url = 'clients'
    client_list  = call_api('GET', relative_url).get('pageItems',[])
    active_clients = filter(lambda x: x.get('active') == True, client_list)
    return active_clients

def get_all_loans_for_client(client_id):
    accounts = get_all_accounts_for_client(client_id)
    return accounts.get('loanAccounts', [])

def get_all_saving_accounts_for_client(client_id):
    logging.info("fetching loans for the client id %s", client_id)
    relative_url = 'clients/{}/accounts?fields=savingsAccounts'.format(client_id)
    response = call_api('GET', relative_url)
    return response.get('savingsAccounts', [])

def get_client_for_external_id(external_id):
    relative_url = 'search'
    params = {'query': external_id, 'resource': 'clients'}
    clients = call_api('GET', relative_url, params=params)
    return clients[0] if clients else None

def get_client_business_details_for_client_id(client_id):
    relative_url = f'datatables/ml_client_business/{client_id}/'
    response = call_api('GET', relative_url)
    return response if response else None

def download_report_from_data_export(data_export_id):
    logging.info("fetching all client details")
    report_id  = call_api("GET",'data-export/prepared/addToQueue',{"tenantIdentifier":MUSONI_TENANT_ID,"exportId":data_export_id,"fileFormat":"csv"})
    logging.info("report queued")
    time.sleep(5)
    if report_id:
        response = call_api("GET",f'data-export/prepared/{report_id}/attachment/direct-link')
        if response:
            if '.zip' in response['value']:
                r = requests.get(response['value'])
                zf = zipfile.ZipFile(io.BytesIO(r.content))
                file = zf.namelist()[0]
                data = pd.read_csv(zf.open(file),delimiter = ';')
            else:
                data = pd.read_csv(response['value'],delimiter=';')
    return data.to_dict("records")

def transfer_fund(client_id, office_id, from_id, from_account_type, to_id, to_account_type, amount, date, note):
    url = "accounttransfers"
    payload = {
        "fromOfficeId": office_id,
        "fromClientId": client_id,
        "fromAccountType": from_account_type,
        "fromAccountId": from_id,
        "toOfficeId": office_id,
        "toClientId": client_id,
        "toAccountType": to_account_type,
        "toAccountId": to_id,
        "dateFormat": "dd-MM-yyyy",
        "locale": "en_GB",
        "transferDate": date,
        "transferAmount": amount,
        "transferDescription": note
    }
    return call_api('POST', url, params=None, payload=payload)

def approve_transfer(transfer_id):
    url=f'makercheckers/{transfer_id}'
    params = {
        'command': 'approve'
    }
    payload = {}
    return call_api('POST', url, params=params, payload=payload)


def transfer_funds_from_savings_to_loan(client_id, office_id, saving_id, loan_id, amount, date):
    transfer_response = transfer_fund(
        client_id=client_id,
        office_id=office_id,
        from_id=saving_id,
        from_account_type=2,
        to_id=loan_id,
        to_account_type=1,
        amount=amount,
        date=date,
        note="loan repayment")
    response = approve_transfer(transfer_response['commandId'])
    return True

def create_client(payload):
    url = 'clients/'
    return call_api('POST', url, payload=payload)

def process_batch(payload):
    url = 'batches/'
    return call_api('POST', url, payload=payload)

def reactivate_client(client_id, date):
    url  = f'clients/{client_id}'
    payload = {
        "dateFormat":"dd MMMM yyyy",
        "locale":"en",
        "reactivationDate":date,
    }
    params = {
        'command': 'reactivate'
    }
    return call_api('POST', url, params, payload)

def activate_client(client_id, date):
    url  = f'clients/{client_id}'
    payload = {
        "dateFormat":"dd MMMM yyyy",
        "locale":"en",
        "activationDate":date,
    }
    params = {
        'command': 'activate'
    }
    return call_api('POST', url, params, payload)

def close_client(client_id, date):
    url = f'clients/{client_id}'
    payload = {
        "dateFormat":"dd-MM-yyyy",
        "locale":"en_GB",
        "closureDate":date,
        "closureReasonId":422
    }
    params = {
        'command': 'close'
    }
    return call_api("POST", url, params, payload)

def close_saving_account(account_id, date):
    url = f"savingsaccounts/{account_id}"
    payload = {
        "dateFormat":"dd-MM-yyyy",
        "locale":"en_GB",
        "closedOnDate":date,
        "note":"closing saving account"
    }
    params = {
        'command': 'close'
    }
    return call_api("POST", url, params, payload)


def batch_processing(requests):
    url = 'f'

def send_sms(phone_number, message):
    logging.info("Sending SMS for phone number %s", phone_number)
    if SANDBOX_SMS:
        payload = {
            "body": message,
            "channel": "SMS",
            "recipient": phone_number
        }
    else:
        payload = {
            "body": message,
            "channel": "SMS",
            "from": "Wasoko",
            "recipient": phone_number
        }
    response = call_ims_notifier_api("POST", NOTIFIER_URL, {}, payload)
    logging.info(response)
    return response

def update_client_credit_details(client_id, data):

    field_mapping = CLIENT_PROFILE_FIELD_MAPPING
    
    payload = {}
    for key in data:
        if key in field_mapping:
            payload.update({field_mapping[key]:data[key]})

    payload.update ({
        "locale":"en_GB",
        "dateFormat":data.get('date_format', "dd-MM-yyyy"),
        })
    relative_url=f'datatables/ml_client_details/{client_id}/'
    params={'apptable':'m_client'}
    method='PUT'
    response = call_api(method, relative_url, params=params, payload=payload)
    return response

def update_client_by_id(client_id, data):
    payload = data
    relative_url = f'clients/{client_id}'
    method = 'PUT'
    response = call_api(method, relative_url, payload=payload)
    return response


def update_client_business_details(client_id, data):
    payload = data
    payload.update({
        "locale": "en_GB",
    })
    relative_url = f'datatables/ml_client_business/{client_id}/'
    method = 'PUT'
    response = call_api(method, relative_url, payload=payload)
    return response


def update_customer_product(client_id, new_product_id):
    payload = {
        "locale":"en_GB",
        "dateFormat":"dd-MM-yyyy",
        "Kenya_Loan_Product_Assignment_cd_Loan_Product_As9": new_product_id
        }
    relative_url=f'datatables/ml_client_details/{client_id}/'
    params={'apptable':'m_client'}
    method='PUT'
    response = call_api(method, relative_url, params=params, payload=payload)


def call_data_filter(payload, offset=0, limit=1000):
    relative_url = 'datafilters/loans/queries/run-filter'
    params = {'offset': offset, 'limit': limit}
    method = 'POST'
    response = call_api(method, relative_url, params=params, payload=payload)
    return response

def apply_charges(loan_id, payload):
    relative_url = f'loans/{loan_id}/charges'
    method = 'POST'
    payload = {
        "chargeId": payload['charge_id'],
        "locale": "en",
        "amount": payload['amount_or_percentage'],
        "dateFormat": "yyyy-MM-dd",
        "dueDate": str(payload['due_date'])
    }
    response = call_api(method, relative_url, payload=payload)
    return response


def create_user(payload):
    url = 'users/'
    return call_api('POST', url, payload=payload)


def delete_user(user_id):
    method = 'DELETE'
    relative_url =f'users/{user_id}'
    headers = MUSONI_HEADERS
    url = MUSONI_BASE_URL + relative_url
    auth = HTTPBasicAuth(MUSONI_USERNAME, MUSONI_PASSWORD)
    response = requests.request(method, url, headers=headers, auth=auth)
    if response.status_code not in (200, 201):
        if response.status_code == 404:
            raise Exception(f'{str(response.status_code)}:requested resource is not found')
        raise Exception(f"{str(response.status_code)}:{str(response.json()['errors'][0]['developerMessage'])}")
    return response.json()


def create_staff(payload):
    url = 'staff/'
    return call_api('POST', url, payload=payload)


def propose_and_accept_client_transfer(client_id, destination_office_id, destination_office_name):
    url = f'clients/{client_id}?command=proposeAndAcceptTransfer'
    payload_value = {
        'destinationOfficeId': f"{destination_office_id}",
        'note': f"Client Relocating to {destination_office_name}"
    }
    return call_api('POST', url, payload=payload_value)


def update_staff_by_id(staff_id, payload):
    relative_url = f'staff/{staff_id}'
    response = call_api('PUT', relative_url, payload=payload)
    return response


def update_user_by_id(user_id, payload):
    relative_url = f'users/{user_id}'
    response = call_api('PUT', relative_url, payload=payload)
    return response


def send_report(webhook_url: str, text: str) -> Response:
    return requests.post(webhook_url, json={'text': text})

def send_report_card(webhook_url: str, title: str, subtitle: str, paragraph: str) -> Response:
    header = {
        'title': title,
        'subtitle': subtitle,
    }
    widget = {'textParagraph': {'text': paragraph}}
    return requests.post(
        webhook_url,
        json={
            'cards': [
                {
                    'header': header,
                    'sections': [{'widgets': [widget]}],
                }
            ]
        },
    )






