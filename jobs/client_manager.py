import sys
import logging
import csv
import click
import random
from datetime import datetime, timedelta
import pandas as pd
import musoni_utils
from config import *
import psycopg2
import psycopg2.extras
from sqlalchemy import create_engine
import json
from dateutil.parser import parse
import phonenumbers
from multiprocessing import Pool
import concurrent.futures
import math
from multiprocessing import Pool
import concurrent.futures

logging.basicConfig(
    encoding='utf-8',
    level=logging.DEBUG,
    handlers=[logging.StreamHandler(sys.stdout)])


def create_client(client_data, office_id, staff_id):
    activate_date_str = parse(client_data['Activation Date'], dayfirst=True).strftime("%d %B %Y")
    payload = {
        "officeId": office_id,
        "active": True,
        "externalId": client_data['External Id'],
        "dateFormat": "dd MMMM yyyy",
        "locale": "en",
        "activationDate": activate_date_str,
        "submittedOnDate": activate_date_str,
        "firstname": client_data['First Name'],
        "middlename": "",
        "lastname": ".",
        "staffId": staff_id,
        "mobileNo": client_data['Mobile Number']
    }
    return musoni_utils.create_client(payload)

def propose_and_accept_client_transfers(reader, office_id, office_name):
    output_record = []
    for record in reader:
        logging.info("record ID to process %s", record['client_id'])
        logging.info("processing record %s", record)
        try:
            client_id = record['client_id']
            response = musoni_utils.propose_and_accept_client_transfer(client_id, office_id, office_name)

            record['status'] = 'success'
            record['resourceId'] = response['resourceId']
        except:
            record['resourceId'] = ""
            record['status'] = 'fail'
            logging.exception("error in processing the record %s", record)
        output_record.append(record)
    return output_record


def get_clients_without_open_loans(office_ids):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))
    query = '''
        select distinct  mc.id as client_id,
            mc.external_id as external_client_id,
            mc.display_name as store_name,
            mc.status_enum,
            mc.office_id,
            m_office.name as branch,
            ml.total_outstanding_derived,
            ml.loan_status_id
    from m_client mc join m_loan ml on mc.id = ml.client_id
    left join m_office m_office on m_office.id = mc.office_id
    where not ml.total_outstanding_derived > 0 and mc.office_id in {};
        '''
    return pd.read_sql_query(query.format(f"({','.join(str(x) for x in office_ids)})"), engine).to_dict('records')


def close_client(client_id, date):
    return musoni_utils.close_client(client_id, date)


def reactivate_client(client_id, date):
    return musoni_utils.reactivate_client(client_id, date)


def activate_client(client_id, date):
    return musoni_utils.activate_client(client_id, date)


def get_office_id_by_name(offices, office_name):
    for office in offices:
        if office['name'].lower() == office_name.lower():
            return office['id']
    raise Exception(f"No office found for then given office name {office_name}")


def get_random_staff(staff_list, office_id):
    filtered_staffs = [staff for staff in staff_list if staff['officeId'] == office_id]
    try:
        return random.choice(filtered_staffs)['id']
    except:
        pass
    return ''


def get_staff_id_by_name(staff_list, staff_name):
    first_name, last_name = staff_name.lower().split(' ')
    for staff in staff_list:
        if staff['firstname'].lower() == first_name and staff['lastname'].lower() == last_name:
            return staff['id']
    raise Exception(f"No staff found for then given staff name ${staff_name}")


def get_staff_id_by_country(country_name):
    return DEFAULT_STAFF_FOR_COUNTRY[country_name.lower()]


def get_client_business_details(business_details, ext_id):
    for biz_details in business_details:
        if ext_id == biz_details['external_id']:
            return biz_details
    raise Exception(f"No business details found for then given customer id ${ext_id}")


def get_formatted_mobile_number_by_country(mobile_number, country):
    logging.info("adding the extension number to the phone number %s %s", mobile_number, country)

    if country.lower() in MOBILE_PHON_EXTENSIONS:
        extension_number = MOBILE_PHON_EXTENSIONS[country.lower()]
        logging.info("extension number chosen %s", extension_number)
        if not mobile_number.startswith(extension_number):
            mobile_number = extension_number + mobile_number[1:] if mobile_number.startswith('0') else mobile_number

    logging.info("validating phone number be for proceeding %s", mobile_number)
    try:
        phone_number = phonenumbers.parse(mobile_number, COUNTRY_CODES[country.lower()])
        logging.info("phone and details %s", phone_number)
        if phonenumbers.is_valid_number(phone_number):
            logging.info("phone is valid returning the phone number %s", phone_number)
            return mobile_number
        else:
            logging.info("invalid phone number returning empty string")
            return ''
    except:
        logging.exception('error in validating the mobile number')
        return ''


def check_and_compare_if_kenya_phone_number(formatted_number, country, musoni_number):
    if country.lower() == "kenya":
        if "".join(["0", formatted_number[4:]]) != musoni_number:
            updated_number = "".join(["0", formatted_number[4:]])
            return updated_number
        else:
            return None
    if formatted_number != musoni_number:
        return formatted_number
    else:
        return None



def process_batch(reader, close_date, num_threads):
    output_record = []
    client_saving_accounts = {}
    client_id_batch = 21

    # Divide client IDs into smaller batches
    client_ids = list(set(record['client_id'] for record in reader))
    batch_size = math.ceil(len(client_ids) / client_id_batch)
    client_id_batches = [client_ids[i:i+batch_size] for i in range(0, len(client_ids), batch_size)]

    # Fetch saving accounts for clients asynchronously in batches
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
        futures = []
        for batch in client_id_batches:
            futures.append(executor.submit(fetch_saving_accounts_batch, batch))

        # Wait for all batch requests to complete
        for future in concurrent.futures.as_completed(futures):
            result = future.result()
            client_saving_accounts.update(result)

    # Process each record
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
        futures = []
        for record in reader:
            if record['client_id'] in CLIENTS_SKIP_FOR_CLOSE or record['result'] != CLOSE_CLIENT_FILTER_TEXT:
                logging.info("skipping record %s", record['client_id'])
                output_record.append(record)
                continue

            client_id = record['client_id']
            saving_accounts = client_saving_accounts[client_id]
            future = executor.submit(process_record, client_id, saving_accounts, close_date)
            futures.append((future, record))

        for future, record in futures:
            try:
                status = future.result()
                record['status'] = status
            except Exception as e:
                record['status'] = 'fail'
                logging.exception("error in processing the record %s", record)
            output_record.append(record)

    return output_record


def fetch_saving_accounts_batch(client_ids):
    result = {}
    for client_id in client_ids:
        saving_accounts = musoni_utils.get_all_saving_accounts_for_client(client_id)
        result[client_id] = saving_accounts
    return result


def process_record(client_id, saving_accounts, close_date):
    logging.info("saving accounts:: %s", saving_accounts)
    for saving_account in saving_accounts:
        musoni_utils.close_saving_account(saving_account['id'], close_date)
    close_client(client_id, close_date)
    return 'success'

@click.group()
def commands():
    pass

@click.command()
def update_kyc_status():
    postgres_conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )

    mysql_engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))
    postgres_cursor = postgres_conn.cursor()

    try:
        # Get clients from PostgreSQL query
        postgres_query = """
            SELECT id, customer_id, approved_credit_limit, approved_loan_product, consent_status, kyc_grace_period, kyc_status,
                consented_at, created_at, updated_at, risk_segment, musoni_client_id
            FROM preapproved_credit_customers
            WHERE kyc_status = FALSE;
        """
        postgres_cursor.execute(postgres_query)
        postgres_clients = postgres_cursor.fetchall()


        # Extract musoni_client_ids from postgres_clients
        musoni_client_ids = [str(client[-1]) for client in postgres_clients]



        if musoni_client_ids:
            # Get clients from MySQL query using musoni_client_ids
            mysql_query = '''
            SELECT
                client_id,
                Name_as_per_ID2,
                ID_Number3,
                Main_Mobile_Pho4,
                Alternative_Pho5,
                Do_you_use_Whatsapp_cd_Do_you_use_Whatsapp6,
                Do_you_use_Whatsapp_cd_Is_your_WhatsAp7,
                Whatsapp_Number8,
                Date_of_Birth9,
                Gender_cd_Gender10,
                Next_of_Kin_Name13,
                Next_of_Kin_Rel14,
                Next_of_Kin_Mob15,
                Shop_Name17,
                Name_of_Registe18,
                Name_of_Owner_a19,
                Date_of_Registration20,
                Expiry_Date21,
                Landmark22,
                Credit_Terms_Explained_cd_Credit_Terms_Ex25,
                image_ID_Card_Photo_27,
                image_ID_Card_Photo_Back28,
                image_Business_Regist29,
                image_Photo_of_the_sh30,
                image_Photo_of_the_sh31,
                image_Photo_of_the_sh32
            FROM
                cct_KYCKnowYourCustomer2023
            WHERE
                client_id IN (%s) 
                AND COALESCE(Name_as_per_ID2, '') != ''
                AND COALESCE(ID_Number3, '') != ''
                AND COALESCE(Main_Mobile_Pho4, '') != ''
                AND COALESCE(Alternative_Pho5, '') != ''
                AND COALESCE(Do_you_use_Whatsapp_cd_Do_you_use_Whatsapp6, '') != ''
                AND COALESCE(Do_you_use_Whatsapp_cd_Is_your_WhatsAp7, '') != ''
                AND COALESCE(Whatsapp_Number8, '') != ''
                AND COALESCE(Date_of_Birth9, '') != ''
                AND COALESCE(Gender_cd_Gender10, '') != ''
                AND COALESCE(Next_of_Kin_Name13, '') != ''
                AND COALESCE(Next_of_Kin_Rel14, '') != ''
                AND COALESCE(Next_of_Kin_Mob15, '') != ''
                AND COALESCE(Shop_Name17, '') != ''
                AND COALESCE(Name_of_Registe18, '') != ''
                AND COALESCE(Name_of_Owner_a19, '') != ''
                AND COALESCE(Date_of_Registration20, '') != ''
                AND COALESCE(Expiry_Date21, '') != ''
                AND COALESCE(Landmark22, '') != ''
                AND COALESCE(Credit_Terms_Explained_cd_Credit_Terms_Ex25, '') != ''
                AND COALESCE(image_ID_Card_Photo_27, '') != ''
                AND COALESCE(image_ID_Card_Photo_Back28, '') != ''
                AND COALESCE(image_Business_Regist29, '') != ''
                AND COALESCE(image_Photo_of_the_sh30, '') != ''
                AND COALESCE(image_Photo_of_the_sh31, '') != ''
                AND COALESCE(image_Photo_of_the_sh32, '') != '';
                '''

            # Execute the MySQL query
            mysql_clients = mysql_engine.execute(mysql_query % ','.join(['%s'] * len(musoni_client_ids)),
                                                 musoni_client_ids).fetchall()

            if mysql_clients:
                # Update kyc_status in PostgreSQL for matching clients
                for mysql_client in mysql_clients:
                    musoni_client_id = mysql_client[0]  # Assuming musoni_client_id is in the first column
                    logging.info("musoni_client_id:: %s", musoni_client_id)

                    try:
                        # Update kyc_status to true in PostgreSQL
                        update_query = '''UPDATE preapproved_credit_customers
                        SET kyc_status = TRUE, updated_at = NOW()
                        WHERE musoni_client_id = %s;'''
                        postgres_cursor.execute(update_query, (musoni_client_id,))
                        postgres_conn.commit()
                    except Exception as e:
                        # Handle any update errors
                        logging.exception(f"Error updating kyc_status for musoni_client_id {musoni_client_id}: {str(e)}")
            else:
                logging.info("No musoni_client_ids found in MySQL response.")
        else:
            logging.info("No musoni_client_ids found in PostgreSQL response.")

    except Exception as e:
        # Handle any query or connection errors
        logging.exception(f"Error occurred: {str(e)}")

    # Close the connections
    postgres_cursor.close()
    postgres_conn.close()


@click.command()
@click.option('--target-date', default=(datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d"))
def assign_credit_details_to_ims_client(target_date):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(
        user=MUSONI_DB_CONNECTION_USERNAME,
        pwd=MUSONI_DB_CONNECTION_PASSWORD,
        host=MUSONI_DB_CONNECTION_HOST,
        port=MUSONI_DB_CONNECTION_PORT,
        db=MUSONI_DB_CONNECTION_DB))
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )

    created_clients_query = '''
        SELECT client_id, ml_client_details.submittedon_date, 
        Kenya_Loan_Product_Assignment_cd_Loan_Product_As9 as loan_product_id, Credit_Limit10 as credit_limit,
        Available_Credi11, Delinquency12, external_id
        FROM ml_client_details
        LEFT JOIN m_client
        ON ml_client_details.client_id = m_client.id
        WHERE ml_client_details.submittedon_date = '{}'
    '''
    update_client_cursor = conn.cursor()
    update_client_query = '''
    UPDATE customers SET customer_profile_id = %s , credit_limit = %s
    WHERE id = %s and customer_profile_id is null;
    '''
    reader = pd.read_sql_query(created_clients_query.format(target_date), engine).to_dict('records')
    logging.info("processing records for date %s", target_date)
    try:
        for record in reader:
            logging.info("processing record with id %s", record['client_id'])
            try:
                update_client_cursor.execute(update_client_query, (LOAN_TO_PRODUCT_ID_MAP[record['loan_product_id']],
                                                                   record['credit_limit'], record['external_id']))
            except:
                logging.exception("error in processing record with id %s", record['client_id'])
                conn.rollback()
        conn.commit()
    except:
        logging.exception("error in processing records")
    finally:
        conn.close()


@click.command()
@click.option('--close-date', default=datetime.today().strftime("%d-%m-%Y"))
@click.option('--days-since-last-loan', default=90)
@click.option('--batch-size', default=99)
@click.option('--num-threads', default=4)
def close_clients_without_approved_limit(close_date, days_since_last_loan, batch_size, num_threads):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))

    query = '''
        select * from (select 
        m_client.id as client_id,
        m_client.external_id as external_client_id,
        m_client.display_name as store_name,
        m_office.name as branch,
        m_code_value.code_value as country,
        loan_det.last_loan_date,
        ml_client_details.Credit_Limit10 AS credit_limit,
        ml_client_details.Available_Credi11 as available_limit,
        r_enum_value.enum_value as client_status,
        CASE WHEN loan_det.last_loan_date < current_date - INTERVAL {} DAY and ob =0 then 'Close Client'
        WHEN loan_det.last_loan_date >= current_date - INTERVAL {} DAY or ob > 0 then 'Active'
        else 'Close Client' end as result
        from m_client
        left join ml_client_details on client_id = m_client.id
        left join (Select * from r_enum_value where enum_name = 'status_enum') r_enum_value on m_client.status_enum = r_enum_value.enum_id
        left join m_office on m_office.id = m_client.office_id
        left join m_code_value on m_code_value.id = ml_client_details.COUNTRY_cd_Country8
        left join (select client_id,max(disbursedon_date) as last_loan_date,sum(CASE WHEN total_outstanding_derived <0 then 0 else total_outstanding_derived end) as ob from m_loan group by client_id)loan_det on loan_det.client_id = m_client.id
        where r_enum_value.enum_value = 'Active') as temp where result='Close Client';
    '''

    # Read data in batches
    reader = pd.read_sql_query(query.format(days_since_last_loan, days_since_last_loan), engine, chunksize=batch_size)

    # Process each batch
    output_record = []
    fail_count = 0
    success_count = 0
    try:
        for batch in reader:
            batch_output = process_batch(batch.to_dict('records'), close_date, num_threads)
            output_record.extend(batch_output)
    except:
        logging.exception("error in processing file")

    # Log output records
    logging.debug("output records to be shared %s", output_record)

    if output_record:
        writer = csv.DictWriter(open("closed_clients_without_approved_limit.csv", "w"),
                                fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)
        for client in output_record:
            if client['status'] == 'fail':
                fail_count += 1
            else:
                success_count += 1
        report_close_client_results(fail_count, success_count)


def report_close_client_results(failed_close, successful_close):
    logging.info("Sending close client results")
    title = "Job Name: Close Clients Without Loans "
    subtitle = "Description: Cron Job Summary"
    paragraph = f"Number of clients successfully closed: {successful_close}" \
                f"\nNumber of clients failed to close: {failed_close}"
    musoni_utils.send_report_card(WEBHOOK_URL, title, subtitle, paragraph)



@click.command()
def migrate_clients_from_nai_east_to_thika():

    reader = get_clients_without_open_loans([BRANCH_OFFICE_IDS_MAP[1]['office_id']])
    try:
        output_record = propose_and_accept_client_transfers(reader, BRANCH_OFFICE_IDS_MAP[2]['office_id'], BRANCH_OFFICE_IDS_MAP[2]['name'])
    except:
        logging.exception("error in processing records")

    logging.debug("output records to be shared %s", output_record)
    if output_record:
        writer = csv.DictWriter(open("clients_migrated_from_nai_east_to_thika.csv", "w"),
                                fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


@click.command()
def migrate_clients_from_ruyenzi_to_kigali():
    reader = get_clients_without_open_loans([BRANCH_OFFICE_IDS_MAP[3]['office_id']])
    try:
        output_record = propose_and_accept_client_transfers(reader, BRANCH_OFFICE_IDS_MAP[4]['office_id'],
                                                            BRANCH_OFFICE_IDS_MAP[4]['name'])
    except:
        logging.exception("error in processing records")

    logging.debug("output records to be shared %s", output_record)
    if output_record:
        writer = csv.DictWriter(open("clients_migrated_from_ruyenzi_to_kigali.csv", "w"),
                                fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


@click.command()
def migrate_clients_from_dar_east_and_west_to_central():
    reader = get_clients_without_open_loans([ BRANCH_OFFICE_IDS_MAP[5]['office_id'], BRANCH_OFFICE_IDS_MAP[6]['office_id']])
    try:
        output_record = propose_and_accept_client_transfers(reader, BRANCH_OFFICE_IDS_MAP[7]['office_id'],
                                                            BRANCH_OFFICE_IDS_MAP[7]['name'])
    except:
        logging.exception("error in processing records")

    logging.debug("output records to be shared %s", output_record)
    if output_record:
        writer = csv.DictWriter(open("clients_migrated_from_dar_east_and_west_to_central.csv", "w"),
                                fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)

@click.command()
@click.argument('file-path')
@click.option('--close-date', default=datetime.today().strftime("%d-%m-%Y"))
def close_clients(file_path, close_date):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    client_id = record['id']
                    saving_accounts = musoni_utils.get_all_saving_accounts_for_client(client_id)
                    for saving_account in saving_accounts:
                        musoni_utils.close_saving_account(saving_account['id'], close_date)
                    close_client(client_id, close_date)
                    record['status'] = 'success'
                except:
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
        writer = csv.DictWriter(open("client_close_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)
        
@click.command()
@click.argument('file-path')
def activate_clients(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    client_id = str(record['Client Id']).replace(',','')
                    date = record['date']
                    # reactivate_client(client_id, date)
                    activate_client(client_id, date)
                    record['status'] = 'success'
                except:
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
        writer = csv.DictWriter(open("client_close_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


def file_client_creation_payload(offices, client_list, counter):
    payload_list = []
    requestId = counter

    for record in client_list:
        activate_date_str = parse(record['Activation Date'], dayfirst=True).strftime("%d %B %Y")
        office_id = get_office_id_by_name(offices, record['Office Name'])
        staff_id = get_staff_id_by_country(record['Country'])
        requestId = requestId + 1
        payload = {}

        payload["requestId"] = requestId
        payload["relativeUrl"] = "clients"
        payload["method"] = "POST"
        payload["headers"] = [MUSONI_HEADERS]

        payload_body = {}

        payload_body["officeId"] = office_id
        payload_body["active"] = True
        payload_body["externalId"] = record['External Id']
        payload_body["dateFormat"] = "dd MMMM yyyy"
        payload_body["locale"] = "en"
        payload_body["activationDate"] = activate_date_str
        payload_body["submittedOnDate"] = activate_date_str
        payload_body["firstname"] = record['First Name']
        payload_body["middlename"] = ""
        payload_body["lastname"] = "."
        payload_body["staffId"] = staff_id
        payload_body['mobileNo'] = get_formatted_mobile_number_by_country(record['Mobile Number'], record['Country'])

        payload["body"] = json.dumps(payload_body)

        payload_list.append(payload)

    return payload_list


@click.command()
@click.argument('file-path')
def create_and_activate_client(file_path):
    output_record = []
    offices = musoni_utils.get_offices()
    staff = musoni_utils.get_staff()
    try:
        with open(file_path) as fp:
            reader = list(csv.DictReader(fp))
            for i in range(0, len(reader), MUSONI_CLIENT_CREATION_BATCH_SIZE):
                batch_reader = reader[i:i + MUSONI_CLIENT_CREATION_BATCH_SIZE]
                payload = file_client_creation_payload(offices, batch_reader, i)
                logging.info("payload to be posted %s", payload)
                response = []
                try:
                    response = musoni_utils.process_batch(payload)
                    logging.info("response : %s", response)
                except Exception as e:
                    logging.exception("error in processing the records %s", batch_reader)
                output_record = output_record + response
    except:
        logging.exception("error in processing file")

    if output_record:
        writer = csv.DictWriter(open("create_customer_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


def get_client_and_business_creation_payload(offices, client_list, counter, business_details):
    payload_list = []
    requestId = counter
    for record in client_list:
        if(record['country_name'].lower() == 'Zanzibar'.lower()):
            record['country_name'] = 'tanzania'


        requestId = requestId + 1
        client_payload = {}
        client_payload["requestId"] = requestId
        client_payload["relativeUrl"] = "clients"
        client_payload["method"] = "POST"
        client_payload["headers"] = [MUSONI_HEADERS]

        office_id = get_office_id_by_name(offices, record['office_name'])
        staff_id = get_staff_id_by_country(record['country_name'])
        client_business_detail = get_client_business_details(business_details, record['external_id'])

        if(client_business_detail['country'].lower() == 'Zanzibar'.lower()):
            client_business_detail['country'] = 'Tanzania'

        client_payload_body = {}

        client_payload_body["officeId"] = office_id
        client_payload_body["active"] = True
        client_payload_body["externalId"] = record['external_id']
        client_payload_body["dateFormat"] = "yyyy-mm-dd"
        client_payload_body["locale"] = "en"
        client_payload_body["activationDate"] = record['activation_date'].strftime("%Y-%m-%d")
        client_payload_body["submittedOnDate"] = record['activation_date'].strftime("%Y-%m-%d")
        client_payload_body["firstname"] = record['first_name']
        client_payload_body["middlename"] = ""
        client_payload_body["lastname"] = "."
        client_payload_body["staffId"] = staff_id
        client_payload_body["mobileNo"] = get_formatted_mobile_number_by_country(record['mobile_number'],
                                                                                 record['country_name'])

        client_payload["body"] = json.dumps(client_payload_body)

        business_payload = {}
        business_payload["requestId"] = requestId + 1
        business_payload["relativeUrl"] = "datatables/ml_client_business/$.clientId"
        business_payload["method"] = "POST"
        business_payload["headers"] = [MUSONI_HEADERS]
        business_payload["reference"] = requestId

        business_payload_body = {}

        business_payload_body["client_id"] = "$.clientId"
        business_payload_body["businessName"] = client_business_detail['store_name']
        business_payload_body["BusinessType_cv_businessType"] = client_business_detail['business_type']
        business_payload_body["businessStartDate"] = client_business_detail['business_start_date'].strftime("%Y-%m-%d")
        business_payload_body["locale"] = "en"
        business_payload_body["city"] = client_business_detail['city']
        business_payload_body["country"] = client_business_detail['store_manager']
        business_payload_body["COUNTRY_cd_Country13"] = COUNTRY_NAME_ID_MAP[client_business_detail['country']]
        business_payload_body["Address10"] = client_business_detail['address']
        business_payload_body["Landmark11"] = client_business_detail['land_mark']
        business_payload_body["Latitude12"] = client_business_detail['latitude']
        business_payload_body["Longitude13"] = client_business_detail['longitude']
        business_payload_body["Location_Verified_cb_Location_Verified13"] = '471' if client_business_detail[
                                                                                         'location_verified'] == True else '472'

        business_payload["body"] = json.dumps(business_payload_body)

        payload_list.append(client_payload)
        payload_list.append(business_payload)

        requestId = requestId + 1

    return payload_list


@click.command()
def update_profile_mismatches():
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    # fetch updated ims customers
    query = '''
    with data as (select c.id, c.manager_first_name, c.manager_last_name, 
    c.store_name, oc2.name as country_name, c.phone_number, 
    c.land_mark, c.longitude, c.latitude FROM customers c 
    join organization_locations ol on ol.id = c.organization_location_id 
    join organization_regions or2 on or2.id = ol.organization_region_id 
    join organization_cities oc on oc.id = or2.organization_city_id 
    join organization_countries oc2 on oc2.id = oc.organization_country_id 
    where cast(c.updated_at as date) = current_date - 1)
    
    select * from data;
    
    '''

    updated_customers_ims = list()
    musoni_count = 0
    ext_ids = []

    try:
        fetch_conn = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        fetch_conn.execute(query)
        updated_customers_ims = fetch_conn.fetchall()
    except:
        logging.exception("error in fetching updated customers in IMS ")

    for ims_customer in updated_customers_ims:
        logging.info("fetching Musoni client details with external_id: %s", ims_customer['id'])
        try:
            musoni_client = musoni_utils.get_client_for_external_id(ims_customer['id'])
            if musoni_client:
                musoni_count = musoni_count + 1
                ext_ids.append(musoni_client['entityExternalId'])
                try:
                    musoni_client_details = musoni_utils.get_client_business_details_for_client_id(
                        musoni_client['entityId'])

                except:
                    logging.exception("Client details with external_id: %s is currently not available",
                                      ims_customer['id'])
                check_for_data_mismatch_and_update(musoni_client, musoni_client_details, ims_customer)
        except:
            logging.exception("Error while fetching customer with external_id: %s", ims_customer['id'])
    logging.info("Number of Musoni client found to update %s with ext_ids: %s", musoni_count, ext_ids)


def check_for_data_mismatch_and_update(musoni_client, musoni_client_details, ims_customer):
    client_update_payload = {}
    client_details_update_payload = {}
    formatted_number = get_formatted_mobile_number_by_country(ims_customer['phone_number'],
                                                              ims_customer['country_name'])
    # check and update client
    if "entityMobileNo" not in musoni_client and formatted_number is not None:
        phone_number = check_and_compare_if_kenya_phone_number(formatted_number, ims_customer['country_name'],
                                                               musoni_client['entityMobileNo'])
        if phone_number:
            client_update_payload['mobileNo'] = phone_number

    if formatted_number is not None and musoni_client['entityMobileNo'] != formatted_number:
        phone_number = check_and_compare_if_kenya_phone_number(formatted_number, ims_customer['country_name'],
                                                               musoni_client['entityMobileNo'])
        if phone_number:
            client_update_payload['mobileNo'] = phone_number

    if musoni_client['entityName'] != ims_customer['store_name'] + ' .':
        client_update_payload['firstname'] = ims_customer['store_name'][:50]
        client_update_payload['lastname'] = '.'

    if client_update_payload:
        logging.info("client update payload to be posted: %s", client_update_payload)
        musoni_utils.update_client_by_id(musoni_client['entityId'], client_update_payload)

    # check and update client business details
    if musoni_client_details:
        if musoni_client_details[0]["country"] != " ".join(
                [ims_customer["manager_first_name"], ims_customer["manager_last_name"]]):
            client_details_update_payload["country"] = " ".join(
                [ims_customer["manager_first_name"], ims_customer["manager_last_name"]])

        if musoni_client_details[0]["businessName"] != ims_customer["store_name"]:
            client_details_update_payload["businessName"] = ims_customer["store_name"]

        if musoni_client_details[0]["Landmark11"] != ims_customer["land_mark"]:
            client_details_update_payload["Landmark11"] = ims_customer["land_mark"]

        if str(musoni_client_details[0]["Latitude12"]) != ims_customer["latitude"]:
            client_details_update_payload["Latitude12"] = ims_customer["latitude"]

        if str(musoni_client_details[0]["Longitude13"]) != ims_customer["longitude"]:
            client_details_update_payload["Longitude13"] = ims_customer["longitude"]

    if client_details_update_payload:
        logging.info("client business details update payload to be posted: %s", client_details_update_payload)
        musoni_utils.update_client_business_details(musoni_client['entityId'], client_details_update_payload)


@click.command()
@click.option('--run-date', default=(datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d"),
              help="date for which we migrate customers")
def create_and_activate_client_for_date(run_date):
    psycopg2.extras.register_uuid()

    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )

    query = '''
    with data as (
    select 
    organization_countries.name as country_name,
    LEFT(regexp_replace(customers.store_name, '[^\w]+',' ','g'),50) as first_name,
    LEFT((CASE WHEN LENGTH(regexp_replace(customers.store_name, '[^\w]+',' ','g'))>50 then CONCAT( RIGHT(regexp_replace(customers.store_name,'[^\w]+',' ','g'),LENGTH(customers.store_name)-50),'.') else '.' end),50) as last_name,
    --replace(TRIM(organization_regions.name),' ','_') as office_name,
    TRIM(organization_regions.name) as office_name,
    customers.id as External_id,
    CONCAT('0',RIGHT(regexp_replace(customers.phone_number,'[^\w]+','','g'),9)) as mobile_number,
    customers.created_at::date as activation_date,
    'True' as active_status,
    CASE WHEN coalesce(customers.credit_limit,0) > 0  then 'True' else 'False' end as is_credit_customer,
    initcap((CASE WHEN (lower(customers.gender)!='male' and lower(customers.gender)!='female' and lower(customers.gender)!='other') then '' else customers.gender end))as gender,
    customers.credit_limit,
    CASE WHEN customer_profiles.credit_days is not null then concat(organization_countries.name,'',concat(customer_profiles.credit_days,concat('_day',TRIM(substring(customer_profiles.name,'[^-]*$')::text)))) else '' end as loan_product
    from customers
    left join organization_locations on organization_locations.id = customers.organization_location_id
    LEFT JOIN organization_regions ON organization_regions.id = organization_locations.organization_region_id
    LEFT JOIN organization_cities on organization_cities.id = organization_regions.organization_city_id
    LEFT JOIN organization_countries on organization_countries.id = organization_cities.organization_country_id
    LEFT JOIN customer_profiles on customer_profiles.id = customers.customer_profile_id
    where organization_countries.name in %s  and regexp_replace(customers.store_name, '[^\w]+','','g')<>'' and customers.created_at::date = %s::date)

    select * from data;

    '''

    output_record = []
    external_ids = list()
    offices = musoni_utils.get_offices()
    try:
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute(query, (CREATE_CUSTOMER_COUNTRY_LIST, run_date))
        reader = cursor.fetchall()
        for record in reader:
            external_ids.append(record['external_id'])
        if len(external_ids):
            business_details = get_business_details_musoni(external_ids)
        else:
            raise Exception(f"No customers created for date: {run_date}")
        for i in range(0, len(reader), MUSONI_CLIENT_CREATION_BATCH_SIZE):
            batch_reader = reader[i:i + MUSONI_CLIENT_CREATION_BATCH_SIZE]
            payload = get_client_and_business_creation_payload(offices, batch_reader, i, business_details)

            logging.info("payload to be posted %s", payload)

            try:
                response = musoni_utils.process_batch(payload)
                logging.info("response : %s", response)
            except Exception as e:
                logging.exception("error in processing the records %s", batch_reader)

            output_record = output_record + response
    except:
        logging.exception("error in processing file")

    finally:
        conn.close()

    if output_record:
        writer = csv.DictWriter(open("create_customer_output.csv", "w"), fieldnames=output_record[1].keys())
        writer.writeheader()
        writer.writerows(output_record)


def get_business_details_musoni(ids):
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )

    external_ids = tuple(ids)
    business_details = list()

    query = '''
    with data as(
    select
    customers.id as external_id,
    regexp_replace(customers.store_name, '[^\w]+',' ','g') as store_name,
    '' as business_type,
    customers.created_at::date as business_start_date,
    '' as Address,
    organization_cities.name as city,
    organization_regions.name as branch,
    organization_countries.name as country,
    concat(customers.manager_first_name,' ',customers.manager_last_name) as store_manager,
    customers.land_mark,
    customers.latitude,
    customers.longitude,
    customers.location_verified


    from customers  
    left join organization_locations on organization_locations.id = customers.organization_location_id
    LEFT JOIN organization_regions ON organization_regions.id = organization_locations.organization_region_id
    LEFT JOIN organization_cities on organization_cities.id = organization_regions.organization_city_id
    LEFT JOIN organization_countries on organization_countries.id = organization_cities.organization_country_id
    where customers.id in %s
    )

    select * from data

    '''
    try:
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute(query, [external_ids])
        business_details = cursor.fetchall()

    except:
        logging.exception("error in processing query")

    finally:
        conn.close()

    return business_details


@click.command()
@click.argument('file-path')
def change_loan_product(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    client_id = record['id']
                    product_id = record['new_product_id']
                    response = musoni_utils.update_customer_product(client_id, product_id)
                    record['status'] = 'success'
                    record['client_id'] = response['id']
                    sys.exit(0)
                except Exception as e:
                    record['status'] = 'fail'
                    record['client_id'] = str(e)
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")

    if output_record:
        writer = csv.DictWriter(open("change_product_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


@click.command()
@click.argument('file-path')
def change_customer_credit_profile(file_path):
    output_record = []
    print("Following fields are supported in this update and ensure you columns headers same as below")
    print("The fields can contain only need fields, however client_id is mandatory")
    print(','.join(CUSTOMER_PROFILE_ID_MAP.keys()))
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    client_id = str(record['client_id']).replace(',','')
                    response = musoni_utils.update_client_credit_details(client_id, record)
                    record['status'] = 'success'
                    record['client_id'] = response['clientId']
                except Exception as e:
                    record['status'] = 'fail'
                    record['client_id'] = str(e)
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")

    if output_record:
        writer = csv.DictWriter(open("change_product_output.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


commands.add_command(update_kyc_status, 'update-client-kyc-status-if-completed')
commands.add_command(close_clients, 'close')
commands.add_command(close_clients_without_approved_limit, 'close-without-limit')
commands.add_command(assign_credit_details_to_ims_client, 'assign-credit-details')
commands.add_command(activate_clients, 'activate')
commands.add_command(create_and_activate_client, 'create-customer-from-file')
commands.add_command(create_and_activate_client_for_date, 'create-customer')
commands.add_command(change_loan_product, 'change-loan-product')
commands.add_command(change_customer_credit_profile, 'customer_credit_profile')
commands.add_command(update_profile_mismatches, 'update-mismatches')
commands.add_command(migrate_clients_from_nai_east_to_thika, 'migrate-clients-nai-east-to-thika')
commands.add_command(migrate_clients_from_ruyenzi_to_kigali, 'migrate-clients-ruyenzi-to-kigali')
commands.add_command(migrate_clients_from_dar_east_and_west_to_central, 'migrate-clients-dar-east-and-west-to-central')


if __name__ == '__main__':
    commands()
