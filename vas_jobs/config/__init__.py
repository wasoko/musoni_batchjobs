import os
from pickle import FALSE


current_env = os.getenv('MUSONI_JOBS_ENV', 'local')

BASE_WASOKO_API_URL = 'https://dev.d.api.wasoko.cloud/'

if current_env == 'local':
    from .local import *
elif current_env == 'dev':
    from .dev import *
elif current_env == 'qa':
    from .qa import *
elif current_env == 'uat':
    from .uat import *
elif current_env == 'prod':
    from .prod import *