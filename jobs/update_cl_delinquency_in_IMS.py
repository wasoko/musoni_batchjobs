import sys
import logging
import csv
import click

from crm_connector import *

logging.basicConfig(
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])

auth_response  = get_auth_token()
token = auth_response['token_details']['access_token']

def update_credit_limit_to_ims(customer_id, credit_limit, delinquency):
    logging.info("updating the customer credit limit to IMS %s %s", customer_id, credit_limit)
    try:
        customer_details = get_customer_details(token, customer_id)
        customer_details['credit_limit']= credit_limit
        customer_details['delinquency'] = delinquency
        customer_details['language_id'] = customer_details['language']['id']
        customer_details['organization_location_id'] = customer_details['organization_location']['id']
        response = update_customer_details(token, customer_id, customer_details)
        logging.info("updated the credit limit for the customer and response %s", response)
    except:
        logging.exception("error in updating the credit limit for the customer %s", customer_id)

@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def update_IMS(file_path):
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                try:
                    update_credit_limit_to_ims(record['customer_id'], record['credit_limit'], record['delinquency'])
                except:
                    record['status'] = 'fail'
                    logging.exception("error in processing the record %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
    if output_record:
            writer = csv.DictWriter(open("clients_reactivation_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)


commands.add_command(update_IMS, 'update_IMS')

if __name__ == '__main__':
    commands()