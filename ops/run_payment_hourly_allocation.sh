#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/payment_through_allocation.py process-for-last-hour
  # python jobs/musoni_payment_update.py  2022-03-11
  #  we can give the date
popd
