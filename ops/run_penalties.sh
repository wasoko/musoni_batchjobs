#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  if [ -z "$ARGS" ]
  then
    echo "ARGS is not passed runing for previous day"
    /usr/bin/python3 jobs/penalties.py apply-penalties 
  else
    echo "ARGS is passed runing for"
    echo ${ARGS}
    /usr/bin/python3 jobs/penalties.py apply-penalties --run-date ${ARGS}
  fi
  /usr/bin/python3 jobs/penalties.py report-penalties
popd
