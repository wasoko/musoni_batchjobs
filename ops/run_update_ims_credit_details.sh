#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  if [ -z "$ARGS" ]
  then
    echo "ARGS is not passed running with previous days date"
    /usr/bin/python3 jobs/client_manager.py assign-credit-details
  else
    echo "ARGS is passed running with "
    echo ${ARGS}
    /usr/bin/python3 jobs/client_manager.py assign-credit-details --target-date ${ARGS}
  fi
popd