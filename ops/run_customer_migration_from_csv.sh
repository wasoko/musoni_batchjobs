#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  if [ -z "$ARGS" ]
  then
    /usr/bin/python3 jobs/client_manager.py create-customer-from-file customer_migration.csv
  fi
popd