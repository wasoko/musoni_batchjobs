import sys
import pandas as pd
from datetime import datetime, timedelta
from sqlalchemy import create_engine
from config import *

def check_latest_data_available():
    if not CHECK_LATEST_MUSONI_DATA_DUMP_AVAILABLE:
        print("musoni data dump check paused")
        return
    
    engine = create_engine('mysql+pymysql://{user}:{pwd}@{host}:{port}/{db}'.format(user=MUSONI_DB_CONNECTION_USERNAME,
                                                                                    pwd=MUSONI_DB_CONNECTION_PASSWORD,
                                                                                    host=MUSONI_DB_CONNECTION_HOST,
                                                                                    port=MUSONI_DB_CONNECTION_PORT,
                                                                                    db=MUSONI_DB_CONNECTION_DB))

    date_to_check = (datetime.today() + timedelta(days=-1))
    records = pd.read_sql_query("select count(*) as 'cnt' from m_loan where date(lastmodified_date)=%(date_to_check)s", engine, params={'date_to_check': date_to_check.strftime("%Y-%m-%d")})
    print("musoni records available for the date %s %s", date_to_check, records['cnt'][0])
    if records['cnt'][0] > 0:
        return
    
    print("CRITICAL ERROR :: latest dump not available exiting from processing ")
    sys.exit(1)

if __name__ == '__main__':
    check_latest_data_available()