#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  /usr/bin/python3 jobs/payment_sms_remainders.py process-sms-for-country ${CURRENCY_CODE} ${RETRY_FLAG}
popd
