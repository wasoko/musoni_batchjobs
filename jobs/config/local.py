import os

# DB_CONNECTION_HOST = 'ims-db.local'
# DB_CONNECTION_DB = 'erp_dev'
# DB_CONNECTION_PORT = '5432'
# DB_CONNECTION_USERNAME = 'khBw2W'
# DB_CONNECTION_PASSWORD = 'Joh7AhTh'

READ_DB_CONNECTION_HOST = 'ims-db.local'
READ_DB_CONNECTION_DB = 'erp_dev'
READ_DB_CONNECTION_PORT = '5432'
READ_DB_CONNECTION_USERNAME = 'khBw2W'
READ_DB_CONNECTION_PASSWORD = 'Joh7AhTh'


DB_CONNECTION_HOST = 'ims-db.local'
DB_CONNECTION_DB = 'qa'
DB_CONNECTION_PORT = '5432'
DB_CONNECTION_USERNAME = 'khBw2W'
DB_CONNECTION_PASSWORD = 'Geing5Fu'
#
# READ_DB_CONNECTION_HOST = 'ims-db.local'
# READ_DB_CONNECTION_DB = 'qa'
# READ_DB_CONNECTION_PORT = '5432'
# READ_DB_CONNECTION_USERNAME = 'khBw2W'
# READ_DB_CONNECTION_PASSWORD = 'Geing5Fu'

MUSONI_DB_CONNECTION_USERNAME = 'root'
MUSONI_DB_CONNECTION_PASSWORD = 'TpQK4hZ2C`8ztsy#'
MUSONI_DB_CONNECTION_HOST = 'npi-mysql-db.local'
MUSONI_DB_CONNECTION_PORT = '3306'
MUSONI_DB_CONNECTION_DB = 'demo_musoni_db'

CREDIT_TILL_NUMBERS = '239356'
 
MUSONI_USERNAME = 'MusoniIntegrationJob'
MUSONI_PASSWORD = 'Musoni@123'

MUSONI_BASE_URL = 'https://api.demo.irl.musoniservices.com/v1/'
MUSONI_API_KEY = "4ihoKzyAQa7HX1JLfdcmr3aiTxbDpbliaDXth6SN"
MUSONI_TENANT_ID = 'wasoko'
MUSONI_HEADERS = {
    'X-Fineract-Platform-TenantId': MUSONI_TENANT_ID,
    'x-api-key': MUSONI_API_KEY
}

BASE_WASOKO_API_URL = 'https://dev.d.api.wasoko.cloud/'

CI_AUTH_USERNAME =  'MusoniIntegrationJob@wasoko.com'
CI_AUTH_PASSWORD= 'Musoni@123'
CI_AUTH_APPLICATIONID = 'b5318e50-2192-4647-9c90-1e569f49fe69'

OMS_PENALTY_FEE_TOKEN = '$2a$12$Be/uUM7B0JHWS0Z2R2CDte730MWTQoxkvf5R1AZHrcHt9bgJLTzSi'


MUSONI_PAYMENT_AUDIT_DATA_EXPORT_ID = 19
MUSONI_SA_WITH_POSITIVE_BALANCE_ID = 19
UPDATE_CREDIT_LIMIT_DATA_EXPORT_CLIENTS = 21
UPDATE_CREDIT_LIMIT_DATA_EXPORT_LOANS = 20
UPDATE_AVAILABLE_LIMIT_TO_MUSONI = True
UPDATE_AVAILABLE_LIMIT_TO_IMS = False
UPDATE_DELINQUENCY_TO_MUSONI = True
UPDATE_DELINQUENCY_TO_IMS = True
BRANCH_FILTER_LIST = [22,23]

DEFAULT_STAFF_FOR_COUNTRY = {
    "kenya": 55, #William
    "côte d'ivoire": 57, #Stanley
    "uganda": 16, #Moses
    "rwanda": 20, #Jasmine
    "tanzania": 66, #Victor
}

NOTIFIER_URL= 'https://uat.t.api.wasoko.cloud/notifier/notify'
SANDBOX_SMS = True

CLIENTS_SKIP_FOR_CLOSE = [103184, 471]
CLOSE_CLIENT_FILTER_TEXT = 'Close Client'

BLACKLIST_WRITEOFF_REASON_CODES = [457, 458, 459, 462, ]

# FS_PAYMENT_BASE_URL = "https://uat.t.api.wasoko.cloud/fs-musoni-payments-integration/"
FS_PAYMENT_BASE_URL = "https://qa.t.api.wasoko.cloud/fs-musoni-payments-integration/"
FS_PAYMENT_API_URL = f'{FS_PAYMENT_BASE_URL}payments'
FS_PAYMENT_ALLOCATION_API_URL = f'{FS_PAYMENT_BASE_URL}paymentAllocation'
FS_PAYMENT_API_KEY = "$2a$10$.YCv3NfuwjfiDae7j5ATOOlwfzb5cyt87Gy0i6nSj79IfFCK889Q."

TZ_PENALTY_LOAN_PRODUCT_ID_LIST = [31]
COUNTRY_CHARGES_ID_MAP = {
    "TZS": "15"
}

COUNTRY_PRODUCT_ID_MAP = {
    "TZS": [31]
}

COUNTRY_SMS_ENABLED_BRANCHES = {
    'TZS': 'all',
    'KES': [4,],
    'RWF': 'all',
    'XOF': 'all',
    'UGX': 'all',
}

LOAN_TO_PRODUCT_ID_MAP = {
    # kenya
    401: 'e37a8849-9185-4c00-a4f6-2d925cff20c9',
    402: '3d3525af-bb21-433e-bd29-bfa4e0c71a15',
    403: '0bea52b4-a79d-4d39-9a00-5c8b7ce0ca51',
    404: 'b78625a8-d7b8-481b-a96f-c04460fffb06',
    # rwanda
    466: '85594eef-026f-4c55-8367-29478fcd8343',
    467: '23eda8e2-2919-4935-9637-4391fb72cdf6',
    468: '5bfabaad-191c-4a7a-aea6-c5fa5d21e6c1',
    # uganda
    462: 'ea0ff478-14cb-48f9-99e7-c2c8926178d4',
    463: '2c755688-2133-4895-bbb5-0805b9c60d73',
    464: 'b01fcd9b-ec60-4874-a6c2-18f0c89cbcf7',
    465: 'cc11ae14-25a5-48ff-9150-cf6e5ff95082',
    # Côte d''Ivoire
    446: '11d975a8-327c-458a-be44-694c65a78278',
    # tanzania
    # 471: '',
    # 473: '',
    476: '7101071f-3767-44d0-97c9-87450c590b17',
    # 477: '',
    # 478: ''
}
BRANCH_OFFICE_IDS_MAP = {
    1: {
        'name': 'Nairobi East',
        'office_id': 6
    },
    2: {
        'name': 'Thika',
        'office_id': 10
    },
    3: {
        'name': 'Ruyenzi',
        'office_id': 30
    },
    4: {
        'name': 'Kigali',
        'office_id': 14
    },
    5: {
        'name': 'Dar es Salaam East',
        'office_id': 16
    },
    6: {
        'name': 'Dar es Salaam West',
        'office_id': 17
    },
    7: {
        'name': 'Dar es Salaam Central',
        'office_id': 17
    }
}

FIELD_REP_ROLE = [14]

WEBHOOK_URL = "https://chat.googleapis.com/v1/spaces/AAAAfZ1sBeg/messages?key=AIzaSyDdI0hCZtE6vySjMm-WE" \
              "fRq3CPzqKqqsHI&token=v-SdDwBb9glrjwWv0Zw2tdJQVQyw0-y9Q2QRd8gMCGM"

