from datetime import datetime
import datetime as dt
from dateutil.parser import parse
import psycopg2
import psycopg2.extras
import requests
import pandas as pd
import csv
import sys
import click
from config import *
import logging
import pandas as pd
import re
import json

logging.basicConfig(
    encoding='utf-8', 
    level=logging.DEBUG, 
    handlers=[logging.StreamHandler(sys.stdout)])


def allocate_money(record):
    transaction_date = record['date'].strftime('%Y-%m-%dT%H:%M:%S.%f%z')
    payload = {
        'sender_number': record['to_payer'],
        'confirmation_code':record['confirmation_code'],
        'transaction_amount':record['amount'],
        'current_balance': record['current_balance'],
        'transaction_time':transaction_date,
        'actual_message': record['actual']
    }
    logging.info("posting payment to payments api %s", json.dumps(payload))
    headers = {"Authorization": FS_PAYMENT_API_KEY}
    response = requests.post(FS_PAYMENT_API_URL, headers=headers, json=payload)
    logging.info("posting payment to payments api %s", response.text)
    if response.status_code != 200:
        return False, response.json().get('errorDescription', 'No error details')
    return True, 'SUCCESS'


def call_payment_api(payload):
    status = False
    comment = ''
    counter = 1
    while status == False and counter <= 2:
        logging.info("posting record for allocating money %s %s", payload, counter)
        status, comment = allocate_money(payload)
        if not status and comment == "Deposit to Savings account failed":
            payload['date'] = datetime.today()
            counter = counter + 1
        else:
            logging.info("unhandled/unknown status and comment skipping posting %s %s", status, comment)
            break
    return status, comment

def call_payment_pg(payload):
    url = BASE_WASOKO_API_URL+"api/pg/v1/mpesa/payment"
    json_payload = json.dumps(payload)
    headers = {
    'x-api-token': '$2a$10$tjJQk7F4Xh2Bo6X03C.D2OKvfxy6ll7reey3kOdDQV583YOQcr7Aa',
    'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=json_payload)
    return 'success', response.text
    

def get_formatted_phone_number(phone_number):
    if not phone_number.startswith('254'):
        phone_number = '254' + phone_number
    return '+' + phone_number


def get_custom_formatted_phone_number(phone_number, suffix):
    stripped = re.sub(r"\W", '', phone_number)
    if stripped.startswith('0'):
        stripped = stripped[1:]
    if not stripped.startswith(suffix):
        stripped = suffix + stripped
    return '+' + stripped


def is_processed_in_musoni(confirmation_code, conn):
    query  = "select * from musoni_payment_update_tracker where status=true and confirmation_code = %s"
    cursor = conn.cursor()
    cursor.execute(query, (confirmation_code,))
    count = cursor.rowcount
    logging.info("records found %s", cursor.fetchall())
    return True if count >= 1 else False

def is_processed_in_ims(confirmation_code, conn):
    query  = "select confirmation_code, cpd.created_at, cp.aasm_state from customer_payment_details cpd join payment_confirmations pc on confirmation_code=payment_refrence_code join customer_payments cp on cpd.customer_payment_id=cp.id where payment_refrence_code=%s"
    cursor = conn.cursor()
    cursor.execute(query, (confirmation_code,))
    count = cursor.rowcount
    logging.info("records found %s", cursor.fetchall())
    return True if count >= 1 else False


def is_migrated_to_musoni(confirmation_code, conn):
    query  = "select * from musoni_payment_request_tracker where confirmation_code like %s"
    cursor = conn.cursor()
    cursor.execute(query, ('%'+confirmation_code+'%',))
    count = cursor.rowcount
    logging.info("records found %s", cursor.fetchall())
    return True if count >= 1 else False

def is_processed(confirmation_code, conn):
    logging.info("check if record in IMS and musoni %s", confirmation_code)
    if not is_processed_in_musoni(confirmation_code, conn):
        if not is_processed_in_ims(confirmation_code, conn):
            return False
        else:
            if not is_migrated_to_musoni(confirmation_code, conn):
                return False
            logging.info("migrated to musoni")
        logging.info("processed in IMS %s", confirmation_code)
        return True
    logging.info("processed in musoni %s", confirmation_code)
    return True


@click.group()
def commands():
    pass

@click.command()
@click.argument('file-path')
def process_rw_payments_from_payment_file(file_path):
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    output_record = []
    try:
        with open(file_path) as fp:
            df = pd.read_csv(fp)
            df[['Prefix', 'Phone Number', 'Suffix']] = df['From'].str.split(':|/', 2, expand=True)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record %s", record)
                if not is_processed(str(record['Id']), conn) and record['Currency.14'] == 'RWF':
                    try:
                        logging.info("Processing the record")
                        payload = {
                            'to_payer': get_custom_formatted_phone_number(record['Phone Number'], '250'),
                            'confirmation_code': record['Id'],
                            'current_balance': record['Balance'],
                            'amount': record['Amount'],
                            'date': datetime.strptime(record['Date'], "%Y-%m-%d %H:%M:%S").date(),
                            'actual': record['Id']
                        }
                        status, comment = allocate_money(payload)
                        record['status'] = status
                        record['comment'] = comment
                    except Exception as e:
                        record['status'] = False
                        record['comment'] = str(e)
                        logging.exception("exception wile processing %s", record)
                else:
                    record['status'] = False
                    record['comment'] = 'already processed'
                    logging.exception("record processed skipping %s", record)
                    if record['Currency.14'] != 'RWF':
                        logging.exception("record Currency is %s instead of RWF", record['Currency'])
                output_record.append(record)
            conn.close()
    except Exception as openingFileException:
        logging.exception("error in processing file", openingFileException)
        conn.close()
        sys.exit(1)
        
    if output_record:
        writer = csv.DictWriter(open("missing_rw_payments_processing.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)




@click.command()
@click.argument('file-path')
def process_tz_payments_from_payment_file(file_path):
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB,
        user=DB_CONNECTION_USERNAME,
        password=DB_CONNECTION_PASSWORD,
        host=DB_CONNECTION_HOST,
        port=DB_CONNECTION_PORT
    )
    output_record = []
    try:
        with open(file_path) as fp:
            df = pd.read_csv(fp, nrows=2, header=None)
            if df[1][1] != '816222':
                logging.warning("The Sort code is not belongs to the Credit till payment exiting")
                sys.exit(1)
            fp.seek(0)
            df = pd.read_csv(fp, skiprows=5)
            df = df.dropna(subset=['Paid In'])
            df = df[df['Reason Type'] != 'Pay Merchant Reversal']
            df = df[df['Transaction Status'] != 'Declined']
            for idx, record in df.iterrows():
                record = record.to_dict()
                phone_number = record['Details'].split(' ')[-1]
                logging.info("processing record %s", record)
                if not is_processed(record['Receipt No.'], conn) and record['Currency'] == 'TZS':
                    try:
                        logging.info("Processing the record")
                        payload = {
                            'to_payer': get_custom_formatted_phone_number(phone_number, '255'),
                            'confirmation_code': record['Receipt No.'],
                            'current_balance': str(record['Balance']).replace(',', ''),
                            'amount': str(record['Paid In']).replace(',', ''),
                            'date': parse(record['Completion Time'], dayfirst=True).date(),
                            'actual': record['Receipt No.']
                        }
                        status, comment = call_payment_api(payload)
                        record['status'] = status
                        record['comment'] = comment
                    except Exception as e:
                        record['status'] = False
                        record['comment'] = str(e)
                        logging.exception("exception wile processing %s", record)
                else:
                    record['status'] = False
                    record['comment'] = 'already processed'
                    logging.exception("record processed skipping %s", record)
                    if record['Currency'] != 'TZS':
                        logging.exception("record Currency is %s instead of TZS", record['Currency'])
                output_record.append(record)
            conn.close()
    except Exception as openingFileException:
        logging.exception("error in processing file", openingFileException)
        conn.close()
        sys.exit(1)

    if output_record:
        writer = csv.DictWriter(open("missing_tz_payments_processing.csv", "w"), fieldnames=output_record[0].keys())
        writer.writeheader()
        writer.writerows(output_record)


@click.command()
@click.argument('file-path')
def process_payments_csv(file_path):
    conn = psycopg2.connect(
        database=READ_DB_CONNECTION_DB, 
        user=READ_DB_CONNECTION_USERNAME, 
        password=READ_DB_CONNECTION_PASSWORD, 
        host=READ_DB_CONNECTION_HOST, 
        port=READ_DB_CONNECTION_PORT
    )
    output_record = []
    try:
        with open(file_path) as fp:
            reader = csv.DictReader(fp)
            for record in reader:
                logging.info("processing record %s", record)
                if not is_processed(record['Receipt No.'], conn):
                    try:
                        logging.info("Processing the record")
                        payload = {
                            'to_payer': get_formatted_phone_number(record['Other Party Info']),
                            'confirmation_code':record['Receipt No.'],
                            'current_balance':record['Balance'].replace(',', ''),
                            'amount':record['Paid In'].replace(',', ''),
                            'date':parse(record['Completion Time'], dayfirst=True).date(),
                            'actual':record['Receipt No.'] + ' missing payments'
                        }
                        status, comment = call_payment_api(payload)
                        record['status'] = status
                        record['comment'] = comment
                    except Exception as e:
                        record['status'] = False
                        record['comment'] = str(e) 
                        logging.exception("exception wile processing %s", record)
                else:
                    record['status'] = False
                    record['comment'] = 'already processed'
                    logging.exception("record processed skipping %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
        sys.exit(1)

    if output_record:
            writer = csv.DictWriter(open("missing_payments_processing.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)

@click.command()
@click.argument('file-path')
def process_failed_payments_for_all_countries(file_path):
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )
    output_record = []
    try:
        with open(file_path, mode="rb") as fp:
            is_excel = False
            try: 
                logging.info("try the excel processing")
                df = pd.read_excel(fp,)
                is_excel =True
            except:
                logging.info("failed file is not excel try it as csv")
                df = pd.read_csv(fp)
            df['confirmation_code'] = df['confirmation_code'].astype(str)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record %s", record)
                if not is_processed(record['confirmation_code'], conn):
                    try:
                        logging.info("Processing the record")
                        payload = {
                            'to_payer': str(record['correct_customer_number']) if str(record['correct_customer_number'])[0] == '+' else '+' + str(record['correct_customer_number']),
                            'confirmation_code':record['confirmation_code'],
                            'current_balance':str(record['current_balance']).replace(',', ''),
                            'amount':str(record['transaction_amount']).replace(',', ''),
                            'date':parse(record['transaction_time'], dayfirst=True).date(),
                            'actual': record['confirmation_code'] + ' failed payments'
                        }
                        status, comment = call_payment_api(payload)
                        record['status'] = status
                        record['comment'] = comment

                    except Exception as e:
                        record['status'] = False
                        record['comment'] = str(e) 
                        logging.exception("exception wile processing %s", record)
                else:
                    record['status'] = False
                    record['comment'] = 'already processed'
                    logging.exception("record processed skipping %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
        sys.exit(1)

    if output_record:
            writer = csv.DictWriter(open("failed_payment_processing_output.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)


@click.command()
@click.argument('file-path')
def process_payments_payment_file(file_path):
    conn = psycopg2.connect(
        database=DB_CONNECTION_DB, 
        user=DB_CONNECTION_USERNAME, 
        password=DB_CONNECTION_PASSWORD, 
        host=DB_CONNECTION_HOST, 
        port=DB_CONNECTION_PORT
    )
    output_record = []
    try:
        with open(file_path, mode="rb") as fp:
            is_excel = False
            try: 
                logging.info("try the excel processing")
                df = pd.read_excel(fp,  nrows=2, header=None)
                is_excel =True
            except:
                logging.info("failed file is not excel try it as csv")
                df = pd.read_csv(fp,  nrows=2, header=None)
            logging.info("sort code is df %s", df[1][1])
            if df[1][1]!='239032':
                logging.warning("The Sort code is not belongs to the Credit till payment exiting")
                sys.exit(1)
            fp.seek(0)
            df = pd.read_csv(fp, skiprows=6) if not is_excel else pd.read_excel(fp, skiprows=6)
            df = df.dropna(subset=['Paid In'])
            df = df[df['Reason Type'] != 'Pay Merchant Reversal']
            df[['Phone Number', 'Customer Name']] = df['Other Party Info'].str.split(' ', 1, expand=True)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record %s", record)
                if not is_processed(record['Receipt No.'], conn):
                    try:
                        logging.info("Processing the record")
                        payload = {
                            'to_payer': get_formatted_phone_number(record['Phone Number']),
                            'confirmation_code':record['Receipt No.'],
                            'current_balance':str(record['Balance']).replace(',', ''),
                            'amount':str(record['Paid In']).replace(',', ''),
                            'date':parse(record['Completion Time'], dayfirst=True).date(),
                            'actual': record['Receipt No.'] + ' missing payments'
                        }
                        status, comment = call_payment_api(payload)
                        record['status'] = status
                        record['comment'] = comment

                    except Exception as e:
                        record['status'] = False
                        record['comment'] = str(e) 
                        logging.exception("exception wile processing %s", record)
                else:
                    record['status'] = False
                    record['comment'] = 'already processed'
                    logging.exception("record processed skipping %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
        sys.exit(1)

    if output_record:
            writer = csv.DictWriter(open("missing_payments_processing.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)

@click.command()
@click.argument('file-path')
def process_payments_payment_file_pg(file_path):
    output_record = []
    try:
        with open(file_path, mode="rb") as fp:
            is_excel = False
            try: 
                logging.info("try the excel processing")
                df = pd.read_excel(fp,  nrows=2, header=None)
                is_excel =True
            except:
                logging.info("failed file is not excel try it as csv")
                df = pd.read_csv(fp,  nrows=2, header=None)
            logging.info("sort code is df %s", df[1][1])
            fp.seek(0)
            df = pd.read_csv(fp, skiprows=6) if not is_excel else pd.read_excel(fp, skiprows=6)
            df = df.dropna(subset=['Paid In'])
            df = df[df['Reason Type'] != 'Pay Merchant Reversal']
            df[['Phone Number', 'Customer Name']] = df['Other Party Info'].str.split(' ', 1, expand=True)
            for idx, record in df.iterrows():
                record = record.to_dict()
                logging.info("processing record %s", record)
                try:
                    logging.info("Processing the record")
                    payload = {
                        "transaction_amount": str(record['Paid In']).replace(',', ''),
                        "transaction_time": parse(record['Completion Time'], dayfirst=True).strftime("%Y-%m-%dT%H:%M:%S"),
                        "sender_number": get_formatted_phone_number(record['Phone Number']),
                        "confirmation_code": record['Receipt No.']
                    }
                    status, comment = call_payment_pg(payload)
                    print(status, comment)
                    record['status'] = status
                    record['comment'] = comment
                except Exception as e:
                    record['status'] = False
                    record['comment'] = str(e) 
                    logging.exception("exception wile processing %s", record)
                output_record.append(record)
    except:
        logging.exception("error in processing file")
        sys.exit(1)

    if output_record:
            writer = csv.DictWriter(open("missing_payments_processing.csv", "w"), fieldnames=output_record[0].keys())
            writer.writeheader()
            writer.writerows(output_record)

@click.command()
def allocate():
    confirmation_code = click.prompt("confirmation code")
    sender_number = click.prompt("sender number")
    amount = click.prompt("amount")
    payment_date = click.prompt("date (dd-mm-yyyy)")
    actual_message = click.prompt("actual message")
    current_balance = click.prompt("balance")
    payload = {
        'to_payer': get_formatted_phone_number(sender_number),
        'confirmation_code':confirmation_code,
        'current_balance': current_balance,
        'amount':amount,
        'date':datetime.strptime(payment_date, "%d-%m-%Y").date(),
        'actual': actual_message
    }
    allocate_money(payload)

commands.add_command(allocate, "allocate")
commands.add_command(process_payments_csv, "process-file")
commands.add_command(process_payments_payment_file, "process-payment-file")
commands.add_command(process_payments_payment_file_pg, "process-payment-file-pg")
commands.add_command(process_tz_payments_from_payment_file, "process-tz-payment-file")
commands.add_command(process_rw_payments_from_payment_file, "process-rw-payment-file")
commands.add_command(process_failed_payments_for_all_countries, "process-failed-payments" )

if __name__ == '__main__':
    commands()