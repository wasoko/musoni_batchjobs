#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
    echo "ARGS is passed runing for" 
    echo ${DATE_OF_CHANGE} ${SYSTEM} ${TRIGGERED_BY}
    /usr/bin/python3 jobs/credit_limit_manager.py update-limit credit_limit_update.csv ${TRIGGERED_BY} --date-of-change=${DATE_OF_CHANGE} --system=${SYSTEM}
popd
